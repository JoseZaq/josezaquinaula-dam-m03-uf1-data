package cat.itb.joseZaquinaula7e4.dam.m03.uf2.practica;

import java.util.Locale;
import java.util.Scanner;

public class RecursiveFactorial {
    public static void main(String[] args) {
        //VARs
        Scanner scanner= new Scanner(System.in).useLocale(Locale.US);
        //actions
        int numNums = scanner.nextInt();
        for (int i = 0; i < numNums; i++) {
            int num = scanner.nextInt();
            System.out.println(factorial(num));
        }
    }

    /**
     * funcion recursiva, realiza el factorial de un n dado
     * @param n numero a facorizar
     * @return valor de n!
     */
    private static int factorial(int n) {
        // n! = n*(n-1)!
        if(n!=0)
            return n*factorial(n-1);
        return 1;
    }
}
