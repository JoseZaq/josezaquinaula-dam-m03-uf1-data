package cat.itb.joseZaquinaula7e4.dam.m03.uf2.practica;

import java.util.Locale;
import java.util.Scanner;

public class RecursiveFibonacci {
    public static void main(String[] args) {
        //VARs
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        //actions
        int n=scanner.nextInt();
        System.out.println(isFibonacciNum(n));
    }

    /**
     * verifica si es un numero de fibbonaci n
     * @param n numero a verificar
     * @return true or false
     */
    private static boolean isFibonacciNum(int n) {
        int i=0; //index while
        while(true) {
            int fib= fibonacci(i); //actual fibonnaci
            if (n == fib)
                return true;
            if(fib > n) //si el n es mayor que el fib actual es porque no es un num fib n
                return false;
            i++;
        }
    }

    /**
     * realiza el f(n)= f(n-2) + f(n+1), f()= fibbonaci function
     * @param n
     * @return
     */
    private static int fibonacci(int n) {
        //f(n) = f(n-2) + f(n-1)
        if( n==2 || n==1 || n==0)
            return 1;
        return fibonacci(n-2) + fibonacci(n-1);
    }
}
