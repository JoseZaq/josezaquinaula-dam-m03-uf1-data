package cat.itb.joseZaquinaula7e4.dam.m03.uf2.practica;

public class Peak {
    //VARs
    //nom del cim, alçada (metres), país, distància (metres), temps(en minuts)-.
    private String nom;
    private double height;
    private String country;
    private double distance;
    private int time;
    //contructor

    public Peak(String nom, double height, String country, double distance, int time) {
        this.nom = nom;
        this.height = height;
        this.country = country;
        this.distance = distance;
        this.time = time;
    }
    //functions

    //getters

    /**
     * getSpeed en km/h
     * @return velocidad en km/h
     */
    public double getSpeed(){
        return (distance/1000)/((double)time/60);
    }
    public String getNom() {
        return nom;
    }

    public double getHeight() {
        return height;
    }

    public String getCountry() {
        return country;
    }

    public double getDistance() {
        return distance;
    }

    public int getTime() {
        return time;
    }
}
