package cat.itb.joseZaquinaula7e4.dam.m03.uf2.practica;

public class Time {
    //VARs
    //functions

    /**
     * Convierte minutos a horas:minutos
     * @param minutes minutos de entrada
     * @return formato de minutos en hh:mm
     */
    public static String minutesToHHMM(int minutes){
        int hours = minutes/60;
        int minutesRes=minutes;
        if(hours !=0){ //si el time es menos de una 1 hora, los minustos son los mismos
            minutesRes = minutes%60;} //residuo de minutos/horas
        String hhmm= String.format("%d:%02d",hours,minutesRes);
        return hhmm;
    }
}
