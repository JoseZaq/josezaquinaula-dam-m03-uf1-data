package cat.itb.joseZaquinaula7e4.dam.m03.uf2.practica;

import java.util.Locale;
import java.util.Scanner;

public class HighPeaks {
    /*
    Un alpinista ens ha demanat que li fem un programa per enregitrar i evaluar les seves fites.
     Vol poder enmagatzemar els diferents cims que ha fet amb dades de la seva ascenció -nom del cim,
      alçada (metres), país, distància (metres), temps(en minuts)-.
      A més a més vol tenir estadístiques del cim més alt, com fet amb el mínim temps i el que s'ha fet
      a una velocitat més ràpida (distància/temps).
     */
    public static void main(String[] args) {
        //VARs
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);

        //actions
        int numPeaks = scanner.nextInt();
        Peak[] peaks = new Peak[numPeaks];
        //fori para ingresar y crear datos de una cima nueva
        for (int i = 0; i < peaks.length; i++) {
            String trash= scanner.nextLine();
            String name=scanner.nextLine();
            double height = scanner.nextDouble();
            trash= scanner.nextLine();
            String country = scanner.nextLine();
            double distance = scanner.nextDouble();
            int time = scanner.nextInt();
            peaks[i] = new Peak(name,height,country,distance,time);

        }
        //OUTPUT
            //fori para printar los datos de todas las cimas
        System.out.println("------------------------\n--- Cims aconseguits ---\n" +
                "------------------------\n");
        for (int i = 0; i < peaks.length; i++) {
            System.out.printf("%s - %s (%.0fm) - Temps: %s\n",peaks[i].getNom(),
                    peaks[i].getCountry(),peaks[i].getHeight(),Time.minutesToHHMM(peaks[i].getTime()));
        }
        //functions
        System.out.printf("------------------------\n"+"N. cims: %d\n" +
                        "Cim més alt: %s (%.0fm)\n" +
                        "Cim és ràpid: %s (%s)\n" +
                        "Cim més veloç: %s %.2fkm/hora\n" +
                        "------------------------\n",peaks.length,higherPeak(peaks).getNom(),
                higherPeak(peaks).getHeight(),fastestPeak(peaks).getNom(),Time.minutesToHHMM(fastestPeak(peaks).getTime()),
                bigSpeedPeak(peaks).getNom(),bigSpeedPeak(peaks).getSpeed());
    }

    private static Peak bigSpeedPeak(Peak[] peaks) {
        double max =( peaks[0].getDistance() / peaks[0].getTime());
        Peak bigSpeedPeak=peaks[0];
        for (int i = 1; i < peaks.length; i++) {
            if(max < ( peaks[i].getSpeed())){
                max =peaks[i].getSpeed();
                bigSpeedPeak = peaks[i];
            }
        }
        return bigSpeedPeak;
    }

    private static Peak fastestPeak(Peak[] peaks) {
        double min = peaks[0].getTime();
        Peak fastestPeak=peaks[0];
        for (int i = 1; i < peaks.length; i++) {
            if(min > peaks[i].getTime()){
                min = peaks[i].getTime();
                fastestPeak = peaks[i];
            }
        }
        return fastestPeak;
    }

    private static Peak higherPeak(Peak[] peaks) {
        double max = peaks[0].getHeight();
        Peak higherPeak=peaks[0];
        for (int i = 1; i < peaks.length; i++) {
            if(max < peaks[i].getHeight()){
                max = peaks[i].getHeight();
                higherPeak = peaks[i];
            }
        }
        return higherPeak;
    }
}
