package cat.itb.joseZaquinaula7e4.dam.m03.uf2.recursivity;

import cat.itb.joseZaquinaula7e4.dam.m03.uf1.arrays.ArrayReader;

import java.util.Locale;
import java.util.Scanner;

public class ArrayMaxValueRecursive {
    public static void main(String[] args) {
        //VARs
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        int[] nums;
        //actions
        //arrayReader
        nums=ArrayReader.scannerReadIntArray(scanner);
        //
        int[] nounums=new int[nums.length-1];
        for (int i = 1; i < nums.length; i++) {
            nounums[i-1]=nums[i];
        }
        System.out.println(arrayMaxValueRecursive(nums[0],nounums));

    }

    private static int arrayMaxValueRecursive(int num,int[] nums) {
        if(nums.length -1 == 0)
            return Math.max(nums[0],nums[1]);
        //make the new array
        int[] nounums= new int[nums.length-1];
        for (int i = 1; i < nums.length; i++) {
            nounums[i-1]=nums[i];
        }

        return arrayMaxValueRecursive(nums[0],nounums);
    }
}
