package cat.itb.joseZaquinaula7e4.dam.m03.uf2.recursivity;

import java.util.Locale;
import java.util.Scanner;

public class DotLineRecursive {
    /**
     * Imprime en la consola n puntos
     * @param args
     */
    public static void main(String[] args) {
        //VARs
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        //actions
        int numPunts=scanner.nextInt();
        System.out.println(doLineRecursive(numPunts));
    }

    /**
     * imprime un numero de puntos de manera recursiva
     * @param numPunts numero de puntos a imprimir
     * @return puntos
     */
    private static String doLineRecursive(int numPunts) {
        if(numPunts ==0)
            return "";
        return "."+doLineRecursive(numPunts-1);
    }
}
