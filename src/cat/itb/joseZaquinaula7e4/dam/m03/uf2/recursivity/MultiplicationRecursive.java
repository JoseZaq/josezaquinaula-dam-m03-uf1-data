package cat.itb.joseZaquinaula7e4.dam.m03.uf2.recursivity;

import java.util.Locale;
import java.util.Scanner;

public class MultiplicationRecursive {
    /**
     * Calcula el producte de dos naturals mitjançant la suma i crides recursives.
     *     input: 5 3 output: 15
     * @param args
     */
    public static void main(String[] args) {
        //VARs
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        int a=scanner.nextInt(),b=scanner.nextInt();
        //actions
        System.out.println(multiplicationRecursive(a,b));

    }
    /**
     * Retorna la multiplicacion de a y b por el metodo recursivo
     * @param a num1
     * @param b num2
     * @return retorna a*b
     */
    private static int multiplicationRecursive(int a,int b) {
        //M(A,B) = A+M(A,B-1)
        if(b==0)
            return 0;
        return a+multiplicationRecursive(a,b-1);
    }

}
