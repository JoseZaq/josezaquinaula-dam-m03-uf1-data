package cat.itb.joseZaquinaula7e4.dam.m03.uf2.dataclasses;

public class Product {
    //VARs
    private String nom;
    private double price;
    private int count;
    //contructor

    public Product(String nom, double price) {
        this.nom = nom;
        this.price = price;
        this.count = 1;
    }

    public Product(String nom, double price, int count) {
        this.nom = nom;
        this.price = price;
        this.count = count;
    }
    //getters and setters
    public double getTotalPrize(){
        return price*count;
    }
    public int getCount() {
        return count;
    }

    public String getNom() {
        return nom;
    }

    public double getPrice() {
        return price;
    }
}
