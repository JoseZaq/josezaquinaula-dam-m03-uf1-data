package cat.itb.joseZaquinaula7e4.dam.m03.uf2.dataclasses;

public class Student {
    //VARs
    private String nom;
    private Escuela escuela;
    //contructor
    public Student(String nom, Escuela escuela) {
        this.nom = nom;
        this.escuela = escuela;
    }
    //functions

    @Override
    public String toString() {
        return escuela.toString() + '\n' + nom;
    }

    //getters
    public String getNom() {
        return nom;
    }

    public Escuela getEscuela() {
        return escuela;
    }

}
