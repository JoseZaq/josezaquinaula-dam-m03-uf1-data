package cat.itb.joseZaquinaula7e4.dam.m03.uf2.dataclasses;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;

public class FromAvgSallaryInfo {
    private static double sumSal =0;

    /*
        En una empresa ens diuen que volen coneixer el salari mitjà dels seus treballadors, i veure quin cobra per sota.
         L'usuari introduirà una línia per cada treballador amb el seu sou, i el seu nom.
          Quan el sou sigui -1 és que ja ha acabat.
        Un cop introduits els treballadors printa per pantalla el nom dels treballadors que cobren per sota
         del sou mitjà.
         */
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in).useLocale(Locale.UK);
        List<Trabajador> trabajadorList = new ArrayList<Trabajador>();
        double sueldo=scanner.nextDouble();
        //while de obtencion: ingresar variables hasta ingresar -1
        while (sueldo != -1){
            String nom=scanner.next();
            trabajadorList.add(new Trabajador(nom,sueldo));
            sueldo=scanner.nextDouble();
        }
        double mediaSal=0;
        //sumar todos los salarios
        for (int i = 0; i < trabajadorList.size(); i++) {
            sumSal +=trabajadorList.get(i).getSueldo();
        }
        mediaSal = sumSal /trabajadorList.size();
        //obtener el salario mas bajo de la lista de trebajadores
        for (int i = 0; i < trabajadorList.size(); i++) {
            if( mediaSal > trabajadorList.get(i).getSueldo()){
                mediaSal=trabajadorList.get(i).getSueldo();
                System.out.println(trabajadorList.get(i).getNom());
            }
        }
    }
}
