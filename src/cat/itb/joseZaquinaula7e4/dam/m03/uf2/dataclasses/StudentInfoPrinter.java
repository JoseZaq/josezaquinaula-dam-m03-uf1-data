package cat.itb.joseZaquinaula7e4.dam.m03.uf2.dataclasses;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;

public class StudentInfoPrinter {
    //Llegeix la informació de diverses escoles i de diversos estudiant de cada escola
    // i printa'n les dades. L'usuari primer introduirà el nombre d'escoles
    // i la informació de les escoles. Després el número d'estudiants
    // i la informació dels estudiants (escola i nom complert).
    // Per indicar l'escola, indicarà el número en que s'ha introduit en la primera llista
    public static void main(String[] args) {
        Scanner scanner= new Scanner(System.in).useLocale(Locale.UK);
        int numEscolas = scanner.nextInt();
        String trash;
        Escuela[] escuelas = new Escuela[numEscolas];
        trash = scanner.nextLine();
        for (int i = 0; i < numEscolas; i++) {
            String name = scanner.nextLine();
            String adress = scanner.nextLine();
            String postalCOde = scanner.nextLine();
            String city = scanner.nextLine();
            escuelas[i]=new Escuela(name,adress,postalCOde,city);
        }
        int numStudents = scanner.nextInt();
        Student[] students = new Student[numStudents];
        for (int i = 0; i < numStudents; i++) {
            int indexEscuela = scanner.nextInt();
            trash = scanner.nextLine();
            String nom = scanner.nextLine();
            students[i] = new Student(nom,escuelas[indexEscuela]);
        }
        for (int i = 0; i < numStudents; i++) {
            System.out.println(students[i].toString()+"\n--------------------");
        }
    }
}
