package cat.itb.joseZaquinaula7e4.dam.m03.uf2.dataclasses;

public class Escuela {
    //datos de una escuela
    //VARs
    private String nom;
    private String adress;
    private String postalCode;
    private String City;
    //constructor

    public Escuela(String nom, String adress, String postalCode, String city) {
        this.nom = nom;
        this.adress = adress;
        this.postalCode = postalCode;
        this.City = city;
    }
    //function

    @Override
    public String toString() {
        return
                 nom + '\n' +
                         "adreça: "+adress + '\n' + "codipostal: " +postalCode + '\n' + "ciutat: " +City;
    }

    //getters

    public String getNom() {
        return nom;
    }

    public String getAdress() {
        return adress;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public String getCity() {
        return City;
    }
}

