package cat.itb.joseZaquinaula7e4.dam.m03.uf2.dataclasses;

public class StudentGrade {
    /* almacena datos de las notas de un estudiante*/
    //VARs
    private final String nom;
    private double exerciceGrade;
    private double examGrade;
    private double proyectGrade;
    //contructor

    public StudentGrade(String nom, double exerciceGrade, double examGrade, double proyectGrade) {
        this.nom = nom;
        this.exerciceGrade = exerciceGrade;
        this.examGrade = examGrade;
        this.proyectGrade = proyectGrade;
    }

    //metodos
    public double getFinalGrade(){
        return 0.3*exerciceGrade+0.3*examGrade+0.4*proyectGrade;
    }
    //getters and setters
    public String getNom() {
        return nom;
    }

    public double getExerciceGrade() {
        return exerciceGrade;
    }

    public double getExamGrade() {
        return examGrade;
    }

    public double getProyectGrade() {
        return proyectGrade;
    }
}
