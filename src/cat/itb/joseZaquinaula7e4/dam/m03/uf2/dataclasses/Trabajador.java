package cat.itb.joseZaquinaula7e4.dam.m03.uf2.dataclasses;

public class Trabajador {
    /*trabajadores*/
    //VARs
    private String nom;
    private double sueldo;
    //contructor

    public Trabajador(String nom, double sueldo) {
        this.nom = nom;
        this.sueldo = sueldo;
    }
    //getters

    public String getNom() {
        return nom;
    }

    public double getSueldo() {
        return sueldo;
    }
}
