package cat.itb.joseZaquinaula7e4.dam.m03.uf2.dataclasses;

import java.util.Locale;
import java.util.Scanner;

public class MultiProductInfoPrinter {
    /*Llegeix el nom i preu de diferents productes producte,
     tal com s'indica en l'exercici anterior. L'usuari primer introduirà el número de productes a llegir
    Printa tots els productes.
    intput:
    3
    Taula
    232.32
    Cadira
    90.90
    Bolígraf
    76.22
    output:
    El producte Taula val 232.32€
    El producte Cadira val 90.90€
    El producte Bolígraf val 76.22€
    */
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in).useLocale(Locale.UK);
        int numProducts= scanner.nextInt();
        Product[] products = new Product[numProducts];
        for (int i = 0; i < numProducts; i++) {
            String nomP=scanner.next();
            double priceP=scanner.nextDouble();
            products[i]=new Product(nomP,priceP);
        }
        for (int i = 0; i < products.length; i++) {
            System.out.printf("El producte %s val %.2f€\n", products[i].getNom(), products[i].getPrice());
        }
    }
}
