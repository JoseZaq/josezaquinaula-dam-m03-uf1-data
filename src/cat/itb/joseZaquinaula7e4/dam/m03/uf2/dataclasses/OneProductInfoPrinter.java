package cat.itb.joseZaquinaula7e4.dam.m03.uf2.dataclasses;

import java.util.Locale;
import java.util.Scanner;

public class OneProductInfoPrinter {
    /*Llegeix el nom i preu d'un producte guardant la info en una classe -Product(String name, double price)-.
    Imprimeix-lo amb el següent format:
    input:
        Taula
        232.32
    output:
        El producte Taula val 232.32€
    */
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in).useLocale(Locale.UK);
        String nomProduct=scanner.next();
        double priceProduct=scanner.nextDouble();
        Product product= new Product(nomProduct,priceProduct);
        System.out.printf("El producte %s val %.2f€",product.getNom(),product.getPrice());
    }
}
