package cat.itb.joseZaquinaula7e4.dam.m03.uf2.dataclasses;

import java.util.Locale;
import java.util.Scanner;

public class GradeCalculator {
    /**
     * En una UF d'un mòdul ens han dit que la nostra nota final serà la
     * $: KaTeX parse error: Can't use function ' in math mode at position 1:
     * $̲ nota = nota_exercicis * 0.3 + nota_examen * 0.3 + nota_projecte * 0.4 $:
     * KaTeX parse error: Can't use function ' in math mode at position 1: $̲
     * Fes un programa que donades les notes parcials calculi les notes finals.
     * Usa una classe per guardar les dades de les notes parcials.
     * L'usuari primer introduirà el número de estudiants a llegir.
     * input:
     * 3
     * Ot 8.2 4.5 6.1
     * Mar 9.5 8.7 6.4
     * Ona 2.3 3.4 2.1
     * output:
     * Ot: 6.3
     * Mar: 8.0
     * Ona: 2.6
     */
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in).useLocale(Locale.UK);
        StudentGrade[] studentGrades = getStudents(scanner);
        printStudentsGrade(studentGrades);
    }

    public static void printStudentsGrade(StudentGrade[] studentGrades) {
        System.out.println("----- Estudiants -----");
        for (StudentGrade studentGrade : studentGrades) {
            System.out.printf("%s: %.1f\n", studentGrade.getNom(), studentGrade.getFinalGrade());
        }
        System.out.println("----- Resum -----");
        int apr = getAprovats(studentGrades);
        int sus = getSuspensos(studentGrades);
        System.out.printf("Aprovats: %d\nSuspesos: %d\n",apr,sus);
    }

    public static int getSuspensos(StudentGrade[] studentGrades) {
        int sus = 0;
        for(StudentGrade studentGrade : studentGrades){
            if(studentGrade.getFinalGrade() < 5)
                sus++;
        }
        return sus;

    }

    public static int getAprovats(StudentGrade[] studentGrades) {
        int apr = 0;
        for(StudentGrade studentGrade : studentGrades){
            if(studentGrade.getFinalGrade() > 5)
                apr++;
        }
        return apr;
    }

    public static StudentGrade[] getStudents(Scanner scanner) {
        int studentsNums= scanner.nextInt();
        StudentGrade[] studentGrades = new StudentGrade[studentsNums];
        for (int i = 0; i <studentGrades.length; i++) {
            String nom=scanner.next();
            double exercice=scanner.nextDouble();
            double exam=scanner.nextDouble();
            double proyect= scanner.nextDouble();
            studentGrades[i] = new StudentGrade(nom,exercice,exam,proyect);
        }
        return studentGrades;
    }
}
