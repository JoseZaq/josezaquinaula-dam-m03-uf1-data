package cat.itb.joseZaquinaula7e4.dam.m03.uf2.dataclasses;

import java.util.List;
import java.util.Locale;
import java.util.Scanner;

public class SchoolInfoPrinter {
    //Llegeix la informació d'una escola (name, adress, postalCode, city)
    // i posa-la en una classe. Imprimeix les dades amb el format de l'exemple.
    // Crea les següents funcions:
    //public static readSchool(Scanner scanner);
    //public static printSchool(School school);

    public static Escuela readSchool(Scanner scanner){
        Escuela escuela;
        String name = scanner.nextLine();
        String adress = scanner.nextLine();
        String postalCOde = scanner.nextLine();
        String city = scanner.nextLine();
        return  escuela = new Escuela(name,adress,postalCOde,city);
    }
    public static void printSchool(Escuela escuela){
        System.out.printf("%s\nadreça: %s\ncodipostal: %s\nciutat: %s\n",escuela.getNom(),escuela.getAdress(),escuela.getPostalCode(),escuela.getCity());
    }
    public static void main(String[] args) {
        Scanner scanner= new Scanner(System.in).useLocale(Locale.UK);
        Escuela escuela=readSchool(scanner);
        printSchool(escuela);
    }
}
