package cat.itb.joseZaquinaula7e4.dam.m03.uf2.staticfunctions;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class IntegerLists {
    public static List<Integer> readIntegerList(Scanner scanner){
        List<Integer> list = new ArrayList<>();
        int entero = scanner.nextInt();
        while(entero != -1){
            list.add(entero);
            entero= scanner.nextInt();
        }
        return list;
    }
    public static int min(List<Integer> list){
        int min=list.get(0);
        for(int i=1;i<list.size();i++){
            if(min > list.get(i)){
                min =list.get(i);
            }
        }
        return min;
    }
    public static int max(List<Integer> list){
        int max=list.get(0);
        for(int i=1;i<list.size();i++){
            if(max < list.get(i)){
                max =list.get(i);
            }
        }
        return max;
    }
    public static float avg(List<Integer> list){
        float avg=0;
        for (Integer integer : list) {
            avg += integer;
        }
        avg = avg / list.size();
        return avg;
    }
    public static int sum(List<Integer> list){
        int sum=0;
        for(int i=0;i<list.size();i++){
                sum +=list.get(i);
        }
        return sum;
    }
}
