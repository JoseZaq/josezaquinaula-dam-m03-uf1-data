package cat.itb.joseZaquinaula7e4.dam.m03.uf2.staticfunctions;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;

public class IntListReaderSample {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.UK);
        List<Integer> list = IntegerLists.readIntegerList(scanner);
        System.out.println(list);
    }
}
