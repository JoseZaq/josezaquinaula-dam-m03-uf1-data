package cat.itb.joseZaquinaula7e4.dam.m03.uf2.staticfunctions;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;

public class OldestStudent {
    /*Fes un programa que donat diferents edats dels estudiants, retorni l'edat de l'estudiant amb més edat.
     */
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in).useLocale(Locale.UK);
        List<Integer> listaDeEdades = new ArrayList<>();
        int input = scanner.nextInt();
        while(input != -1){
            listaDeEdades.add(input);
            input = scanner.nextInt();
        }
        int max = IntegerLists.max(listaDeEdades);
        System.out.printf("L'alumne més gran té %d anys\n",max);
    }
}
