package cat.itb.joseZaquinaula7e4.dam.m03.uf2.staticfunctions;

import cat.itb.joseZaquinaula7e4.dam.m03.uf1.mix.DniLetterCalculator;

import java.util.Locale;
import java.util.Scanner;

public class PersonValidator {
    /**
            * Returns true if is a valid phone number (composed only by digits, spaces or +)
     * @param phone
     * @return true if valid
     */
    public static boolean isValidPhoneNumber(String phone){
        // TODO
        for(int i=0;i<phone.length()-1;i++){
            char letter=phone.charAt(i);
            if(Character.isAlphabetic(letter)) return false;
        }
        return true;
    }

    /**
     * Returns true if is a valid person name (composed only characters and starts with upper case)
     * @param personName
     * @return true if valid
     */
    public static boolean isValidPersonName(String personName){
        // TODO
        for(int i=0;i<personName.length();i++){
            if(!Character.isUpperCase(personName.charAt(0))) return false;
            if(!Character.isAlphabetic(personName.charAt(i))) return false;
        }
        return true;
    }

    /**
     * Returns true if is a valid dni (including correct letter)
     * @param dni
     * @return true if valid
     */
    public static boolean isValidDni(String dni){
        // TODO
        String dniWithoutLetter = dni.substring(0,dni.length()-1);
        for(int i=0;i<dni.length();i++){
            if(!Character.isDigit(dni.charAt(i)) && i != dni.length()-1) return false;
        }
        if(dni.charAt(dni.length()-1) != DniLetterCalculator.getDniLetter(dniWithoutLetter)) return false;
        return true;
    }

    /**
     * Returns true if is a valid postalCode
     * @param postalCode
     * @return true if valid
     */
    public static boolean isValidPostalcode(String postalCode){
        // TODO
        if(postalCode.length() != 5) return false;
        return true;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.UK);
        String nom=scanner.next();
        String dni=scanner.next();
        String phone = scanner.next();
        String postalCode = scanner.next();
        if(isValidDni(dni) && isValidPersonName(nom) && isValidPostalcode(postalCode) && isValidPhoneNumber(phone))
            System.out.println("Dades correctes");
        else System.out.println("Hi ha alguna dada incorrecta");
    }
}
