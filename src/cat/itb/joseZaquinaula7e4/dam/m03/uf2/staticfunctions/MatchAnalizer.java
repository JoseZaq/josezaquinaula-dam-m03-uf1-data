package cat.itb.joseZaquinaula7e4.dam.m03.uf2.staticfunctions;

import java.util.*;

public class MatchAnalizer {
    /** Volem fer un programa que analitzi els resultats d'un partit de basquet.
     Hem de tenir en compte que en un partit de basquet es poden fer 1, 2 o 3 punts.
     Donat els diferents resultats analitza que està passant.
     L'usuari introduirà primer el nom dels dos equips i després el resultat actual
     Input:
         Granotes
         Flors
         0 2
         0 5
         2 5
         3 5
         3 7
         -1
     Output:
         Cistella de Granotes
         Triple de Granotes
         Cistella de Flors
         Tir lliure de Flors
         Cistella de Granotes
         Guanaya Granotes**/
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in).useLocale(Locale.UK);
        List<Integer> equipo1 = new ArrayList<>(Collections.singletonList(0));
        List<Integer> equipo2 = new ArrayList<>(Collections.singletonList(0));
        String equipo1Name = scanner.next();
        String equipo2Name = scanner.next();
        String ganador;
        int input = scanner.nextInt();
        while( input != -1){
            input=input-IntegerLists.sum(equipo2);
            if(input != 0){
                equipo2.add(input);
                System.out.printf("%s de %s\n",basketPointName(equipo2.get(equipo2.size()-1)),equipo2Name);
            }
            input= scanner.nextInt();
            input=input-IntegerLists.sum(equipo1);
            if(input!=0){
                equipo1.add(input);
                System.out.printf("%s de %s\n",basketPointName(equipo1.get(equipo1.size()-1)),equipo1Name);
            }
            input=scanner.nextInt();
        }
        if(IntegerLists.sum(equipo1) > IntegerLists.sum(equipo2))
             ganador=equipo1Name;
        else ganador= equipo2Name;
        System.out.printf("Guanaya %s\n",ganador);
    }

    private static String basketPointName(int input) {
        switch(input){
            case 1:
                return "Tir lliure";
            case 2:
                return "Cistella";
            case 3:
                return "Triple";
            default:
                break;
        }
        return "";
    }
}
