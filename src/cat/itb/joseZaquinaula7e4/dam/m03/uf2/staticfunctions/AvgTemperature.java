package cat.itb.joseZaquinaula7e4.dam.m03.uf2.staticfunctions;

import java.sql.ClientInfoStatus;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;

public class AvgTemperature {
    /*Fes un programa que donades diferents temperatures (sense decimals), retorni la temperatura mitjana (amb decimals)
     */
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in).useLocale(Locale.UK);
        List<Integer> listaTemp = new ArrayList<>();
        int input = scanner.nextInt();
        while(input != -1){
            listaTemp.add(input);
            input = scanner.nextInt();
        }
        float avg = IntegerLists.avg(listaTemp);
        System.out.printf("Ha fet %.1f graus de mitjana\n",avg);
    }

}
