package cat.itb.joseZaquinaula7e4.dam.m03.uf2.staticfunctions;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;

public class StudentStats {
    /** Després de fer un exàmen un professor vol veure l'estat general de la classe.
     Vol coneixer la nota mínima, la màxima i la mitjana dels seus alumnes.
     Fes un petit programa que li solucioni la tasca (les notes de l'exàmen no tenen decimals).
     input:
     * enteros
     output:
     * Nota mínima:  3
     * Nota màxima:  8
     * Nota mitjana: 5.4
     **/
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in).useLocale(Locale.UK);
        List<Integer> notasExamen = new ArrayList<>();
        int input = scanner.nextInt();
        while(input != -1){
            notasExamen.add(input);
            input = scanner.nextInt();
        }
        System.out.printf("Nota mínima:  %d\n"+
                "Nota màxima:  %d\n" +
                "Nota mitjana: %.1f",IntegerLists.min(notasExamen),IntegerLists.max(notasExamen),IntegerLists.avg(notasExamen));
    }
}
