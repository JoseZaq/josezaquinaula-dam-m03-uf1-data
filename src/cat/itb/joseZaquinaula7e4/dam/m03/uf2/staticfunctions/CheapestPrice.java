package cat.itb.joseZaquinaula7e4.dam.m03.uf2.staticfunctions;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;

public class CheapestPrice {
    /*Defineix la funció min dins de la classe IntegerLists que donada una llista d'enters, retorni el valor mínim.

    public static int min(List<Integer> list)
    Fes un programa que donada la llista de preus (sense decimals) de tot de productes, retorni el preu del producte més baix
    */
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in).useLocale(Locale.UK);
        List<Integer> listaDeCompras = new ArrayList<>();
        int input = scanner.nextInt();
        while(input != -1){
            listaDeCompras.add(input);
            input = scanner.nextInt();
        }
        int min = IntegerLists.min(listaDeCompras);
        System.out.printf("El producte més econòmic val: %d€",min);
    }
}
