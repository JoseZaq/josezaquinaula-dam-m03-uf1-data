package cat.itb.joseZaquinaula7e4.dam.m03.uf2.classfun;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;

public class MostDistantAge {
    //Volem fer un programa que donada les edats de totes les persones d'un concert
    // ens digui la diferència entre la persona més gran i la més jove.
    // Per fer-ho l'usuari introduirà les edats de les persones. Quan ja hagi acabat introduirà -1
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.UK);
        //VARs
        List<Person> personList = new ArrayList<Person>();
        //leer edades
        while(true){
            int age = scanner.nextInt();
            if(age == -1) break;
            personList.add(new Person(age));
        }
        //hallar el menor y mayor de edad
        int gran= personList.get(0).getAge();
        int menor = personList.get(0).getAge();
        for (int i = 1; i < personList.size(); i++) {
            if(gran < personList.get(i).getAge()) gran = personList.get(i).getAge();
            if ( menor > personList.get(i).getAge()) menor = personList.get(i).getAge();
        }
        //diferencia entre el mas grande y el pequeño de las personas en edad y printarlo
        int difAges = gran -menor;
        System.out.println(difAges);
    }
}
