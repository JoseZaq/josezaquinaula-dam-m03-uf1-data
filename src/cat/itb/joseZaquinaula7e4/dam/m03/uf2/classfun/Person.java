package cat.itb.joseZaquinaula7e4.dam.m03.uf2.classfun;

public class Person {
    //VARs
    private int age;
    //contructor

    public Person(int age) {
        this.age = age;
    }
    //getters

    public int getAge() {
        return age;
    }
}
