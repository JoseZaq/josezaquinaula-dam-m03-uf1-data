package cat.itb.joseZaquinaula7e4.dam.m03.uf2.classfun;

import java.util.Locale;
import java.util.Scanner;

public class RectangleSize {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.UK);
        int numRectagles = scanner.nextInt();
        Rectangle[] rectangles = new Rectangle[numRectagles];
        for (int i = 0; i < numRectagles; i++) {
            double width = scanner.nextDouble();
            double height = scanner.nextDouble();
            rectangles[i] = new Rectangle(width,height);
        }
        for (Rectangle rec :
                rectangles) {
            double area = rec.getHeight() * rec.getWidth();
            double per = rec.getHeight() + rec.getWidth();
            System.out.printf("Un rectangle de %.1f x %.1f té %.1f d'area i %.0f de perímetre.\n",rec.getWidth(),rec.getHeight(),area,per);
        }
    }

    /**
     * crea objetos tipo rectangles a partir de datos del usuario
     * @param scanner objeto tipo Scanner para leer las entradas
     * @return array tipo rectangle con los rectangulos creados
     */
    public static Rectangle[] makeRectangles(Scanner scanner){
        int numRectagles = scanner.nextInt();
        Rectangle[] rectangles = new Rectangle[numRectagles];
        for (int i = 0; i < numRectagles; i++) {
            double width = scanner.nextDouble();
            double height = scanner.nextDouble();
            rectangles[i] = new Rectangle(width,height);
        }
        return rectangles;
    }


}
