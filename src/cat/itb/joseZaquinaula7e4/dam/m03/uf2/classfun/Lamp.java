package cat.itb.joseZaquinaula7e4.dam.m03.uf2.classfun;

import java.util.Scanner;

public class Lamp {
    //Implementa un programa que simule una lampara, amb les accions:
    /**
     * TURN ON: Encén la lampara
     * TURN OFF: Apaga la lampara
     * TOGGLE: Canvia l'estat de la lampara
     */
    //VARs
    private boolean estado; //on: true, off:false
    //contructor

    public Lamp() {
        estado=false;
    }
    //fuctions
    public void on(){
        estado = true;
    }
    public void off(){
        estado= false;
    }
    public void toggle(){
            estado=!estado;
    }
    //main
    public static void main(String[] args) {
        //VARs
        Scanner scanner = new Scanner(System.in);
        Lamp lamp = new Lamp();
        //accions
        while(true){
            String input = scanner.nextLine();
            if(input.equals("END")) break;
            lampEstate(lamp,input);
            System.out.println(lamp.getEstado());
        }

    }

    /**
     *
     * @param lamp : object Lamp
     * @param input : input user to generate an action
     */
    private static void lampEstate(Lamp lamp,String input) {
        switch (input) {
            case "TURN OFF":
                lamp.off();
                break;
            case "TURN ON":
                lamp.on();
                break;
            case "TOGGLE":
                lamp.toggle();
                break;
        }
    }

    private boolean getEstado() {
        return estado;
    }
}
