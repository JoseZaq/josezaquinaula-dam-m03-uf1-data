package cat.itb.joseZaquinaula7e4.dam.m03.uf2.classfun;

import cat.itb.joseZaquinaula7e4.dam.m03.uf2.dataclasses.Product;

import java.util.Locale;
import java.util.Scanner;

public class BuyList {
    //Fes un programa que ens ajudi a crear una llista de la compra. L'suauri introduirà primer el nombre de productes.
    // Per cada producte el nom, el preu i la quantitat que en vol comprar. Quan acabi imprimeix el resum de la compra.
    //VARs
    public static void main(String[] args) {
        Scanner scanner =new Scanner(System.in).useLocale(Locale.UK);
        //VARs
        int numProducts = scanner.nextInt();
        Product[] products = new Product[numProducts];
        double totalPrize = 0;
        //fori para crear todos los productos con nom,price,count
        for (int i = 0; i < numProducts; i++) {
            int count = scanner.nextInt();
            String nom = scanner.next();
            double price = scanner.nextDouble();
            products[i] = new Product(nom,price,count);
        }
        //printar el buyList (lista de compras)
        System.out.println("-------- Compra --------");
        for (Product pr: products) {
            totalPrize += pr.getTotalPrize(); //suma todos los productos, saca el total price
            System.out.printf("%d %s (%.2f€) - %.2f€\n",pr.getCount(),
                    pr.getNom(),pr.getPrice(),pr.getTotalPrize());
        }
        System.out.printf("-------------------------\n" + "Total: %.2f€\n" +
                "-------------------------",totalPrize);
    }
}
