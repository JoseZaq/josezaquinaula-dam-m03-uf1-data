package cat.itb.joseZaquinaula7e4.dam.m03.uf2.classfun;

import java.util.ArrayList;
import java.util.List;

public class Camping {
    //parcelas
    //VARs
    private int numPersonas;
    private List<Reserva> reservaList ;
    //constructor

    public Camping() {
        reservaList = new ArrayList<Reserva>();
        numPersonas=0;
    }
    //FUNCTIONs
    public void makeReserva(Reserva reserva){
        numPersonas+=reserva.getNumPersonas();
        reservaList.add(reserva);
    }
    public void removeReserva(String nom ){
        //busca cual tiene el nombre de reserva y lo borra
        for (int i = 0; i < reservaList.size(); i++) {
            if(reservaList.get(i).getNomReserva().equals(nom)) {
                numPersonas-=reservaList.get(i).getNumPersonas();
                reservaList.remove(i);
                return;
            }
        }
    }

    public void printInfo(){
        System.out.printf("parcel·les: %d\npersones: %d\n",reservaList.size(),numPersonas);
    }
    //getters

    public int getNumPersonas() {
        return numPersonas;
    }

    public List<Reserva> getReservaList() {
        return reservaList;
    }
}
