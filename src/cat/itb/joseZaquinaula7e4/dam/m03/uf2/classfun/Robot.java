package cat.itb.joseZaquinaula7e4.dam.m03.uf2.classfun;

public class Robot {
    /*
     * DALT: El robot és mou cap a dalt (tenint en compte la velocitat).
     * BAIX: El robot és mou cap a baix (tenint en compte la velocitat).
     * DRETA: El robot és mou cap a la dreta (tenint en compte la velocitat).
     * ESQUERRA: El robot és mou cap a la esquerra (tenint en compte la velocitat).
     * ACCELERAR: El robot augmenta en 0.5 la velocitat. La velocitat màxima és 10.
     * DISMINUIR: El robot augmenta en 0.5 la velocitat. La velocitat mínima és 0.
     * POSICIO: El robot imprimeix la seua posició.
     * VELOCITAT: El robot imprimeix la seua velocitat.
     */
    //VARs
    private double x;
    private double y;
    private double speedUp;
    //CONTRUCTORes

    public Robot() {
        //inicializar
        x=0;
        y=0;
        speedUp=1;
    }
    //FUNCTIONS
    public void moveUp(){
        y+=speedUp;
    }
    public void moveDown(){
        y-= speedUp;
    }
    public void moveLeft(){
        x+=speedUp;
    }
    public void moveRight(){
        x-=speedUp;
    }
    public void speedUp(){
        if(speedUp +0.5 <= 10)
            speedUp+=0.5;
    }
    public void speedDown(){
        if(speedUp - 0.5 >= 0)
            speedUp-=0.5;
    }
    //getters
    public  double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public  double getSpeed() {
        return speedUp;
    }
}
