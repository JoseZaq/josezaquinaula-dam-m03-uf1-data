package cat.itb.joseZaquinaula7e4.dam.m03.uf2.classfun;

import java.util.Scanner;

public class Rectangle {
    // guarda width and height de un rectangulo
    //VARs
    private double width;
    private double height;
    //contructor
    public Rectangle(double width, double height) {
        this.width = width;
        this.height = height;
    }
    //functions
    public static Rectangle makeRectangle(Scanner scanner){
        double width = scanner.nextDouble();
        double height = scanner.nextDouble();
        return new Rectangle(width,height);
    }
    //getters

    /**
     * calcula el area de unr ectangulo
     * @return variable tipo double que es el area de un retangulo
     */
    public double getArea(){
        return height * width;
    }

    /**
     * calcula el perimetro de un rectangulo
     * @return variable double con el perimetro de un rectangulo
     */
    public double getPerimeter(){
        return height + width;
    }

    public double getWidth() {
        return width;
    }

    public double getHeight() {
        return height;
    }

    @Override
    public String toString() {
        return String.format("Un rectangle de %.1f x %.1f té %.1f d'area i %.2f de perímetre.\n",width,
                height,getArea(),getPerimeter());
    }
}
