package cat.itb.joseZaquinaula7e4.dam.m03.uf2.classfun;

import java.util.Locale;
import java.util.Scanner;

public class CampSiteOrganizer {
    //VARs
    private static Camping camping = new Camping();

    public static void main(String[] args) {
        //Volem fer un petit programa per gestionar un camping.
        // Volem tenir controlat quantes parceles tenim pelenes i quanta gent tenim.
        //Quan s'ocupi una parcel·la l'usuari introduirà el número de persones i el nom de la reserva
        //VARs
        Scanner scanner =new Scanner(System.in).useLocale(Locale.UK);
        //init

        while(true){
            String accion = scanner.next();
            if(accion.equals("END")) break;
            reservaActions(accion,scanner);
            camping.printInfo();
        }

    }

    private static void reservaActions(String accion,Scanner scanner) {
        switch (accion){
            case "ENTRA":
                int numPersonas = scanner.nextInt();
                String nom=scanner.next();
                camping.makeReserva(new Reserva(numPersonas,nom));
                break;
            case "MARXA":
                nom =scanner.next();
                camping.removeReserva(nom);
                break;
        }
    }
}
