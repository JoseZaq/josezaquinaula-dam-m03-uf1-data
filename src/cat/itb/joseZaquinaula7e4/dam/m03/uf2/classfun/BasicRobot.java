package cat.itb.joseZaquinaula7e4.dam.m03.uf2.classfun;

import java.text.DecimalFormat;
import java.util.Locale;
import java.util.Scanner;

public class BasicRobot {
    /**
     * ROBOT CON DIFERENTES ACCIONES:
     * Fes un programa que permeti moure un robot en un plà 2D.
     * El robot ha de guardar la seua posició (X, Y) i la velocitat actual.
     * Per defecte, la posició serà (0, 0) i la velocitat és 1.
     * La velocitat indica la distància que recorre el robot en cada acció.
     * El robot té les següents accions:
     * DALT: El robot és mou cap a dalt (tenint en compte la velocitat).
     * BAIX: El robot és mou cap a baix (tenint en compte la velocitat).
     * DRETA: El robot és mou cap a la dreta (tenint en compte la velocitat).
     * ESQUERRA: El robot és mou cap a la esquerra (tenint en compte la velocitat).
     * ACCELERAR: El robot augmenta en 0.5 la velocitat. La velocitat màxima és 10.
     * DISMINUIR: El robot augmenta en 0.5 la velocitat. La velocitat mínima és 0.
     * POSICIO: El robot imprimeix la seua posició.
     * VELOCITAT: El robot imprimeix la seua velocitat.
     */
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        Robot robot = new Robot();
        String input = scanner.next();
        DecimalFormat df = new DecimalFormat("###.#");
        while(!input.equals("END")){
            switch (input) {
                case "DALT":
                    robot.moveUp();
                    break;
                case "BAIX":
                    robot.moveDown();
                    break;
                case "DRETA":
                    robot.moveRight();
                    break;
                case "ESQUERRA":
                    robot.moveLeft();
                    break;
                case "ACCELERAR":
                    robot.speedUp();
                    break;
                case "DISMINUIR":
                    robot.speedDown();
                    break;
                case "POSICIO":
                    System.out.printf("La posició del robot és (%s, %s)\n", df.format(robot.getX()), df.format(robot.getY()));
                    break;
                case "VELOCITAT":
                    System.out.printf("La velocitat del robot és %s\n", df.format(robot.getSpeed()));
                    break;
            }
            input = scanner.next();
        }
    }
}
