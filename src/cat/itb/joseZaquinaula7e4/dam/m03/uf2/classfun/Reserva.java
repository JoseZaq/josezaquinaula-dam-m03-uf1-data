package cat.itb.joseZaquinaula7e4.dam.m03.uf2.classfun;

public class Reserva {
    //reserva una parcela
    private int numPersonas;
    private String nomReserva;
    //contructor
    public Reserva(int numPersonas, String nomReserva) {
        this.numPersonas = numPersonas;
        this.nomReserva = nomReserva;
    }
    //getters
    public int getNumPersonas() {
        return numPersonas;
    }
    public String getNomReserva() {
        return nomReserva;
    }
}
