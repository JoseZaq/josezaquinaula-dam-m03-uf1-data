package cat.itb.joseZaquinaula7e4.dam.m03.uf2.generalexam;

public class AnimalSight {
    //VARs
    Animal animal;
    int views;
    //contructor
    public AnimalSight(Animal animal, int views) {
        this.animal = animal;
        this.views = views;
    }
    //getters and setters

    @Override
    public String toString() {
        return animal.toString() + ": " + views;
    }
}
