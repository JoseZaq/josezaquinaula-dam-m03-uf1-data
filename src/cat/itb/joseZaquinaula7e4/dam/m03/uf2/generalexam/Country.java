package cat.itb.joseZaquinaula7e4.dam.m03.uf2.generalexam;

/**
 * Amnistia Internacional ens ha demanat que els fem un petit progrograma. Volen tenir registres del diferents atacs a la llibrertat d'expressió realitzats a diferents països.
 *
 * L'usuari introduirà per cada país el su nom, número de casos detectats, i gravetat general. La gravetat general serà un nombre decimal entre el 0 i el 10. A més a més, de cada país es pot calcular la seva puntuació amb la formula:
 *
 * Puntuacio = Casos · Gravetat
 * Puntuacio=Casos⋅Gravetat
 * L'usuari introduirà la llista de països (primer la quantitat)
 * S'imprimirar la llista de paisos i un petit resum en el format indicat a continuació.
 * No pots imprimir la llista fins a haver llegit tots els països
 */
public class Country {
    //VARs
    String nom;
    int cases;
    float dangerous;
    //contructor
    public Country(String nom, int cases, float dangerous) {
        this.nom = nom;
        this.cases = cases;
        this.dangerous = dangerous;
    }
    //functions

    /**
     * function: p(casos) = casos
     * p = casos + p(cases-1);
     * @return
     */
    public float punctuationRecursivity(int cases){
        if(cases ==1)
            return dangerous;
        return dangerous+ punctuationRecursivity(cases-1);
    }

    @Override
    public String toString() {
        return nom + " - casos: " + cases + " - gravetat: " + dangerous + " - puntuació: " + punctuationRecursivity(cases);
    }
}
