package cat.itb.joseZaquinaula7e4.dam.m03.uf2.generalexam;

import java.util.Locale;
import java.util.Scanner;

public class EndangeredAnimalSights {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        int animalLenght=scanner.nextInt();
        Animal[] animalArray = new Animal[animalLenght];
        scanner.nextLine();
        for (int i = 0; i < animalArray.length; i++) {
            String cName= scanner.nextLine();
            String name = scanner.nextLine();
            animalArray[i] = new Animal(cName,name);
        }
        //
        int viewsLenght = scanner.nextInt();
        AnimalSight[] animalSights = new AnimalSight[viewsLenght];
        for (int i = 0; i < viewsLenght; i++) {
            int animal = scanner.nextInt();
            int views  =scanner.nextInt();
            animalSights[i] = new AnimalSight(animalArray[animal],views);
            animalArray[animal].addSights(views);
        }
        //
        printOutput(animalSights,animalArray);
    }

    private static void printOutput(AnimalSight[] animalSight, Animal[] animals) {
        System.out.println("--- Avistaments ---");
        for (int i = 0; i < animalSight.length; i++) {
            System.out.println(animalSight[i]);
        }
        System.out.println("--- Resum ---");
        for (int i = 0; i < animals.length; i++) {
            System.out.println(animals[i]+ ": "+animals[i].sights);
        }

    }

}
