package cat.itb.joseZaquinaula7e4.dam.m03.uf2.generalexam;

import java.util.Locale;
import java.util.Scanner;

public class LibertySpeechApp {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        int countriesLenght = scanner.nextInt();
        Country[] countriesArray = new Country[countriesLenght];
        for (int i = 0; i < countriesArray.length; i++) {
            String nom = scanner.next();
            int cases = scanner.nextInt();
            float dangerous  = scanner.nextFloat();
            countriesArray[i] = new Country(nom,cases,dangerous);
        }
        printCountries(countriesArray);
    }

    private static void printCountries(Country[] countriesArray) {
        System.out.println("---------- Paisos ----------");
        for (Country country : countriesArray) {
            System.out.println(country.toString());
        }
        System.out.println("---------- Resum ----------");
        System.out.println("Casos totals: "+ totalcases(countriesArray));
        System.out.println("País amb més casos: " + highestCasesCountry(countriesArray));
    }

    private static String highestCasesCountry(Country[] countriesArray) {
        int max = countriesArray[0].cases;
        int indexHighestCases = 0;
        for (int i = 1; i < countriesArray.length; i++) {
            if(countriesArray[i].cases > max){
                max=countriesArray[i].cases;
                indexHighestCases = i;
            }
        }
        return countriesArray[indexHighestCases].nom;
    }

    private static int totalcases(Country[] countriesArray) {
        int totalcases =0;
        for (Country country :
                countriesArray) {
            totalcases += country.cases;
        }
        return totalcases;
    }
}
