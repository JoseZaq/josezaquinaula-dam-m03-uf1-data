package cat.itb.joseZaquinaula7e4.dam.m03.uf2.generalexam;

public class Animal {
    //VARs
    String cientificName;
    String name;
    int sights;
    //constructor

    public Animal(String cientificName, String name) {
        this.cientificName = cientificName;
        this.name = name;
        sights =0;
    }
    //functions
    public void addSights(int sight){
        sights += sight;
    }
    //getters and setters

    @Override
    public String toString() {
        return cientificName + " - " + name;
    }

    public int getSights() {
        return sights;
    }
}
