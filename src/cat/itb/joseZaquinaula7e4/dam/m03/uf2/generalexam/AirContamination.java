package cat.itb.joseZaquinaula7e4.dam.m03.uf2.generalexam;

import cat.itb.joseZaquinaula7e4.dam.m03.uf1.arrays.ArrayReader;

import java.util.Locale;
import java.util.Scanner;

/**
 * Vols fer un estudi de la contaminació atmosfèrica a la teva escola. Per fer-ho instal·les un petit sensor
 * que cada hora enregistra la qualitat de l'aire actual. Per fer-ho obtens un valor enter entre 0 i 1000.
 * Un cop obtinguts tots els valors vols coneixer quans dies la contaminació han estat per sobre de la mitjana.
 */
public class AirContamination {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        int[] tSensor= ArrayReader.scannerReadIntArray(scanner);
        System.out.printf("Contaminació mitjana: %.2f\n" +
                "Dies amb contaminació superior: %d",temperaturePromedio(tSensor),highTempDays(tSensor));
    }

    private static float temperaturePromedio(int[] tSensor) {
        float tAvg = 0;
        for (int j : tSensor) {
            tAvg += j;
        }
        return tAvg / tSensor.length;
    }

    private static int highTempDays(int[] tSensor) {
        float tAvg = temperaturePromedio(tSensor);
        int highDays = 0;
        for (int j : tSensor) {
            if (j > tAvg)
                highDays++;
        }
        return highDays;
    }
}
