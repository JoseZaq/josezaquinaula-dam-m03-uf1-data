package cat.itb.joseZaquinaula7e4.dam.m03.uf4.generalexam.twitter;

public class Twitter {
    public static void main(String[] args) {
        Tweet tweet1 = new Tweet("iamdevloper","07 de gener","Remember, a few hours of trial and error can save you several minutes of looking at the README");
        Tweet tweet2 = new Tweet( "softcatala", "29 de març","Avui mateix, #CommonVoiceCAT segueix creixent 🚀:\n🗣️ 856 hores enregistrades\n✅ 725 de validades.\nSi encara no has participat, pots fer-ho aquí!");
        tweet1.print();
        tweet2.print();
        //part 2
        PollTweet pollTweet3 = new PollTweet("musicat","02 d'abril","Quina cançó t'agrada més?",
                "Comèdia dramàtica - La Fúmiga", "In the night - Oques Grasses", "Una Lluna a l'Aigua - Txarango"
        , "Esbarzers - La Gossa Sorda");
        Tweet tweet4 = new Tweet("ProgrammerJokes", "05 d'abril","Q: what's the object-oriented way to become weathy?\n\nA: Inheritance");
        //votos
        pollTweet3.vote(0);
        pollTweet3.vote(0);
        pollTweet3.vote(0);
        pollTweet3.vote(1);
        pollTweet3.vote(1);
        pollTweet3.vote(2);
        pollTweet3.vote(2);
        pollTweet3.vote(3);
        pollTweet3.vote(3);
        pollTweet3.vote(3);
        pollTweet3.vote(3);
        //print
        pollTweet3.print();
        tweet4.print();
    }
}
