package cat.itb.joseZaquinaula7e4.dam.m03.uf4.generalexam.gameenemies;

public class Troll extends Enemy{
    //vars
    int resistencia;
    //contructor
    public Troll(String nom, int vida,int resistencia) {
        super(nom, vida);
        this.resistencia = resistencia;
    }

    @Override
    public void attack1(int forca) {
        //resistencia > a la fuerza --> no afecta a la vida
        if(resistencia > forca) {
            return;
        } else{
            // resistencia - forca = fuerza total
            int fuerzaTotal = forca - resistencia;
            setVida(getVida() - fuerzaTotal);
        }
    }
}
