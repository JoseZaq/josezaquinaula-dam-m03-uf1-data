package cat.itb.joseZaquinaula7e4.dam.m03.uf4.generalexam.gameenemies;

public class Zombie extends Enemy{
    //vars
    String so;
    //contructor
    public Zombie(String nom, int vida,String so) {
        super(nom, vida);
        this.so = so;
    }
    //methods
    @Override
    public void attack1(int forca) {
        setVida(getVida() - forca);
        if( vida != 0)
            System.out.println(so);
    }

    //getters and setters

}
