package cat.itb.joseZaquinaula7e4.dam.m03.uf4.generalexam.twitter;

import java.util.List;

public class PollTweet extends Tweet{
    //vars
    private String[][] opcions;
    private int totalVotes;
    //contructor
    public PollTweet(String usuari, String data, String text,String option1,
                     String option2, String option3, String option4) {
        super(usuari, data, text);
        opcions = new String[4][2];
        opcions[0][0] = option1;
        opcions[1][0] = option2;
        opcions[2][0] = option3;
        opcions[3][0] = option4;
        for (int i = 0; i < opcions.length; i++) {
            opcions[i][1] = "0";
        }
        totalVotes = 0;
    }
    //methodes
    public void vote(int index){
        opcions[index][1] = String.valueOf(Integer.parseInt(opcions[index][1]) + 1);
        totalVotes ++;
    }

    @Override
    public String toString() {
        StringBuilder stringoptions = new StringBuilder();
        for (String[] opcion : opcions) {
            stringoptions.append(String.format("(%s/%d) %s\n", opcion[1], totalVotes, opcion[0]));
        }
        return super.toString() + stringoptions + "\n";

    }
}
