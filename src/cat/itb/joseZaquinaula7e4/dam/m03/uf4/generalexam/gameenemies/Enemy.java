package cat.itb.joseZaquinaula7e4.dam.m03.uf4.generalexam.gameenemies;

public abstract class Enemy {
    //vars
    String nom;
    int vida;
    //contructor
    public Enemy(String nom, int vida) {
        this.nom = nom;
        this.vida = vida;
    }
    //methods
    public abstract void attack1(int forca);
    public void attack(int forca){
        attack1(forca);
        printAttackDescription(forca);
    }
    protected void printAttackDescription(int forca){
        String description="";
        if(vida != 0)
            description = String.format("L'enemic %s té %d punts de vida després d'un atac de força %d",nom,vida,forca);
        else description = String.format("L'enemic %s ja està mort",nom);
        System.out.println(description);
    }
    //getters ands setters

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public int getVida() {
        return vida;
    }

    public void setVida(int vida) {
        this.vida = Math.max(vida, 0);
    }
}
