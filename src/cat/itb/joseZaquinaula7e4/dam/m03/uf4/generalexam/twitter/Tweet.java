package cat.itb.joseZaquinaula7e4.dam.m03.uf4.generalexam.twitter;

public class Tweet {
    //vars
    protected String usuari;
    protected String data;
    protected String text;
    //contructor

    public Tweet(String usuari, String data, String text) {
        this.usuari = usuari;
        this.data = data;
        this.text = text;
    }

    //methods
    public void print(){
        System.out.println(toString());
    }

    @Override
    public String toString() {
        return "@" + usuari + " . " + data + "\n" + text + '\n';
    }
}
