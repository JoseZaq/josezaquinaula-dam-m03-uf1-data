package cat.itb.joseZaquinaula7e4.dam.m03.uf4.generalexam.gameenemies;

public class Goblin extends  Enemy{
    //Vars

    //contructor
    public Goblin(String nom, int vida) {
        super(nom, vida);
    }
    //methods
    @Override
    public void attack1(int forca) {
        // 1 punto menos si es inferior a 4 la fuerza, contrario 5 puntos de vida
        if(forca < 4) //si es menor 4 o igual 0 menor a 4?
            setVida(getVida() - 1 );
        else
            setVida(getVida() - 5);
    }
}
