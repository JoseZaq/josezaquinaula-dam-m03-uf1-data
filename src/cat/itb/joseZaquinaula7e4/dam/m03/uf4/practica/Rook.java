package cat.itb.joseZaquinaula7e4.dam.m03.uf4.practica;

public class Rook extends ChessPiece{
    //contructores
    public Rook(boolean white) {
        super(white);
        name = "Rook";
    }
    public Rook(){
        super(true);
    }

    @Override
    public String getPieceString() {
        return "♜";
    }

    @Override
    public boolean isMovePossible(Position from, Position to) {
        return (from.getY() == to.getY() && from.getX() != to.getX()) ||
                (from.getX() == to.getX() && from.getY() != to.getY());
    }
}
