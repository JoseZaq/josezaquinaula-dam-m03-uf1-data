package cat.itb.joseZaquinaula7e4.dam.m03.uf4.practica;

import java.util.Locale;
import java.util.Scanner;

public class ChessBoardGame {
    //vars
    private ChessBoard chessBoard;
    private boolean whitePlayer;
    private final Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
    //contructor

    public ChessBoardGame() {
        chessBoard = new ChessBoard();
        whitePlayer = true;
    }

    //methods
    public void start(){
        //VARs
        String from;
        String to;

        while(true){
            if(isWhitePlayer())
                System.out.println("------------------------\n Jugador Peças Blanc\n------------------------");
            else
                System.out.println("------------------------\n Jugador Peças Negras\n------------------------");
            chessBoard.printBoard();
            System.out.print("Coordenadas: ");
            from = scanner.next();
            if(from.equals("-1")) {
                break;
            }
            to = scanner.next();
            // move
            movePiece(new Position(from),new Position(to));
            chessBoard.turnAround();
            switchPlayer();
        }
    }

    private void movePiece(Position from, Position to) {
        if( chessBoard.getPiece(from) != null) {
            if (whitePlayer) {
                if (!chessBoard.getPiece(from).isWhite()) // la pieza a mover debe ser del jugador del turno actual
                    System.out.println("El jugador sols pot moure les seues peces!");
                else if (chessBoard.getPiece(to) != null && chessBoard.getPiece(to).isWhite()) // la pieza a comer no debe ser de el mismo
                    System.out.println("El jugador sols pot capturar les peces dels contrincant!");
                else
                    chessBoard.move(from, to);
            } else {
                if (chessBoard.getPiece(from).isWhite()) // la pieza a mover debe ser del jugador del turno actual
                    System.out.println("El jugador sols pot moure les seues peces!");
                else if (chessBoard.getPiece(to) != null && !chessBoard.getPiece(to).isWhite()) // la pieza a comer no debe ser de el mismo
                    System.out.println("El jugador sols pot capturar les peces dels contrincant!");
                else
                    chessBoard.move(from, to);
            }
        }
    }

    private void switchPlayer(){
        whitePlayer = !whitePlayer;
    }
    public boolean isWhitePlayer() {
        return whitePlayer;
    }
    public boolean isPlayerPiece(ChessPiece piece){
        if(whitePlayer)
            return piece.white;
        else
            return !piece.white;
    }

}
