package cat.itb.joseZaquinaula7e4.dam.m03.uf4.practica;

public class ChessBoard {
    //vars
    private final  ChessPiece[][] board;
    private boolean whiteView;
    //contructor

    public ChessBoard() {
        board = new ChessPiece[8][8];
        whiteView = true;
        //
        setPiece(0,0, new Rook(false));
        setPiece(7,0,new Rook(false));
        setPiece(1,0,new Knight(false));
        setPiece(6,0,new Knight(false));
        setPiece(2,0,new Bishop(false));
        setPiece(5,0,new Bishop(false));
        setPiece(3,0,new Queen(false));
        setPiece(4,0,new  King(false));

        for (int i = 0; i <board.length; i++) {
            setPiece(i,1, new Pawn(false));
        }
        setPiece(0,7, new Rook(true));
        setPiece(7,7,new Rook(true));
        setPiece(1,7,new Knight(true));
        setPiece(6,7,new Knight(true));
        setPiece(2,7,new Bishop(true));
        setPiece(5,7,new Bishop(true));
        setPiece(3,7,new Queen(true));
        setPiece(4,7,new  King(true));

        for (int i = 0; i <board.length; i++) {
            setPiece(i,6,new Pawn(true));
        }
    }
    // methods
    public void printBoard(){
        if(whiteView) {
            for (int j = 0; j < board.length; j++) {
                System.out.print(8 - j + "|"); //rank
                for (int i = 0; i < board[j].length; i++) {
                    System.out.print((board[i][j] == null ? "\u2001" : board[i][j].toString()) + "|");
                }
                System.out.println();
            }
            //File
            System.out.print(" \u0020");
            for (int i = 0; i < board[0].length; i++) {
                System.out.print((char) ('A' + i) + "\u2001");
            }
        }else{
            for (int j = board.length-1;j >= 0; j--) {
                System.out.print(8 - j + "|"); //rank
                for (int i = board[j].length-1; i >= 0; i--) {
                    System.out.print((board[i][j] == null ? "\u2001" : board[i][j].toString()) + "|");
                }
                System.out.println();
            }
            //File
            System.out.print(" \u0020");
            for (int i = 0; i < board[0].length; i++) {
                System.out.print((char) ('H' - i) + "\u2001");
            }
        }
        System.out.println();
    }
    public void move(Position from,Position to){
        //cojo la pieza a mover
        ChessPiece piece = getPiece(from);
        if(piece == null){
            System.out.printf("Error! No hi ha cap peça a %s!\n",from.getCode());
            return;
        }
        //muevo la pieza: elimino la pieza la posicion original, la dibujo en la nueva posicion
        if(isMovePossible(from,to)) {
            setPiece(from, null);
            setPiece(to, piece);
        } else
            System.out.printf("Error! El moviment de %s %s a %s no està permitit.\n"
                    ,piece.name,from.getCode(),to.getCode());
    }

    private boolean isMovePossible(Position from, Position to) {
        ChessPiece piece = getPiece(from);
        if(piece.name.equals("Pawn")){ // solo si la pieza es pawn
            Pawn pawn;
            pawn = (Pawn) piece;
            return pawn.isMovePossible(this,from,to);
        }
        return piece.isMovePossible(from,to);
    }

    public ChessPiece getPiece(Position pos) {
        return board[pos.getX()][pos.getY()];
    }
    public ChessPiece getPiece(int i,int j){
        return getPiece(new Position(i,j));
    }
    public void turnAround(){
        whiteView= !whiteView;
    }
    //position
    public void setPiece(String code, ChessPiece piece){
        setPiece(new Position(code.charAt(0),code.charAt(1)),piece );
    }
    public void setPiece(int i, int j, ChessPiece piece){
        setPiece(new Position((char)('A' + i),8 - j),piece);
    }
    public void setPiece(Position pos,ChessPiece piece){
        board[pos.getX()][pos.getY()] = piece;
    }
}
