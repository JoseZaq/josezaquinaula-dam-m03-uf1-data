package cat.itb.joseZaquinaula7e4.dam.m03.uf4.practica;

public class Queen extends ChessPiece{
    public Queen( boolean white) {
        super( white);
        name ="Queen";
    }

    @Override
    public String getPieceString() {
        return "♛";
    }

    @Override
    public boolean isMovePossible(Position from, Position to) {
        return (Math.abs(to.getX() - from.getX()) == Math.abs(to.getY() - from.getY()) );
    }
}
