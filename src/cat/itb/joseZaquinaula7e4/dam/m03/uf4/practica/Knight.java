package cat.itb.joseZaquinaula7e4.dam.m03.uf4.practica;

public class Knight extends ChessPiece{
    public Knight( boolean white) {
        super(white);
        name = "Knight";
    }

    @Override
    public String getPieceString() {
        return "♞";
    }

    @Override
    public boolean isMovePossible(Position from, Position to) {
        if(Math.abs(from.getX() - to.getX()) == 2){
            return Math.abs(from.getY() - to.getY()) == 1;
        }else if(Math.abs(from.getY() - to.getY()) == 2){
            return Math.abs(from.getX() - to.getX()) == 1;
        }else
            return false;

    }
}
