package cat.itb.joseZaquinaula7e4.dam.m03.uf4.practica;

import cat.itb.joseZaquinaula7e4.dam.m03.uf4.exercices.figures.ConsoleColors;

public abstract class ChessPiece {
    //vars
    protected String name;
    protected boolean white;
    protected String pieceString;
    //contructor

    public ChessPiece(boolean white) {
        this.white = white;
    }
    // methods

    //getters and setters
    public abstract String getPieceString();
    public boolean isWhite() {
        return white;
    }
    //
    @Override
    public String toString() {
        return  (white ? ConsoleColors.YELLOW : ConsoleColors.BLUE) + getPieceString() + ConsoleColors.RESET;
    }
    public abstract boolean isMovePossible(Position from, Position to);
}
