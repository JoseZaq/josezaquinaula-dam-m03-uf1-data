package cat.itb.joseZaquinaula7e4.dam.m03.uf4.practica;

public class Pawn extends ChessPiece{
    public Pawn(boolean white) {
        super( white);
        name = "Pawn";
    }

    @Override
    public String getPieceString() {
        return "♟";
    }
    public boolean isMovePossible(ChessBoard board,Position from,Position to){
        // si es pieza blanca el rank se resta, sino se suma
        if(white) {
            if ((to.getX() == from.getX() + 1 && to.getY() == from.getY() - 1) ||
                    (to.getX() == from.getX() - 1 && to.getY() == from.getY() - 1)) //si el destino es una diagonal a izquierda o derecha, tiene que haber UNA PIEZA
                return board.getPiece(to) != null;
        }else{
            if ((to.getX() == from.getX() + 1 && to.getY() == from.getY() + 1) ||
                    (to.getX() == from.getX() - 1 && to.getY() == from.getY() + 1))
                return board.getPiece(to) != null;
        }
        return isMovePossible(from, to);
    }
    @Override
    public boolean isMovePossible(Position from, Position to) {
        // si es pieza blanca el rank se resta, sino se suma (en coordenadas de matriz)
        if(white)
            return to.getY() == from.getY()-1;
        return to.getY() == from.getY()+1;


    }
}
