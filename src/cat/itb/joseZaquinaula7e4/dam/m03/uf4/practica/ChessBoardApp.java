package cat.itb.joseZaquinaula7e4.dam.m03.uf4.practica;

import java.util.Locale;
import java.util.Scanner;

public class ChessBoardApp {
    public static void main(String[] args) {
        ChessBoardGame chessBoardGame = new ChessBoardGame();
        chessBoardGame.start();
    }
}
