package cat.itb.joseZaquinaula7e4.dam.m03.uf4.practica;

public class King extends ChessPiece{
    public King( boolean white) {
        super(white);
        name = "King";
    }

    @Override
    public String getPieceString() {
        return "♚";
    }

    @Override
    public boolean isMovePossible(Position from, Position to) {
        return Math.abs(from.getX() - to.getX()) <= 1 && Math.abs(from.getY() - to.getY()) <= 1;
    }
}
