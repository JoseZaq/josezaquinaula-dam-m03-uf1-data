package cat.itb.joseZaquinaula7e4.dam.m03.uf4.practica;

public class Bishop extends ChessPiece{

    public Bishop( boolean white) {
        super(white);
        name = "Bishop";
    }

    @Override
    public String getPieceString() {
        return "♝";
    }

    @Override
    public boolean isMovePossible(Position from, Position to) {
        return (Math.abs(to.getX() - from.getX()) == Math.abs(to.getY() - from.getY()) ) ||
                ((from.getY() == to.getY() && from.getX() != to.getX()) ||
                (from.getX() == to.getX() && from.getY() != to.getY()));

    }
}
