package cat.itb.joseZaquinaula7e4.dam.m03.uf4.practica;

public class Position {
    //vars
    char file;
    int rank;
    //contructor

    public Position(char file, int rank) {
        this.file = file;
        this.rank = rank;
    }
    public Position(int i, int j){
        file = (char)('A' + i);
        rank = 8 - j;
    }
    public Position(String code){
        file = Character.toUpperCase(code.charAt(0));
        rank = Integer.parseInt(String.valueOf(code.charAt(1)));
    }
    //methods
    public String getCode(){
        return (file+String.valueOf(rank));
    }
    public char getFile() {
        return file;
    }
    public int getX() {
        return file - 'A';
    }
    public int getY(){
        return 8 - rank;
    }

    public void setFile(char file) {
        this.file = file;
    }

    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }
}
