package cat.itb.joseZaquinaula7e4.dam.m03.uf4.exercices.quiz;

import java.util.Scanner;

public abstract class Question {
    //vars
    protected String question;
    protected String answer;
    protected boolean correct;
    //contructors
    public Question(String question, String answer) {
        this.question = question;
        this.answer = answer;
        correct = false;
    }
    //functions
    public void ask(Scanner scanner){
        printQuestion();
        String uAns = scanner.nextLine();
        correct = compareAnswer(uAns);
    }

    protected abstract boolean compareAnswer(String uAns);

    public abstract void printQuestion();
    //getters and setters

    public boolean isCorrect() {
        return correct;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }
}
