package cat.itb.joseZaquinaula7e4.dam.m03.uf4.exercices;

public class ScooterModel extends VehicleModel{
    //vars
    private double power;
    //contructor
    public ScooterModel(String name,double power, Brand brand) {
        super(name, brand);
        this.power = power;
    }

    @Override
    public String toString() {
        return "ScooterModel{" +
                "power=" + power +
                ", name='" + name + '\'' +
                ", brand=" + brand +
                '}';
    }
}
