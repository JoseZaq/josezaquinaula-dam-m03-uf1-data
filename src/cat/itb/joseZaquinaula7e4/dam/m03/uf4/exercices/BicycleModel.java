package cat.itb.joseZaquinaula7e4.dam.m03.uf4.exercices;

/**
 * Una empresa de venda de bicicletes vol fer una aplicació per tenir constància del seu estoc.
 * De cada model de bicicleta en vol guardar el nom, marxes i marca. De cada marca en vol guardar el nom i el país (el nom del país).
 *
 * Crea les classes necessaries per guardar aquesta informació.
 *
 * Fes un petit programa que crei 2 bicicletes d'una única marca i les imprimeixi per pantalla (no s'ha de llegir res de l'usuari).
 *
 * out:
 * BicycleModel{name='modelX', gears=5, brand=BycicleBrand{name='lorem', country='Togo'}}
 * BicycleModel{name='model5s628', gears=7, brand=BycicleBrand{name='lorem', country='Togo'}}
 */
public class BicycleModel extends VehicleModel{
    //VARS
    int gears;
    //contructor
    public BicycleModel(String name, int gears, Brand brand) {
        super(name,brand);
        this.gears = gears;
    }

    //metodos
    @Override
    public String toString() {
        return "BicycleModel{" +
                "name='" + name + '\'' +
                ", gears=" + gears +
                ", brand=" + brand.name+brand +
                '}';
    }
}
