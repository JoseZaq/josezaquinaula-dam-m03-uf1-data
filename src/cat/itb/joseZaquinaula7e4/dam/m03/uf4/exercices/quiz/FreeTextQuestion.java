package cat.itb.joseZaquinaula7e4.dam.m03.uf4.exercices.quiz;

public class FreeTextQuestion extends Question{
    //VARS
    //contructors
    public FreeTextQuestion(String question, String answer) {
        super(question, answer);
    }

    @Override
    protected boolean compareAnswer(String uAns) {
        return answer.equals(uAns);
    }

    @Override
    public void printQuestion() {
        System.out.println(question);
    }
}
