package cat.itb.joseZaquinaula7e4.dam.m03.uf4.exercices.figures;

import java.io.PrintStream;

public class RectangleFigure {
    //vars
    String color;
    int width;
    int height;
    //contrucotr

    public RectangleFigure(String color, int width, int height) {
        this.color = color;
        this.width = width;
        this.height = height;
    }
    //funciones
    public void paint(){
        this.paint(System.out);
    }
    public void paint(PrintStream printStream){
        printStream.print(color);
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                printStream.print('X');
            }
            printStream.println();
        }
        printStream.print(ConsoleColors.RESET);
    }
}
