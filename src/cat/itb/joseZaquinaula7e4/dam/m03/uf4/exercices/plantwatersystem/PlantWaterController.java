package cat.itb.joseZaquinaula7e4.dam.m03.uf4.exercices.plantwatersystem;

public class PlantWaterController {
    //vars
    PlantWater plantWater = new PlantWater();
    //methods
    public void waterIfNeeded(){
        if(plantWater.avgHumidityRecord() < 2)
            plantWater.startWaterSystem();
    }
}
