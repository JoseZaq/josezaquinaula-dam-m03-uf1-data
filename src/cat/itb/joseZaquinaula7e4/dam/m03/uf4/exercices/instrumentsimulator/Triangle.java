package cat.itb.joseZaquinaula7e4.dam.m03.uf4.exercices.instrumentsimulator;

public class Triangle extends Instrument{
    public Triangle(int tone) {
        super(String.valueOf(tone));
    }

    @Override
    public void makeSound() {
        System.out.print("T");
        for (int i = 0; i < Integer.parseInt(tone); i++) {
            System.out.print("I");
        }
        System.out.println("NC");
    }
}
