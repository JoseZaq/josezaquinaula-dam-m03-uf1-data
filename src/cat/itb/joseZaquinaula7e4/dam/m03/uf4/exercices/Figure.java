package cat.itb.joseZaquinaula7e4.dam.m03.uf4.exercices;

import cat.itb.joseZaquinaula7e4.dam.m03.uf4.exercices.figures.ConsoleColors;

public abstract class Figure {
    //vars
    private String color;
    //contructors

    public Figure(String color) {
        this.color = color;
    }
    //functions
    protected void prepareColor(){
        System.out.print(color);
    }
    protected void clearColor(){
        System.out.print(ConsoleColors.RESET);
    }
    public abstract void paint();
    //getters and setters
    public String getColor() {
        return color;
    }

}
