package cat.itb.joseZaquinaula7e4.dam.m03.uf4.exercices;

public class MechanicalArm {
    protected boolean turnOn;
    protected int angle;
    protected int altitude;
    //contructor


    public MechanicalArm() {
    }

    //functions
    public void turnOn(){
        turnOn = true;
    }

    public void turnOff(){
        turnOn = false;
    }

    public void updateAltitude(int altitude){
        setAltitude(this.altitude + altitude );
        if(turnOn){
            this.altitude = Math.max(this.altitude,0);
        }
    }

    public void updateAngle( int angle){
        if(turnOn){
        this.angle += angle;
        this.angle = Math.max(this.angle,0);
        this.angle = Math.min(this.angle,360);}
    }

    //getters and setters
    public void setAngle(int angle) {
        if(turnOn)
            this.angle = angle;
    }

    public void setAltitude(int altitude) {
        if(turnOn)
            this.altitude = altitude;
    }

    @Override
    public String toString() {
        return "MechanicalArm{" +
                "turnOn=" + turnOn +
                ", angle=" + angle +
                ", altitude=" + altitude +
                '}';
    }
}
