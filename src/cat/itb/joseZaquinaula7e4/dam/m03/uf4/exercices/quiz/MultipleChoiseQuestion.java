package cat.itb.joseZaquinaula7e4.dam.m03.uf4.exercices.quiz;

public class MultipleChoiseQuestion extends Question{
    //vars
    String choise1,choise2,choise3,choise4;
    //constructors
    public MultipleChoiseQuestion(String question,String choise1,String choise2,
                                  String choise3,String choise4, String answer) {
        super(question, answer);
        this.choise1 = choise1;
        this.choise2  = choise2;
        this.choise3 = choise3;
        this.choise4 = choise4;
    }

    @Override
    protected boolean compareAnswer(String uAns) {
        return answer.equals(uAns);
    }

    @Override
    public void printQuestion() {
        System.out.println(question + "\n" +
                "a. "+choise1 + "\n" +
                "b. "+choise2 + "\n" +
                "c. "+choise3 + "\n" +
                "d. "+choise4);
    }
}
