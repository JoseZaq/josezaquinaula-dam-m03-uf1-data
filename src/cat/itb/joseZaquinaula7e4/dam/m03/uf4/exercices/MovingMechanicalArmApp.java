package cat.itb.joseZaquinaula7e4.dam.m03.uf4.exercices;

public class MovingMechanicalArmApp {
    public static void main(String[] args) {
        /*
        * setTurnedOn(true)
        updateAltitude(3)
        move(4.5)
        updateAngle(180)
        updateAltitude(-3)
        updateAngle(-180)
        updateAltitude(3)
        move(-4.5)
        setTurnedOn(false)
         */
        MovingMechanicalArm arm = new MovingMechanicalArm();
        arm.turnOn();
        arm.updateAltitude(3);
        arm.move(4.5);
        arm.updateAngle(180);
        arm.updateAltitude(-3);
        arm.updateAngle(-180);
        arm.move(-4.5);
        arm.turnOff();
        System.out.println(arm);
    }
}
