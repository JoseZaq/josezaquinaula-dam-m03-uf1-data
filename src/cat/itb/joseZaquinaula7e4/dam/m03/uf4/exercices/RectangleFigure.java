package cat.itb.joseZaquinaula7e4.dam.m03.uf4.exercices;

public class RectangleFigure extends Figure{
    //vars
    private int width;
    private int height;
    //contructor
    public RectangleFigure(int width,int heigth,String color) {
        super(color);
        this.height = heigth;
        this.width = width;
    }
    public RectangleFigure(String color,int width,int heigth) {
        super(color);
        this.height = heigth;
        this.width = width;
    }

    @Override
    public void paint() {
        prepareColor();
        for (int i = 0; i < height ; i++) {
            for (int j = 0; j < width; j++) {
                System.out.print('X');
            }
            System.out.println();
        }
        clearColor();
    }
}
