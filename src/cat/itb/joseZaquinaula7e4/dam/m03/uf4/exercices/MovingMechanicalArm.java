package cat.itb.joseZaquinaula7e4.dam.m03.uf4.exercices;

public class MovingMechanicalArm extends MechanicalArm{
    //vars
    private double position;
    //contructors
    public void move(double pos){
        setPosition(getPosition() + pos);
    }
    //getters and setters

    public double getPosition() {
        return position;
    }

    public void setPosition(double distance) {
        if(turnOn)
            this.position = Math.max(distance,0);
    }

    @Override
    public String toString() {
        return "MovingMechanicalArm{" +
                "turnOn=" + turnOn +
                ", angle=" + angle +
                ", altitude=" + altitude +
                ", position=" + position +
                '}';
    }
}
