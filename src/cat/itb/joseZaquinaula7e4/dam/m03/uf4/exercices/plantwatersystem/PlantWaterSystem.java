package cat.itb.joseZaquinaula7e4.dam.m03.uf4.exercices.plantwatersystem;

public interface PlantWaterSystem {
    //metodos
    void addHumidityData(double sensorData); //añade un registro de humedad y borra el dato mas antiguo de la lista.
    void startWaterSystem(); //acciona el riego
}
