package cat.itb.joseZaquinaula7e4.dam.m03.uf4.exercices;

public class LeftPiramidFigure extends Figure{
    //vars
    int base;
    //contructor
    public LeftPiramidFigure(int base,String color) {
        super(color);
        this.base = base;
    }
    public LeftPiramidFigure(String color,int base) {
        super(color);
        this.base = base;
    }
    //functions
    @Override
    public void paint() {
        prepareColor();
        for (int i = 1; i <= base; i++) {
            for (int j = 0; j < i; j++) {
                System.out.print("X");
            }
            System.out.println();
        }
        clearColor();
    }
    // getters and setters
}
