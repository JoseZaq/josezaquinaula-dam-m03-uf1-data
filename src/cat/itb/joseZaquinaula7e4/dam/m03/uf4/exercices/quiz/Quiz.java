package cat.itb.joseZaquinaula7e4.dam.m03.uf4.exercices.quiz;

import java.util.*;

/**
 * Volem fer un petit programa de preguntes.
 * Tenim dos tipus de preguntes:
 *
 * opció múltiple
 * text lliure
 *
 * Necessitaràs les següents classes:
 * Question
 * FreeTextQuestion
 * MultipleChoiseQuestion
 * Quiz (conté una llista de preguntes)
 *
 * Pas 1: Fes l'estructura de dades que et permeti guardar aquestes preguntes.
 * Pas 2: Fes un programa que, donat un quiz creat per codi, imprimeixi les preguntes.
 * Pas 3: Permet que l'usuari pugui respondre les preguntes.
 * Pas 4: Un cop acabat, imprimeix quantes respostes són correctes.
 */
public class Quiz {
        /* List preguntes = new arrat list preguntes
        * añadiendo preguntes
        * startQuiz()
        * printResults() */
        //vars
        Scanner scanner;
        //contructor

    public Quiz(Scanner scanner) {
        this.scanner = scanner;
    }

    List<Question> questions = new ArrayList<>(Arrays.asList(
            new FreeTextQuestion("¿Como es el nombre completo de Dam?", "desarrollo de aplicaciones multiplataforma"),
            new MultipleChoiseQuestion("¿Como se puede tener buenas notas en Dam?","durmiendo",
                    "jugando","solo estudiando","estudiando y practicando","d")
    ));
    //functions
    public void startQuiz(){
        System.out.println(             "QUIZ DEL ITB");
        for (int i = 1; i <= questions.size(); i++) {
            System.out.print(i+". ");
            questions.get(i-1).ask(scanner);
        }
    }
    public void results(){
        System.out.println("            Respuestas Correctas");
        for (int i = 0; i < questions.size(); i++) {
            if(questions.get(i).isCorrect())
                System.out.printf("---pregunta No.%d correcta :D\n",i+1);
            else
                System.out.printf("---pregunta No.%d incorrecta D':\n",i+1);
        }
    }
}
