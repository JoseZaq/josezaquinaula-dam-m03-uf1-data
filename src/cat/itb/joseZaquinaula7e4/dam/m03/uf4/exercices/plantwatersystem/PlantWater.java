package cat.itb.joseZaquinaula7e4.dam.m03.uf4.exercices.plantwatersystem;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PlantWater implements PlantWaterSystem{
    //vars
    List<Double> humidityRecord;
    //contructors
    public PlantWater(){
        humidityRecord= new ArrayList<Double>(Arrays.asList(0.0,0.0,0.0,0.0,0.0,0.0
                ,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0));
    }
    @Override
    public void addHumidityData(double sensorData) {
        humidityRecord.add(0,sensorData);
        humidityRecord.remove(humidityRecord.size()-1);
    }

    public double avgHumidityRecord(){
        double avgRec = 0;
        for (Double aDouble : humidityRecord) {
            avgRec += aDouble;
        }
        avgRec /= humidityRecord.size();
        return  avgRec;
    }
    @Override
    public void startWaterSystem() {
        System.out.println("Sistema de riego activado");
    }
    //getters and setters

    public List<Double> getHumidityRecord() {
        return humidityRecord;
    }
}
