package cat.itb.joseZaquinaula7e4.dam.m03.uf4.exercices;

import cat.itb.joseZaquinaula7e4.dam.m03.uf4.exercices.figures.ConsoleColors;
import cat.itb.joseZaquinaula7e4.dam.m03.uf4.exercices.figures.RectangleFigure;

public class ThreeRectangles {
    public static void main(String[] args) {
        //vars
        RectangleFigure rec1 = new RectangleFigure(ConsoleColors.RED, 4,5);
        RectangleFigure rec2 = new RectangleFigure(ConsoleColors.YELLOW, 2,2);
        RectangleFigure rec3 = new RectangleFigure(ConsoleColors.GREEN, 3,5);
        //funciones
        rec1.paint();
        rec2.paint();
        rec3.paint();
    }
}
