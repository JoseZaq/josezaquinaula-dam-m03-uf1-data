package cat.itb.joseZaquinaula7e4.dam.m03.uf4.exercices;

import cat.itb.joseZaquinaula7e4.dam.m03.uf4.exercices.figures.ConsoleColors;

import java.util.ArrayList;
import java.util.List;

public class ListOfFigurePainter {
    public static void main(String[] args) {
        List<Figure> figureList = new ArrayList<>();
        figureList.add(new LeftPiramidFigure(3, ConsoleColors.GREEN));
        figureList.add(new RectangleFigure(4,5,ConsoleColors.RED));
        for(Figure figure : figureList){
            figure.paint();
        }
    }

}
