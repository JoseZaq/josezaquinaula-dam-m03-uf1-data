package cat.itb.joseZaquinaula7e4.dam.m03.uf4.exercices.instrumentsimulator;

public abstract  class Instrument {
    //vars
    String tone;
    //contructors

    public Instrument(String tone) {
        this.tone = tone;
    }
    //functions
    public abstract void makeSound();
}
