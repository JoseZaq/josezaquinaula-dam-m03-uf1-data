package cat.itb.joseZaquinaula7e4.dam.m03.uf4.exercices;

/**
 * L'empresa que feia bicicletes ara també ven patinets.
 * Tingues en compte que els patinets no tenen marxes. Per altre banda si que t'has de guardar la potencia que tenen.
 * Crea i modifica les classes que necessitis.
 *
 * Fes un petit programa que crei una bicicleta i un patinet d'una única marca i les imprimeixi per pantalla (no s'ha de llegir res de l'usuari).
 *
 * Output
 * BicycleModel{name='modelX', gears=5, brand=VehicleBrand{name='lorem', country='Togo'}}
 * ScooterModel{name='s562', power=45.3, brand=VehicleBrand{name='lorem', country='Togo'}}
 */
public class VehicleModel {
    //vars
    String name;
    Brand brand;
    //contructors
    public VehicleModel(String name, Brand brand) {
        this.name = name;
        this.brand = brand;
    }

}
