package cat.itb.joseZaquinaula7e4.dam.m03.uf4.exercices;

import cat.itb.joseZaquinaula7e4.dam.m03.uf4.exercices.figures.ConsoleColors;

public class ThreeRectanglesOneLeftPiramid {
    public static void main(String[] args) {
        /*rectangle: color: RED, llargada: 4, amplada: 5
        triangle: color: YELLOW, altura: 3
        rectangle color: GREEN, llargada: 3, amplada: 5*/
        Figure rec1 = new RectangleFigure(4,5, ConsoleColors.RED);
        Figure tr1 = new LeftPiramidFigure(3,ConsoleColors.YELLOW);
        Figure rec2 = new RectangleFigure(3,5, ConsoleColors.GREEN);
        //actions
        rec1.paint();
        tr1.paint();
        rec2.paint();
    }
}
