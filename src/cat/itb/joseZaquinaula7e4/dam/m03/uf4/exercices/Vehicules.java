package cat.itb.joseZaquinaula7e4.dam.m03.uf4.exercices;

public class Vehicules {
    public static void main(String[] args) {
        Brand brand = new Brand("lorem","Togo");
         BicycleModel bicycle1 = new BicycleModel("modelX",5,brand);
         BicycleModel bicycle2 = new BicycleModel("model15s628",7,brand);
        ScooterModel scooter1 = new ScooterModel("s562",45.3,brand);
        System.out.println(bicycle1.toString());
        System.out.println(bicycle2.toString());

    }
}
