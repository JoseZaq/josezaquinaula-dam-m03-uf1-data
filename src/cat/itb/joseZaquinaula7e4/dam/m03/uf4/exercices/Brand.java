package cat.itb.joseZaquinaula7e4.dam.m03.uf4.exercices;

public class Brand {
    //VARS
    String name;
    String country;
    //contrucotr

    public Brand(String name, String country) {
        this.name = name;
        this.country = country;
    }

    @Override
    public String toString() {
        return "Brand{" +
                "name='" + name + '\'' +
                ", country='" + country + '\'' +
                '}';
    }
}
