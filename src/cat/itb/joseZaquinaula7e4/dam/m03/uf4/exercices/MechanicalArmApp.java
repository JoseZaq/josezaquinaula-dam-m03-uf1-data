package cat.itb.joseZaquinaula7e4.dam.m03.uf4.exercices;

public class MechanicalArmApp {
    public static void main(String[] args) {
        MechanicalArm arm = new MechanicalArm();
        arm.turnOn();
        arm.updateAltitude(-10);
        arm.updateAngle(361);

        System.out.println(arm);
    }
}
