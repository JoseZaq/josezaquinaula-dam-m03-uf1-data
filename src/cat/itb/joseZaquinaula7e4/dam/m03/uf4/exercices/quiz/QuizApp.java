package cat.itb.joseZaquinaula7e4.dam.m03.uf4.exercices.quiz;

import java.util.Locale;
import java.util.Scanner;

public class QuizApp {
    public static void main(String[] args) {
        Scanner scanner  = new Scanner(System.in).useLocale(Locale.US);
        Quiz quiz = new Quiz(scanner);
        quiz.startQuiz();
        quiz.results();
    }
}
