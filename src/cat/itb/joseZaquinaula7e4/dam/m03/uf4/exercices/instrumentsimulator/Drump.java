package cat.itb.joseZaquinaula7e4.dam.m03.uf4.exercices.instrumentsimulator;

public class Drump extends Instrument{

    public Drump(String tone) {
        super(tone);
    }

    @Override
    public void makeSound() {
        System.out.print("T");
        for (int i = 0; i < 3; i++) {
            System.out.print(tone);
        }
        System.out.println("M");

    }
}
