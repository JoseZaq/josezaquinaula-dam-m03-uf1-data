package cat.itb.joseZaquinaula7e4.dam.m03.uf4.exercices.instrumentsimulator;

import java.util.ArrayList;
import java.util.List;

public class InstrumentSimulator {
    public static void main(String[] args) {
        List<Instrument> instruments = new ArrayList<>();
        instruments.add(new Triangle(5));
        instruments.add(new Drump("A"));
        instruments.add(new Drump("O"));
        instruments.add(new Triangle(1));
        instruments.add(new Triangle(5));

        play(instruments);
    }

    private static void play(List<Instrument> instruments) {
        for(Instrument instrument: instruments){
            instrument.makeSound();
        }
    }
}
