package cat.itb.joseZaquinaula7e4.dam.m03.uf5.generalexam;

import java.util.*;

/**
 * La mateixa editorial ens demana que necessita tenir la següent informació (cada apartat conta un punt):
 *
 * El llibre amb més pàgines (si n'hi ha més d'un, un qualsevol)
 * El llistat dels diferents llibres ordenats per any de publicació (any més gran primer). Els llibres del mateix any, ordenar-los per el títol.
 * Nombre mitjana de pàgines dels llibres publicats
 * La informació dels llibres publicats després del 2018
 * La suma del resultat de múltiplicar el nombre de pàgines per l'any de publicació de cada llibre
 * Dels llibres de més de 100 pàgines, ordenats per ordre alfabètic del títol, imprimir-ne el títol.
 */
public class BookRanking {
    public static void main(String[] args) {
        //vars
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        List<Book> books = new ArrayList<>();
        // actions
        //read books
        int nBooks = scanner.nextInt();
        for (int i = 0; i < nBooks; i++) {
            scanner.nextLine();
            Book book = Book.readBook(scanner);
            books.add(book);
        }
        // rankings
        System.out.println("### Llibre amb més pàgines");
        Book topPagesBook = Collections.max(books, Comparator.comparingInt(Book::getPages));
        System.out.println(topPagesBook);
        System.out.println();
        //
        System.out.println("### Llibres per any");
        books.sort(Comparator.comparingInt(Book::getDate).thenComparing(Book::getTitle).reversed());
        books.forEach(System.out::println);
        System.out.println();
        //
        System.out.println("### Mitjana de pàgines");
        double avgPages = books.stream().mapToDouble(Book::getPages).average().getAsDouble();
        System.out.printf("%.1f\n",avgPages);
        System.out.println();
        //
        System.out.println("### Llibres publicats després del 2018");
        books.stream().filter(b -> b.getDate() > 2018).forEach(System.out::println);
        System.out.println();
        //
        System.out.println("### Suma de anys per pàgines");
        int sum = books.stream().mapToInt(b -> b.getPages() * b.getDate()).sum();
        System.out.println(sum);
        System.out.println();
        //
        System.out.println("### Més de 100 pàgines");
        books.stream().filter(b -> b.getPages() > 100).
                sorted(Comparator.comparing(Book::getTitle)).forEach(b -> System.out.println(b.getTitle()));
    }
}
