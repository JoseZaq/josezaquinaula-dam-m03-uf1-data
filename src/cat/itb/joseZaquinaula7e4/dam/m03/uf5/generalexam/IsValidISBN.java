package cat.itb.joseZaquinaula7e4.dam.m03.uf5.generalexam;

import java.util.Locale;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * L'editorial ens ha demanat fer un programa que valide si un codi ISB és valid o no.
 *
 * El número d'ISBN està format per tretze dígits (ISBN-13), però fins a finals del 2006 en constava de deu (ISBN-10) i es va canviar per raons de compatibilitat amb el sistema de codi de barres EAN-13.
 *
 * La primera part és un prefix -978- que identifica el producte llibre. Aquesta part no apareix en el format ISBN-10
 * La segona part, formada per 9 núeros es correspon a l'identificació del llibre i edició concreta d'aquest.
 * L'últim caràcter es correspon a un dígit de control. Aqust dígit pot ser un número de 0 al 9 o la lletra X.
 * Per exemple, el llíbre Effective Java de Joshua Bloch té els seguents codis ISBN:
 *
 * ISBN-10: 013468599-7
 * ISBN-13: 978013468599-1
 */
public class IsValidISBN {
    public static void main(String[] args) {
        //vars
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        Pattern pattern1 = Pattern.compile("$978");
        Pattern pattern2 = Pattern.compile("\\d+-(\\d||X)");
        Matcher matcher;
        //actions
        int nISBNs = scanner.nextInt();
        for (int i = 0; i < nISBNs; i++) {
            //
            String ISBN = scanner.next();
            //
            matcher = pattern1.matcher(ISBN);
            if(ISBN.length() == 14) {
                if (!matcher.matches()) {
                    System.out.println(false);
                    break;
                }
            }
            //
            matcher = pattern2.matcher(ISBN);
            if(!matcher.matches()){
                System.out.println(false);
                break;
            }
            System.out.println(true);
        }
    }
}
