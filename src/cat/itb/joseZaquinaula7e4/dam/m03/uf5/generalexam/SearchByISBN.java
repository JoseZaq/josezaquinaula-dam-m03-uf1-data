package cat.itb.joseZaquinaula7e4.dam.m03.uf5.generalexam;

import java.util.*;

/**
 * Una editorial vol tenir referència dels diferents llibres que va publicant. De cada llibre en guardar el títol, l'autor (el pots guardar amb un string), ISBN número de pàgines i any de publicació. El ISBN és un codi únic que identifica cada un dels llibres.
 * Fes un programa que llegeixi primer un llistat de llibre i després donat un ISBN imprimeixi per pantalla el títol i autor del llibre. Usa una estructura adequada per a que sigui fàcil i ràpid cercar un llibre per ISBN.
 */
public class SearchByISBN {
    public static void main(String[] args) {
        //vars
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        Map<String,Book> bookMap = new HashMap<>();
        // actions
        //read books
        int nBooks = scanner.nextInt();
        for (int i = 0; i < nBooks; i++) {
            scanner.nextLine();
            Book book = Book.readBook(scanner);
            bookMap.put(book.getISBN(),book);
        }
        // ask books
        nBooks = scanner.nextInt();
        for (int i = 0; i < nBooks; i++) {
            String ISBN = scanner.next();
            if(bookMap.containsKey(ISBN))
                System.out.println(bookMap.get(ISBN).toString());
            else
                System.out.println("Not Found");
        }
    }
}
