package cat.itb.joseZaquinaula7e4.dam.m03.uf5.generalexam;

import java.util.Scanner;

public class Book {
    //vars
    private String title;
    private String author;
    private String ISBN;
    private int pages;
    private int date;
    //contructor

    public Book(String title, String author, String ISBN, int pages, int date) {
        this.title = title;
        this.author = author;
        this.ISBN = ISBN;
        this.pages = pages;
        this.date = date;
    }

    //methods
    public static Book readBook(Scanner scanner){
        String t = scanner.nextLine();
        String a = scanner.nextLine();
        String I = scanner.next();
        int p = scanner.nextInt();
        int d = scanner.nextInt();
        return new Book(t,a,I,p,d);
    }
    //getters and setters

    public String getISBN() {
        return ISBN;
    }

    @Override
    public String toString() {
        return title + " - " + author + " - " + date;
    }

    public String getTitle() {
        return title;
    }

    public String getAuthor() {
        return author;
    }

    public int getPages() {
        return pages;
    }

    public int getDate() {
        return date;
    }
}
