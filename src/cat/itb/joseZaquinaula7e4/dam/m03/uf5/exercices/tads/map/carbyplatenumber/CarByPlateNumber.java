package cat.itb.joseZaquinaula7e4.dam.m03.uf5.exercices.tads.map.carbyplatenumber;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Scanner;

/**
 * Un concesionari de contxes de segona ma vol tenir la informació dels cotxes que te en estoc.
 * De cada cotxe s'en guarda la matricula, el model i el color.
 * Vol poder accedir rapidament a la informació del cotxe usant la matricula.
 */
public class CarByPlateNumber {
    public static void main(String[] args) {
        //vars
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        Map<String,Car> carMap = new HashMap<>();
        //actions
        int nCars = scanner.nextInt();
        for (int i = 0; i < nCars; i++) {
            Car car = Car.readCar(scanner);
            carMap.put(car.getMatricula(),car);
        }
        while(true){
            String matr = scanner.next();
            if(matr.equals("END"))
                break;
            if(carMap.containsKey(matr))
                System.out.println(carMap.get(matr));
        }
    }
}
