package cat.itb.joseZaquinaula7e4.dam.m03.uf5.exercices.tads.map;

import java.util.Scanner;

public class Employee {
    private String id;
    private String name;
    private String lastname;
    private String address;
    //Contrucotr

    public Employee(String id, String name, String lastname, String address) {
        this.id = id;
        this.name = name;
        this.lastname = lastname;
        this.address = address;
    }
    //methods
    public static Employee readEmployee(Scanner scanner){
        String id = scanner.next();
        String name = scanner.next();
        String lastname = scanner.next();
        scanner.nextLine();
        String adress = scanner.nextLine();
        return new Employee(id,name,lastname,adress);
    }
    //getters and setters

    @Override
    public String toString() {
        return String.format("%s %s - %s, %s",name,lastname,id,address);
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getLastname() {
        return lastname;
    }

    public String getAddress() {
        return address;
    }
}
