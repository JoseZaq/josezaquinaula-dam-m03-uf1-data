package cat.itb.joseZaquinaula7e4.dam.m03.uf5.exercices.tads.set;

import java.util.HashSet;
import java.util.Locale;
import java.util.Scanner;
import java.util.Set;

/**
 *Després de molts diumenges acompanyant a la teva avia
 * al bingo de la residència decideixes fer un petit programa que t'ajudi.
 *  El Bingo de la residència no es canta linia, només Bingo.
 * L'usuari primer itrodueix 10 números, que són els de la targeta.
 * Després introdueix els números que es van cantant.
 * Després que cada número cantat indica quants t'en queden.
 *  Quan s'hagin cantat tots els números imprimeix BINGO i finialitza el programa
 */
public class Bingo {
    public static void main(String[] args) {
        //vars
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        Set<Integer> bingoCard = new HashSet<>();
        //actions
        for (int i = 0; i < 10; i++) {
            int nums = scanner.nextInt();
            bingoCard.add(nums);
        }
        Set<Integer> bingoGame = new HashSet<>();
        while(true){
            int input = scanner.nextInt();
            bingoGame.add(input);
            bingoCard.removeAll(bingoGame);
            if(bingoCard.isEmpty()) {
                System.out.println("BINGO");
                break;
            }
            System.out.println(bingoGame+ "\n" + bingoCard);
        }
    }

}
