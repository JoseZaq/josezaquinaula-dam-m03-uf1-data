package cat.itb.joseZaquinaula7e4.dam.m03.uf5.exercices.func;

import cat.itb.joseZaquinaula7e4.dam.m03.uf2.dataclasses.Product;

import java.util.*;

/**
 * Usa les classes i funcions de l'exercici UF2.MultiProductNiceInfoPrinter, llegeix un conjunt de productes, i imprimeix per pantalla el més car
 */
public class MostExpensiveProduct {
    public static void main(String[] args) {
        //vars
        Scanner scanner=new Scanner(System.in).useLocale(Locale.UK);
        int numProducts= scanner.nextInt();
        List<Product> products = new ArrayList<>();
        //actions
        for (int i = 0; i < numProducts; i++) {
            String nomP=scanner.next();
            double priceP=scanner.nextDouble();
            products.add(new Product(nomP,priceP));
        }
        for (Product product : products) {
            System.out.printf("El producte %s val %.2f€\n", product.getNom(), product.getPrice());
        }
        //LAMBDAS
        Product max = Collections.max(products, (p1,p2) -> (int) (p1.getPrice() - p2.getPrice()));
        System.out.println("El producto con mas precio es: "+" " + max.getNom()+" " + max.getPrice());
        // con lenguaje mas natural
        Product max1 = Collections.max(products, (p1,p2) -> Double.compare(p1.getPrice(), p2.getPrice()));
        Product max2 = Collections.max(products, Comparator.comparingDouble(p -> p.getPrice()));
        Product max3 = Collections.max(products, Comparator.comparingDouble(Product::getPrice));
        System.out.println("El producto con mas precio es: "+" " + max.getNom()+" " + max.getPrice());
    }
}
