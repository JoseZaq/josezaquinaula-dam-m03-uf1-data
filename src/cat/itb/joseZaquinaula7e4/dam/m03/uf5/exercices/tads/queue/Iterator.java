package cat.itb.joseZaquinaula7e4.dam.m03.uf5.exercices.tads.queue;

import cat.itb.joseZaquinaula7e4.dam.m03.uf2.staticfunctions.IntegerLists;

import java.util.*;

/**
 * Llegeix una llista d'enters per consola (usa IntegerLists.readIntegerList).
 * Elimina els enters que són parells
 * Imprimeix la llista per pantalla
 */
public class Iterator {
    public static void main(String[] args) {
        //vars
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        List<Integer> list = new ArrayList<>();
        // actions
        int input = scanner.nextInt();
        list = IntegerLists.readIntegerList(scanner);

        for(int i=0;i< list.size();i++){
            if(list.get(i) % 2 == 0){
                System.out.println(list.get(i)+ " " + list.get(i) % 2);
                list.remove(i);
            }
        }
        for (int intList : list){
            System.out.print(intList + " ");
        }
    }
}
