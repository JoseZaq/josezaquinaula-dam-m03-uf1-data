package cat.itb.joseZaquinaula7e4.dam.m03.uf5.exercices.tads.map;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Scanner;

/**
 * Una empresa vol tenir enregistrada la informació els seus empleats.
 * De cada empleat en vol tenir el seu dni, nom, cognoms i adreça.
 * Vol poder accedir ràpidament a les dades dels empleats segons el seu DNI.
 *
 * Fes un programa que llegeixi un conjunt d'empleats per pantalla (primer introduirà la quantitat d'empleats).
 * Després imprimeix les dades del empleats pel DNI entrat. Acaba el programa quan introdueixi END.
 */
public class EmpoyeeById {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        int maxNewEmployees = scanner.nextInt();
        Map<String,Employee> employeeMap  = new HashMap<String,Employee>();
        //
        for (int i = 0; i < maxNewEmployees; i++) {
            Employee employee = Employee.readEmployee(scanner);
            employeeMap.put(employee.getId(),employee);

        }
        //
        while(true){
            String id = scanner.next();
            if(id.equals("-1"))
                return;
            if(employeeMap.containsKey(id))
                System.out.println(employeeMap.get(id));
            else
                System.out.println("No hi ha cap DNI");
        }

    }
}
