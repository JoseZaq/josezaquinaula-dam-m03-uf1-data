package cat.itb.joseZaquinaula7e4.dam.m03.uf5.exercices.func.streams;

import cat.itb.joseZaquinaula7e4.dam.m03.uf2.dataclasses.Trabajador;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;

/**
 * Fes l'exercici UF2.FromAvgSallaryInfo (pots usar les classes ja fetes),
 * però fes els càlculs (mitjana i filtre) usant streams
 */
public class FromAvgSallaryInfoStream {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in).useLocale(Locale.UK);
        List<Trabajador> trabajadorList = new ArrayList<Trabajador>();
        double sueldo=scanner.nextDouble();
        //while de obtencion: ingresar variables hasta ingresar -1
        while (sueldo != -1){
            String nom=scanner.next();
            trabajadorList.add(new Trabajador(nom,sueldo));
            sueldo=scanner.nextDouble();
        }
        //sumar todos los salarios
        Double sueldoAvg = trabajadorList.stream().mapToDouble(Trabajador::getSueldo).average().getAsDouble();
        System.out.println(sueldoAvg);
    }
}
