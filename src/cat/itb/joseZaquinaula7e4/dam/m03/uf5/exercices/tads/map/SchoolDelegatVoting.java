package cat.itb.joseZaquinaula7e4.dam.m03.uf5.exercices.tads.map;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Scanner;

/**
 * S'ha de votar el delegat de la classe,
 * i per fer-ho has decidit fer un petit programa que conti els vots i imprimeixi el resultat.
 *
 * Cada usuari introduirà el nom de l'estudiant al que vota.
 * Quan ja no hi hagi més vots escriurà END. Imprimeix els total de vots.
 */
public class SchoolDelegatVoting {
    public static void main(String[] args) {
        Scanner scanner = new Scanner( System.in).useLocale(Locale.US);
        Map<String,Integer> votes = new HashMap<>();
        while(true){
            String nameVoted = scanner.next();
            if(nameVoted.equals("END"))
                break;
            if(votes.containsKey(nameVoted)) {
                int current = votes.get(nameVoted);
                votes.put(nameVoted, current + 1);
            }else
                votes.put(nameVoted,1);
            for( String key: votes.keySet()){
                System.out.printf("%s = %d\n",key,votes.get(key));
            }

        }
    }
}
