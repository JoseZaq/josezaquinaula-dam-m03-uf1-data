package cat.itb.joseZaquinaula7e4.dam.m03.uf5.exercices.tads.map.carbyplatenumber;

import java.util.Scanner;

public class Car {
    //vars
    private String matricula;
    private String modelo;
    private String color;
    //Contructor

    public Car(String matricula, String modelo, String color) {
        this.matricula = matricula;
        this.modelo = modelo;
        this.color = color;
    }
    //methods
    public static Car readCar(Scanner scanner){
        String m = scanner.next();
        String mod = scanner.next();
        String c = scanner.next();
        return new Car(m,mod,c);
    }
    //getters


    @Override
    public String toString() {
        return String.format("%s %s %s",matricula,modelo,color);
    }

    public String getMatricula() {
        return matricula;
    }

    public String getModelo() {
        return modelo;
    }

    public String getColor() {
        return color;
    }
}
