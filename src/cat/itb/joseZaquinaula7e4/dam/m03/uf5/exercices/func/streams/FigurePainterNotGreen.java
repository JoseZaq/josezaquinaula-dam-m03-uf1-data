package cat.itb.joseZaquinaula7e4.dam.m03.uf5.exercices.func.streams;

import cat.itb.joseZaquinaula7e4.dam.m03.uf4.exercices.Figure;
import cat.itb.joseZaquinaula7e4.dam.m03.uf4.exercices.LeftPiramidFigure;
import cat.itb.joseZaquinaula7e4.dam.m03.uf4.exercices.RectangleFigure;
import cat.itb.joseZaquinaula7e4.dam.m03.uf4.exercices.figures.ConsoleColors;

import java.util.ArrayList;
import java.util.List;

/**
 * Imprimeix per pantalla només les que no siguin verdes.
 */
public class FigurePainterNotGreen {
    public static void main(String[] args) {
        List<Figure> figures = new ArrayList<>();
        figures.add(new RectangleFigure(ConsoleColors.RED, 4, 5));
        figures.add(new LeftPiramidFigure(ConsoleColors.YELLOW, 3));
        figures.add(new RectangleFigure(ConsoleColors.GREEN, 3, 5));
        figures.add(new RectangleFigure(ConsoleColors.BLUE, 1, 1));
        figures.add(new LeftPiramidFigure(ConsoleColors.YELLOW, 4));
        figures.add(new RectangleFigure(ConsoleColors.GREEN, 10, 5));
        figures.add(new LeftPiramidFigure(ConsoleColors.YELLOW, 8));
        // sort
        figures.stream().filter(f -> !f.getColor().equals(ConsoleColors.GREEN)).forEach(Figure::paint);
    }
}
