package cat.itb.joseZaquinaula7e4.dam.m03.uf5.exercices.func.streams;

import java.util.Scanner;

public class Country {
    //vars
    private String name;
    private String capital;
    private int area;
    private int density;
    //contructors

    public Country(String name, String capital, int area, int density) {
        this.name = name;
        this.capital = capital;
        this.area = area;
        this.density = density;
    }
    // methods
    public static Country readCountries(Scanner scanner){
            String name = scanner.next();
            String capital = scanner.next();
            int area = scanner.nextInt();
            int density = scanner.nextInt();
            return new Country(name,capital,area,density);
    }
    // getters and setters

    public String getName() {
        return name;
    }

    public String getCapital() {
        return capital;
    }

    public int getArea() {
        return area;
    }

    public int getDensity() {
        return density;
    }

    @Override
    public String toString() {
        return name+" "+ capital +" "+ area+" "+density;
    }
}
