package cat.itb.joseZaquinaula7e4.dam.m03.uf5.exercices.tads.queue;

import java.util.Scanner;

public class Pacient implements Comparable{
    //vars
    private int priority;
    private String name;
    //Contructors

    public Pacient(int priority, String name) {
        this.priority = priority;
        this.name = name;
    }
    //methods
    public static Pacient readPacient(Scanner scanner){
        int p = scanner.nextInt();
        String n = scanner.next();
        return new Pacient(p,n);
    }
    @Override
    public int compareTo(Object o) {
        if(o.equals(this)){
            Pacient ob = (Pacient) o;
            return  ob.getPriority() - priority;
        }
        return 0;
    }
    //getters

    public int getPriority() {
        return priority;
    }

    public String getName() {
        return name;
    }
}
