package cat.itb.joseZaquinaula7e4.dam.m03.uf5.exercices.func.streams;

import cat.itb.joseZaquinaula7e4.dam.m03.uf2.staticfunctions.IntegerLists;

import java.util.List;
import java.util.Locale;
import java.util.Scanner;

/**
 * Llegeix una llista de distàncies per la consola (en format enter, usant IntegerLists.readIntegerList).
 * Converteix cada una de les distàncies a cm i imprimeix-les per pantalla.
 */
public class MetersToCentimeters {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        List<Integer> list = IntegerLists.readIntegerList(scanner);
        list.stream().map(x -> x*1000).forEach(System.out::println); // ME PARECE QUE ES SOLO POR 100 PERO BUANOOO .,_,.
    }
}
