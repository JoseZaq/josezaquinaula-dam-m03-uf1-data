package cat.itb.joseZaquinaula7e4.dam.m03.uf5.exercices.regex;

import java.util.List;
import java.util.Locale;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Volem fer un programa que donat
 * el nom de la UF en un format determinat ens l'imprimeixi en un format amigable per l'usuari.
 */
public class ReadSubjectInfo {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        int cursos = scanner.nextInt();
        String[] cursoList = new String[cursos];
        for (int i = 0; i < cursos; i++) {
            String input = scanner.next();
            cursoList[i] = input;
        }
        for (String s : cursoList) {
            Pattern pattern = Pattern.compile("(\\w+)-M(\\d+)UF(\\d+)");
            Matcher matcher = pattern.matcher(s);
            if(matcher.matches()) {
                String carrera = matcher.group(1);
                int m = Integer.parseInt(matcher.group(2));
                int uf = Integer.parseInt(matcher.group(3));
                System.out.printf("Estàs cursant la unitat formativa %d, del mòdul %d de %s\n", uf, m, carrera);
            }
        }
    }
}
