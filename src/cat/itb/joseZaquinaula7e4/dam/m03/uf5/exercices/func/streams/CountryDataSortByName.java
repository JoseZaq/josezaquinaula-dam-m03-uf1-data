package cat.itb.joseZaquinaula7e4.dam.m03.uf5.exercices.func.streams;

import java.util.*;
import java.util.stream.Collectors;

/**
 * D'un païs ens en volem guardar la següent informació.
 *
 * nom, capital, superfície(km2), densitat (hab./km2)
 * Imprimeix els països de més de amb una densitat superior a 5hab/km2, ordenats per ordre alfabètic.
 *
 * L'usuari primer introduirà la quantitat de països a llegir.
 *
 * Imprimeix els paisos ordenats per ordre alfabètic.
 */
public class CountryDataSortByName {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        List<Country> countries = new ArrayList<>();
        int maxC = scanner.nextInt();
        for (int i = 0; i < maxC; i++) {
            String name = scanner.next();
            String capital = scanner.next();
            int area = scanner.nextInt();
            int density = scanner.nextInt();
            countries.add(new Country(name,capital,area,density));
        }
        // actions
        countries.stream().filter(c -> c.getDensity() > 5).sorted(Comparator.comparing(Country::getName, String.CASE_INSENSITIVE_ORDER)).collect(Collectors.toList()).forEach(System.out::println);
        // el sroted : usa clase Comparator, le pasa el nombre de la ciudad para comparar, y le pide comparar sin tener en cuenta mayusculas :D
    }
}
