package cat.itb.joseZaquinaula7e4.dam.m03.uf5.exercices.func.streams;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;

/**
 * Imprimeix la mitjana de població dels països de menys de 1200000 km2.
 */
public class CountryDataSortAveragePopulation {
    public static void main(String[] args) {
        Scanner     scanner = new Scanner(System.in).useLocale(Locale.US);
        List<Country> countries = new ArrayList<>();
        int maxC = scanner.nextInt();
        for (int j = 0; j < maxC; j++) {
            countries.add(Country.readCountries(scanner));
        }
        // stream
        double avg  = countries.stream().filter(c -> c.getArea() < 1200000)
                .mapToDouble(c -> c.getDensity()* c.getArea()).average().getAsDouble();
        System.out.println(avg);
    }
}
