package cat.itb.joseZaquinaula7e4.dam.m03.uf5.exercices.func.streams;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;

/**
 * Llegeix per pantalla una llista de països, modifica'n el nom i
 * la capital per a que estiguin escrites en majúscules i imprimeix-los.
 */
public class CountryDataUpdateNames {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        List<Country> countries = new ArrayList<>();
        // leer countries
        int maxC = scanner.nextInt();
        for (int j = 0; j < maxC; j++) {
            countries.add(Country.readCountries(scanner));
        }
        // modificar mayusculas e imprimirlos

    }
}
