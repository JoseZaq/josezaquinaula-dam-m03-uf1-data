package cat.itb.joseZaquinaula7e4.dam.m03.uf5.exercices.tads.set;

import java.util.HashSet;
import java.util.Locale;
import java.util.Scanner;
import java.util.Set;

/**
 * Volem fer un petit joc tipus Scatergories o Un, Dos tres,
 * on els diferents jugadors han de dir respostes a una pregunta, però no es poden fer repeticions.
 * Fes un programa que llegeixi la resposta per l'input i printi "MEEEC!" quan hi hagi una repetició.
 * El programa s'acaba quan l'usuari escriu END
 */
public class RepeatedAnswer {
    public static void main(String[] args) {
        //vars
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        Set<String> stringSet = new HashSet<>();
        //actions
        while(true){
            String input = scanner.next();
            if(input.equals("END"))
                break;
            if(!stringSet.add(input))
                System.out.println("MEEEC!");
            System.out.println(stringSet);
        }
    }

}
