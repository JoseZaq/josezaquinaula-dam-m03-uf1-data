package cat.itb.joseZaquinaula7e4.dam.m03.uf5.exercices.regex;

import java.util.Locale;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Donat una linia de text, conta el nombre de paraules plurals que té.
 * Considerarem plurals totes les paraules que acaben en s.
 */
public class CountPlurals {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        int sum = 0;
        String input = scanner.nextLine();
        Pattern pattern = Pattern.compile("\\w+s\\b");
        Matcher matcher = pattern.matcher(input);
        while(matcher.find()) {
            sum++;
            System.out.println(matcher.group());
        }
        System.out.println(sum);
    }
}
