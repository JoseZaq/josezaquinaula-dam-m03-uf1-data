package cat.itb.joseZaquinaula7e4.dam.m03.uf5.exercices.func;

import java.util.Arrays;
import java.util.List;

public class SortByLastDigit {
    public static void main(String[] args) {
        // vars
        List<Integer> list = Arrays.asList(253,432,65,234,43,16,28,432,34,65,312,34,2134,76,2,76,23,67,27,8,54235256,4560,7431);
        // actions
        System.out.println(list);
        list.sort((x1,x2) -> x1 % 10 - x2 % 10);
        System.out.println(list);
    }
}
