package cat.itb.joseZaquinaula7e4.dam.m03.uf5.exercices.func;

import cat.itb.joseZaquinaula7e4.dam.m03.uf5.exercices.tads.map.Employee;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;

/**
 * Usa la classe Employee de l'exercici EmpoyeeById, llegeix una llista (List)
 * d'empleats per pantalla (primer s'introduirà el número d'empleats).
 *
 * Elimina els empleats que tenen un DNI amb la lletra A i imprimeix la llista resultant.
 */
public class FilterEmpoyeeById {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        List<Employee> employees = new ArrayList<>();
        int maxEmp = scanner.nextInt();
        for (int i = 0; i < maxEmp; i++) {
            employees.add(Employee.readEmployee(scanner));
        }
        System.out.println("Antes:\n" + employees);
        employees.removeIf(e -> e.getId().endsWith("A"));
        System.out.println("Despues:");
        employees.forEach(System.out::println);
    }
}
