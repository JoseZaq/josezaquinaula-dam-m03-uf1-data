package cat.itb.joseZaquinaula7e4.dam.m03.uf5.exercices.tads.map;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Scanner;

/**
 * Volem tenir registrats els diferents cartells que té una carretera.
 * La carretera fa més del 1000km, i volem registrar el metre al que hi ha al cartell.
 * Posteriorment volem poder obtenir el cartell que hi ha a un metre determinat.
 *
 * L'usuari primer introduirà el número de cartells, i per cada cartell el metre i el text.
 * Després introduirà un metre i s'ha d'imprimir el cartell o no hi ha cartell.
 * S'acaba quan l'usuari introdueix -1.
 * S'acaba quan l'usuari introdueix -1.
 */
public class RoadSigns {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        Map<Integer,String> signs = new HashMap<>();
        int numberSign = scanner.nextInt();
        for (int i = 0; i < numberSign; i++) {
            int km = scanner.nextInt();
            String sign = scanner.next();
            signs.put(km,sign);
        }
        while(true){
            int kms=scanner.nextInt();
            if(kms == -1)
                return;
            System.out.println(signs.getOrDefault(kms, "No hi ha cap senyal ací"));

        }
    }

}
