package cat.itb.joseZaquinaula7e4.dam.m03.uf5.exercices.regex;

import java.util.Locale;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Considerarem que una contrassenya és de dificultat alta si:
 *
 * té més de 8 caracters
 * té lletres i números
 * té un dels següents caràcters: @,#,~,$
 * no té 3 lletres consecutives
 * no té 3 números consecutius
 * Llegeix els passwords per pantalla i printa sí són de dificultat alta. S'acaba amb la paraula END.
 */
public class DificultPassword {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        String password ="";
        while(true){
            password = scanner.next();
            if(password.equals("END"))
                break;
            Pattern pattern = Pattern.compile("(\\D*\\d(\\w)*)");
            Matcher m = pattern.matcher(password);
            if(m.matches())
                System.out.println("Dificultat alta");
            else
                System.out.println("No és de dificultat alta");
        }
    }
}
