package cat.itb.joseZaquinaula7e4.dam.m03.uf5.exercices.tads.queue;

import java.util.Locale;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Scanner;

/**
 * Un hospital ens ha demanat que els hi fem un programa per a gestionar la cua d'accés a urgencies.
 * Cada pacient tindrà un nom i una prioritat de tipus enter (valor més alt indica més prioritat.
 * Si la línia comença amb un 1 és que ha entrat un pacient al hospital.
 * Si la línia comença amb un 0 és que s'atena un pacient.
 * Si la línia comença amb un -1 és que s'acaba el programa Imprimeix per pantalla
 * les crides dels diferents pacients
 */
public class HospitalQueue {
    public static void main(String[] args) {
        //vars
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        Queue<Pacient> pacientsQueue = new PriorityQueue<>();
        //actions
        while(true){
            int choose = scanner.nextInt();
            if(choose == 1)
                pacientsQueue.add(Pacient.readPacient(scanner));
            if(choose == 0)
                System.out.println(pacientsQueue.poll().getName() + " passi a consulta");
            if(choose == -1)
                break;
        }
    }
}
