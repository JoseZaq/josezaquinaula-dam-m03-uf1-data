package cat.itb.joseZaquinaula7e4.dam.m03.uf5.practica.menus;

import cat.itb.joseZaquinaula7e4.dam.m03.uf5.practica.CovidStatsApp;

import java.util.Scanner;

public abstract class Menu {
    //vars
    CovidStatsApp covidStats;
    Scanner scanner;

    //constructor

    public Menu(Scanner scanner) {
        this.scanner = scanner;
        covidStats= CovidStatsApp.getInstance();
    }

    //methods
    public void showMenu(){
        while(true){
            if(makeMenu())
                return;
        }
    }
    public abstract boolean makeMenu();
}
