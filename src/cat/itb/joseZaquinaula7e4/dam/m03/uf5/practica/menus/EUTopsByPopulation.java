package cat.itb.joseZaquinaula7e4.dam.m03.uf5.practica.menus;

import cat.itb.joseZaquinaula7e4.dam.m03.uf5.practica.Country;
import cat.itb.joseZaquinaula7e4.dam.m03.uf5.practica.utils.Printer;

import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;
import java.util.stream.Collectors;

public class EUTopsByPopulation extends Menu{

    public EUTopsByPopulation(Scanner scanner) {
        super(scanner);
    }

    @Override
    public boolean makeMenu() {
        Printer.printBetweenLines("Relative Tops EU");
        double maxNewCases = covidStats.getCountries().stream().filter(c -> covidStats.getEUCountryNames().contains(c.getId())).
                mapToDouble(c ->(double) c.getTotalCases()/covidStats.getPopulationByName(c.getName())).max().getAsDouble();
        double maxTotalCases = covidStats.getCountries().stream().filter(c -> covidStats.getEUCountryNames().contains(c.getId())).
                mapToDouble(c -> (double) c.getTotalCases()/covidStats.getPopulationByName(c.getName())).max().getAsDouble();
        System.out.printf("Païs amb més casos nous: %.2f%%\nPaïs amb més casos totals: %.2f%%\n",maxNewCases,maxTotalCases);
        return false;
    }
}
