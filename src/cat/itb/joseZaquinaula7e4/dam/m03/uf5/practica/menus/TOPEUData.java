package cat.itb.joseZaquinaula7e4.dam.m03.uf5.practica.menus;

import cat.itb.joseZaquinaula7e4.dam.m03.uf5.practica.Country;
import cat.itb.joseZaquinaula7e4.dam.m03.uf5.practica.CovidStatsApp;
import cat.itb.joseZaquinaula7e4.dam.m03.uf5.practica.utils.Printer;

import java.awt.*;
import java.util.*;
import java.util.List;

public class TOPEUData extends Menu{
    //vars
    public TOPEUData(Scanner scanner) {
        super(scanner);
    }
    //methods
    @Override
    public boolean makeMenu() {
        // creo set de nombres de paises
        List<String> countryNames = covidStats.getEUCountryNames();
        Map<String,Country> countryMap = new HashMap<>();
        // comparo el set con la lista de paises europeos y borro los otros
        // operacion

        int worstTotalCases = covidStats.getCountries().stream().filter(x -> countryNames.contains(x.getId())).
                mapToInt(Country::getTotalCases).max().getAsInt();
        int worstNewCases = covidStats.getCountries().stream().filter(x -> countryNames.contains(x.getId())).
                mapToInt(Country::getNewCases).max().getAsInt();
        //print
        Printer.printBetweenLines("Tops EU");
        System.out.printf("Païs amb més casos nous: %d\nPaïs amb més casos totals: %d\n",
                worstNewCases,worstTotalCases);
        return false;
    }
}
