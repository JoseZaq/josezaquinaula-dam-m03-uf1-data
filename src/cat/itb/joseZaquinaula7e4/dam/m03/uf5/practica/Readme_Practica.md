# Aplicacion de Estadisticas del Covid
## Objetivos
* Analizar las estadisticas de Covid de diferentes paises

## Contenido
* Introducción
* Estructuras y Datos
* Clases

## Introducción
NOTA: PARA PODER VER LOS DIAGRAMAS DE FLUJO DE LAS CLASES PUEDE SER NECESARIO INSTALAR 
MERMAID DE MARKDOWN PREVIAMENTE.


Se quiere realizar una aplicacion que analize las estadisticas del covid de la página: 
https://api.covid19api.com/summary

Estos datos se guardan en un fichero de texto llamado  covidata.txt

Se organizarán los datos por país, las cuales tendrán: 
* nombre
* codigo
* casos nuevos confirmados
* total confirmados
* nuevas muertes
* total de muertos
* nuevos recuperados
* total de recuperados

Se tendrán varios menus: 
* Totals: imprime los casos nuevos totales,y casos totales
* Tops: imprime los paises con mas casos de cada categoria(nuevos y totales)
* Top EU data: imprime los paises con mas casos de cada categoria de la union europea
* Spain By Population: imprime por pantalla cada categoria en porcentaje 
* EU Tops By Population: imprime por pantalla el top europa segun su numero de habitantes
## Estructuras y Datos

### Clases
#### Clase Principal: CovidStatsApp
* Lee el fichero de texto coviddata.txt
* Crea los paises que se encuentran en los datos del fichero de texto
* Crea una lista de paises
* Crea un menu principal que llama a las clases Menus
```mermaid
classDiagram
class CovidStatsApp{
    +CovidStatsApp covidStats 
    +List~Country~ countries
    +String dataCountries
    +readFile(Path path) String 
    +fillCoutriesWithData(String dataCountries) void
    +getInstance()
}
```
#### Country 
```mermaid
classDiagram
class Country{
    +String name
    +String id
    +int newCases
    +int totalCases
    +int newDeaths
    +int totalDeaths
    +int newRecovered
    +int totalRecovered
}
```

#### Clases Menu
##### Menu
clase padre del resto de clases menu
```mermaid
classDiagram
class Menu {
    +Scanner scanner
    +showMenu() void
}
```
#### mainMenu
```mermaid
classDiagram
class mainMenu{ 
    +Menu mainMenu
    +Menu totalsMenu
    +Menu topsMenu
    +Menu topEUDataMenu
    +Menu SpainByPopulation
    +Menu EUTopsByPopulation
}
```
##### Totals
Usa streams para filtrar datos de cada pais
```mermaid
classDiagram
class totalsMenu {
    +makeMenu()
}
    
```
##### Tops
Usa streams para filtrar datos de cada pais
```mermaid
classDiagram
class Tops {
    +makeMenu()
} 
```
##### TopEUData
Usa streams para filtrar datos de cada pais
```mermaid
classDiagram
class TopEUData{
    +makeMenu()
} 
```
##### SpainByPopulation
Usa el fichero de poblacion de los paises, para ver su densidad
```mermaid
classDiagram
class SpainByPopulation {
    +makeMenu()
} 
```

Usa streams para filtrar datos relativos de España
##### EUTopsByPopulation
Usa el fichero de poblacion de los paises, para ver su densidad
```mermaid
classDiagram
class EUTopsByPopulation {
    +makeMenu()
} 
```
Usa streams para filtrar datos relativos de Europa

### Conclusion
El uso de streams permite ahorrar mucho codigo y hacer mas eficiente el 
la aplicacion.
Hay que tomar en cuenta que aunque paresca facil de realizar
comparaciones y acciones con el stream, existen ciertos trucos que ahorran 
mucho mas las lineas de codigo.
Por ejemplo:
* Realizar operaciones en un map: mapToDouble(c ->(double) c.getTotalCases()/covidStats.getPopulationByName(c.getName())
* Usar metodos dentro de un filtro: .filter(x -> countryNames.contains(x.getId()))
