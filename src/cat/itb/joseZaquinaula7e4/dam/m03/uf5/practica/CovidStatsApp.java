package cat.itb.joseZaquinaula7e4.dam.m03.uf5.practica;

import cat.itb.joseZaquinaula7e4.dam.m03.uf5.practica.menus.MainMenu;
import cat.itb.joseZaquinaula7e4.dam.m03.uf5.practica.menus.Menu;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;

public class CovidStatsApp {
    //singleton
    private static CovidStatsApp covidStats;
    public static CovidStatsApp getInstance(){
        if(covidStats == null)
            covidStats  = new CovidStatsApp();
        return covidStats;
    }
    //vars
    private List<Country> countries; //se puede cambiar por un hashMap --- pero para que?
    private List<String> EUCountryNames;
    private Map<String,String> countryCodes;
    private Map<String, Long> population;
    private String dataCountries, dataPopulation,dataCountryCodes;
    //contructors
    public CovidStatsApp() {
        //init
        EUCountryNames = Arrays.asList("BE", "EL", "LT", "PT", "BG", "ES", "LU", "RO", "CZ",
                "FR", "HU", "SI", "DK", "HR", "MT", "SK", "DE", "IT", "NL", "FI", "EE",
                "CY", "AT", "SE", "IE", "LV", "PL");
        countries = new ArrayList<>();
        countryCodes = new HashMap<>();
        population = new HashMap<String, Long>();
        dataCountries = "";
        dataPopulation = "";
        dataCountryCodes = "";
        // filling
        dataCountries = readFile("files/coviddata.txt");
        dataPopulation = readFile("files/population.txt");
        dataCountryCodes= readFile("files/country_codes.txt");
        fillCollections();
    }

    private void fillCollections() {
        String[] dataPopulationArray = dataPopulation.split("(\\n+|\\s+)");
        for (int i = 0; i < dataPopulationArray.length; i+=2) {
            population.put(dataPopulationArray[i], Long.valueOf(dataPopulationArray[i+1]));
        }
        String[] dataCoutryCodesString = dataCountryCodes.split("(\\n+|\\s+)");
        for (int i = 0; i < dataCoutryCodesString.length; i+=2) {
            countryCodes.put(dataCoutryCodesString[i],dataCoutryCodesString[i+1]);
        }
        fillCountriesWithData();
    }

    public String readFile(String stringPath){
        String data ="";
        try {
            data = Files.readString(Path.of(stringPath));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return data;
    }
    public void fillCountriesWithData(){
        String[] data = dataCountries.split("\\n+"); // error al tratar de colocar solo \\n -> cada elemento tenia \r al dividirse el string
        for (int i = 0; i < data.length; i+=8) {
            String name = data[i];
            String id = data[i+1];
            int newCases= Integer.parseInt(data[i+2]);
            int totalCases= Integer.parseInt(data[i+3]);
            int newDeaths= Integer.parseInt(data[i+4]);
            int totalDeaths= Integer.parseInt(data[i+5]);
            int newRecovered= Integer.parseInt(data[i+6]);
            int totalRecovered= Integer.parseInt(data[i+7]);
            countries.add(new Country(name,id,newCases,totalCases,newDeaths,
                    totalDeaths,newRecovered,totalRecovered));
        }
    }
    //gettes and setters
    public List<Country> getCountries() {
        return countries;
    }

    public Country getCountryByName(String countryName){
        for(Country country : countries){
            if(country.getName().equals(countryName)){
                return country;}
        }
        return null;
    }
    public long getPopulationByName(String countryName){
        return getPopulationByCountryId(getCountryByName(countryName).getId());
    }
    public long getPopulationByCountryId(String key){
        return population.get(countryCodes.get(key));
    }

    public List<String> getEUCountryNames() {
        return EUCountryNames;
    }
    // main
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        Menu mainMenu = new MainMenu(scanner);
        mainMenu.showMenu();
    }
}

