package cat.itb.joseZaquinaula7e4.dam.m03.uf5.practica.utils;

/**
 * herramientas esteticas de impresion
 */
public class Printer {
    /**
     * imprime una cadena de caracteres entre dos barras
     * @param string cadena de caracteres a imprimir
     */
    public static void printBetweenLines(String string){
        StringBuilder line= new StringBuilder();
        int lineSize = string.length()+8;
        line.append("-".repeat(Math.max(0, lineSize)));
        System.out.println(line+"\n"+"    "+string+"\n"+line);
    }
}
