package cat.itb.joseZaquinaula7e4.dam.m03.uf5.practica.menus;

import cat.itb.joseZaquinaula7e4.dam.m03.uf5.practica.Country;
import cat.itb.joseZaquinaula7e4.dam.m03.uf5.practica.CovidStatsApp;
import cat.itb.joseZaquinaula7e4.dam.m03.uf5.practica.utils.Printer;

import java.util.List;
import java.util.Scanner;

public class Totals extends  Menu{
    //Vars
    //contructor
    public Totals(Scanner scanner) {
        super(scanner);
        //intancia de covidstatsapp
    }
    //methods
    @Override
    public boolean makeMenu() {
        Printer.printBetweenLines("DADES TOTALS");
        System.out.printf("casos nous: %d \n" +
                "casos totals: %d\n", totalNewCases(),totalCases());
        return true;
    }
    public int totalNewCases(){
        List<Country> countries = covidStats.getCountries();
        return countries.stream().mapToInt(Country::getNewCases).sum();
    }
    public int totalCases(){
        List<Country> countries = covidStats.getCountries();
        return countries.stream().mapToInt(Country::getTotalCases).sum();
    }
}
