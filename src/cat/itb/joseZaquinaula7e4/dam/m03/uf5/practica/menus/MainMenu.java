package cat.itb.joseZaquinaula7e4.dam.m03.uf5.practica.menus;

import cat.itb.joseZaquinaula7e4.dam.m03.uf5.practica.utils.Printer;

import java.util.Scanner;

public class MainMenu extends Menu{
    //vars
    Menu totalsMenu;
    Menu topsMenu;
    Menu topEUDataMenu;
    Menu spainByPopulation;
    Menu eUTopsByPopulation;
    //Contructors

    public MainMenu(Scanner scanner) {
        super(scanner);
        //menus
        totalsMenu = new Totals(scanner);
        topsMenu = new Tops(scanner);
        topEUDataMenu = new TOPEUData(scanner);
        spainByPopulation = new SpainByPopulation(scanner);
        eUTopsByPopulation = new EUTopsByPopulation(scanner);
    }
    @Override
    public boolean makeMenu(){
        Printer.printBetweenLines("Covid Stats App");
        Printer.printBetweenLines("Menu Principal");
        System.out.println("1. Totals\n" +
                "2. Tops\n" +
                "3. TOP EU data\n" +
                "4. Spain By Population\n" +
                "5. EU Tops By Population\n" +
                "6. Exit");
        int option = scanner.nextInt();
        switch (option) {
            case 1:
                totalsMenu.makeMenu();
                break;
            case 2:
                topsMenu.makeMenu();
                break;
            case 3:
                topEUDataMenu.makeMenu();
                break;
            case 4:
                spainByPopulation.makeMenu();
                break;
            case 5:
                eUTopsByPopulation.makeMenu();
                break;
            case 6:
                System.out.println("Graciés :D");
                return true;
            default:
                System.out.println("Opcion inexistente");
                break;
        }
        return false;
    }
}
