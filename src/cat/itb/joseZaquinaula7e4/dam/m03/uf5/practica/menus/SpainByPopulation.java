package cat.itb.joseZaquinaula7e4.dam.m03.uf5.practica.menus;

import cat.itb.joseZaquinaula7e4.dam.m03.uf5.practica.Country;
import cat.itb.joseZaquinaula7e4.dam.m03.uf5.practica.utils.Printer;

import java.util.Scanner;

public class SpainByPopulation extends Menu{
    // vars
    // contructors
    public SpainByPopulation(Scanner scanner) {
        super(scanner);
    }
    // methods
    @Override
    public boolean makeMenu() {
        long population = covidStats.getPopulationByName("Spain");
        Country spain = covidStats.getCountryByName("Spain");
        // densidad
        Printer.printBetweenLines("Spain Relative");
        System.out.printf("casos nous relatius: %.2f%%\ncasos totals relatius: %.2f%%\n",
                (double) spain.getNewCases()/population*100,(double) spain.getTotalCases()/population*100);
        return false;
    }
}
