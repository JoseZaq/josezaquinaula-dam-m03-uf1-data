package cat.itb.joseZaquinaula7e4.dam.m03.uf5.practica.menus;

import cat.itb.joseZaquinaula7e4.dam.m03.uf5.practica.Country;
import cat.itb.joseZaquinaula7e4.dam.m03.uf5.practica.utils.Printer;

import java.util.Collections;
import java.util.Comparator;
import java.util.Scanner;

public class Tops extends Menu{
    //Vars
    public Tops(Scanner scanner) {
        super(scanner);
    }

    @Override
    public boolean makeMenu() {
        Printer.printBetweenLines("Tops");
        Country worstNewCasesCountry = Collections.max(covidStats.getCountries(),
                Comparator.comparingInt(Country::getNewCases));
        Country worstTotalsCasesCountry = Collections.max(covidStats.getCountries(),
                Comparator.comparingInt(Country::getTotalCases));
        System.out.printf("Païs amb més casos nous: %s\nPaïs amb més casos totals: %s\n",
                worstNewCasesCountry.getName(),worstTotalsCasesCountry.getName());
        return true;
    }
}
