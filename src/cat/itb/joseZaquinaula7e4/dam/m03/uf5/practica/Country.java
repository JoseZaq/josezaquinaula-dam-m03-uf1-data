package cat.itb.joseZaquinaula7e4.dam.m03.uf5.practica;

public class Country {
    //vars
    private String name;
    private String id;
    private int newCases;
    private int totalCases;
    private int newDeaths;
    private int totalDeaths;
    private int newRecovered;
    private int totalRecovered;
    //contructors

    public Country(String name, String id, int newCases,
                   int totalCases, int newDeaths, int totalDeaths,
                   int newRecovered, int totalRecovered) {
        this.name = name;
        this.id = id;
        this.newCases = newCases;
        this.totalCases = totalCases;
        this.newDeaths = newDeaths;
        this.totalDeaths = totalDeaths;
        this.newRecovered = newRecovered;
        this.totalRecovered = totalRecovered;
    }
    //methods
    //getters and setters

    public String getName() {
        return name;
    }

    public String getId() {
        return id;
    }

    public int getNewCases() {
        return newCases;
    }

    public int getTotalCases() {
        return totalCases;
    }

    public int getNewDeaths() {
        return newDeaths;
    }

    public int getTotalDeaths() {
        return totalDeaths;
    }

    public int getNewRecovered() {
        return newRecovered;
    }

    public int getTotalRecovered() {
        return totalRecovered;
    }
}
