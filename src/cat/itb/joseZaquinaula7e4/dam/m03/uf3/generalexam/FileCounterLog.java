package cat.itb.joseZaquinaula7e4.dam.m03.uf3.generalexam;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.Locale;
import java.util.Scanner;

/**
 * Volem fer un petit programa que ens generi un log de quants fitxers tenen diferents carpetes.
 *
 * L'usuari introduirà un path.
 * Escriu una linia nova al fitxer ~/counterlog.txt que tingui la següent línia
 * data_hora: Tens numero fitxers a path.
 *
 * Exemple
 * Input
 * /home/sjo/Desktop
 * Output
 * S'agefirà al fitxer ~/counterlog.txt la següent línia
 *
 * 2021-02-16T09:59:04.343769 - Tens 15 fitxers a /home/sjo/Desktop
 */
public class FileCounterLog {
    public static void main(String[] args) {
        //ruta indicada por el usuario
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        String stringFile = scanner.next();
        Path pathFile = Path.of(stringFile);
        //ruta del counterlog.txt
        String stringHome = System.getProperty("user.home");
        Path counterlogPath = Path.of(stringHome,"counterlog.txt");
        try {
            if(!Files.exists(counterlogPath))
                Files.createFile(counterlogPath);
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("error el crear el path");
        }
        //crea la linea a colocar en counterlog
        File file = new File(stringFile);
        File[] numFiles = file.listFiles();
        String date = LocalDateTime.now().toString();
        String line="";
        if(numFiles != null)
            line = String.format("%s - Tens %d fitxers a %s",date,numFiles.length,stringFile);
        //escribe en counterlog la linea creada
        try {
            Files.write(counterlogPath, Collections.singleton(line), StandardOpenOption.APPEND);
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("error el escribir en el archivo");
        }
    }
}
