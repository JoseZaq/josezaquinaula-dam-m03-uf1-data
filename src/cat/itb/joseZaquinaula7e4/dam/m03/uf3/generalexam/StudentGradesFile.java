package cat.itb.joseZaquinaula7e4.dam.m03.uf3.generalexam;

import cat.itb.joseZaquinaula7e4.dam.m03.uf2.dataclasses.GradeCalculator;
import cat.itb.joseZaquinaula7e4.dam.m03.uf2.dataclasses.StudentGrade;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Locale;
import java.util.Scanner;

/**
 * Volem fer un petit programa que llegeixi les notes parcials de diferents estudiants que tenim guardats en un txt i
 * ens imprimeix la nota dels estudiants i un recompte d'aprovats i suspesos.
 * Es considerarà suspesos notes inferiors a 5.
 * El fitxer contindrà primer el número d'estudiants a llegir.
 * Després el nom de l'estudiant (és una paraula sense espais) i les notes del exercicis,
 * exàmen i projecte (en aquest ordre). Aquí tens un exemple de fitxer.
 *
 * Per calcular la nota final hem d'usar la següent funció:
 * nota = nota_{exercicis} * 0.3 + nota_{examen} * 0.3 + nota_{projecte} * 0.4
 * L'usuari introdurià per pantalla la ruta del fitxer.
 * Imprimeix les dades de cada estudiant i un resum
 *
 * Input
 * /home/sjo/students.txt
 * Output
 * ----- Estudiants -----
 * Ot: 6.3
 * Mar: 8.0
 * Ona: 2.6
 * ----- Resum -----
 * Aprovats: 2
 * Suspesos: 1
 */
public class StudentGradesFile {
    public static void main(String[] args) {
        //ruta indicada por el usuario
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        String stringFile = scanner.next();
        Path pathFile = Path.of(stringFile);
        //imprime la informacion de los estudiantes
        try {
            Scanner scannerFile = new Scanner(pathFile).useLocale(Locale.US);
            StudentGrade[] studentGrade = GradeCalculator.getStudents(scannerFile);
            GradeCalculator.printStudentsGrade(studentGrade);
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Error al escanear el archivo");
        }
    }
}
