package cat.itb.joseZaquinaula7e4.dam.m03.uf3.generalexam;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.Locale;
import java.util.Scanner;

/**
 * Volem fer un petit programa que fagi una copia del fitxer ~/.bashrc a una carpeta indicada per l'usuari.
 * L'usuari indirarà una ruta, el nom de la persona i el seu cognom.
 * Fes una copia del fitxer ~/.bashrc a la carpeta ruta/cognom/nom
 * Input
 * /home/sjo/user_data
 * Mar
 * Pi
 * Resultat
 * Ha copiat el fitxer .bashrc a la carpeta /home/sjo/user_data/Pi/Mar
 *
 * Output
 * Fitxer copiat a la carpeta /home/sjo/user_data/Pi/Mar
 */
public class CopyToUserFolder {
    public static void main(String[] args) {
        //ruta indicada por el usuario
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        String stringFile = scanner.next();
        Path pathFile = Path.of(stringFile);
        //ingresa nom y cognom
        String name = scanner.next();
        String lname = scanner.next();
        //crea el path donde guardar la copia
        Path pathCopyFile = pathFile.resolve(lname+"/"+name);
        //copia el archivo .basch
        String stringHome = System.getProperty("user.home");
        Path bashrcPath = Path.of(stringHome,".bashrc");
        try {
            if(!Files.exists(pathCopyFile))
                Files.createDirectories(pathCopyFile);
            Files.copy(bashrcPath,pathCopyFile.resolve(bashrcPath.getFileName()), StandardCopyOption.REPLACE_EXISTING);
            System.out.printf("Fitxer copiat a la carpeta %s\n",pathCopyFile);
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("error al copiar el archivo");
        }
    }
}
