package cat.itb.joseZaquinaula7e4.dam.m03.uf3.exercices;

import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;

/**
 * Fes un programa que fagi un backup del fitxer .profile.
 *
 * Guarda el fitxer ~/.profile a la carpeta ~/backup/2021-02-16T09:59:04.343769/.profile
 *
 * Podeu obtenir la data actual usant:
 *
 * LocalDateTime.now().toString()
 */
public class ProfileBackup {
    public static void main(String[] args) {
        String home = System.getProperty("user.home");
        String profilePathStr = ".profile";
        Path pathFile = Path.of(home,"m3");
        Path backupFile = Paths.get(pathFile.toString(),LocalDateTime.now().toString());
        Path mainProfile = pathFile.resolve(profilePathStr);
        Path backupProfile = backupFile.resolve(profilePathStr);
        //crear el directorio de .profile
        try{
            Files.createDirectories(mainProfile);
            Files.createDirectories(backupFile);
            System.out.println("Carpeta creada");
        }catch (IOException e){
            System.out.println("Error al crear la carpeta");
            e.printStackTrace();
        }
        //crear el directorio del backup
        try {
            Files.createDirectories(mainProfile);
            Files.createDirectories(backupProfile);
            System.out.println("directorios creados con exito");
        }catch (IOException e){
            e.printStackTrace();
            System.out.println("Error al crear los directorios ");
        }
        try {
            Files.copy(mainProfile, backupProfile);
            System.out.printf("backup de la carpeta %s realizada",profilePathStr);
        }catch (FileAlreadyExistsException er){
            System.out.print("Carpeta ya copiada ._.");
        }
        catch (IOException e){
            e.printStackTrace();
            System.out.println("Error al realizar el backup D: ");
        }
    }
}
