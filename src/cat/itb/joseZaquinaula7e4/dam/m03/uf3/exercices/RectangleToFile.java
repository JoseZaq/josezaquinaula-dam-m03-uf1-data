package cat.itb.joseZaquinaula7e4.dam.m03.uf3.exercices;

import cat.itb.joseZaquinaula7e4.dam.m03.uf2.classfun.Rectangle;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.Locale;
import java.util.Scanner;

/**
 * Volem fer un programa que llegeixi de l'entrada una llista de rectangles.
 * Quan els hagi llegit tots, guardarem en un fitxer la informació de cada rectangle.
 *
 * L'usuari indrodueix el número de rectangles
 * L'usuari entra els dos costats d'un rectangle
 * L'usuari entre la ruta del fitxer
 * Guarda al fitxer la superfície i el perímetre dels diferents rectangles
 * Nota: No pots crear el fitxer fins a que s'hagin llegit tots els rectangles.
 *
 * Input
 * 2
 * 2.0 4.0
 * 3.5 5.4
 * ./rectangles.txt
 * Output
 * Fitxer
 * En el fitxer ./rectangles.txt
 *
 * Un rectangle de 2.0 x 4.0 té 8.0 d'area i 12 de perímetre.
 * Un rectangle de 3.5 x 5.4 té 18.9 d'area i 17.8 de perímetre.
 * Consola
 * Dades enregistrades
 */
public class RectangleToFile {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        //pido info de los rectangulos
        int numRect = scanner.nextInt();
        Rectangle[] rectangles = new Rectangle[numRect];
        for (int i = 0; i < rectangles.length; i++) {
            double side1 = scanner.nextDouble();
            double side2 = scanner.nextDouble();
            rectangles[i]  = new Rectangle(side1,side2);
        }
        // guardo path
        Path pathFile = Path.of(scanner.next());
        try{
            if(!Files.exists(pathFile))
                Files.createFile(pathFile);
        }catch (IOException e){
            System.out.println("Error al crear el fichero");
        }
        // guardo linea
        for(Rectangle rectangle : rectangles){
            try {
                Files.writeString(pathFile,rectangle.toString(), StandardOpenOption.APPEND);

            } catch (IOException e) {
                e.printStackTrace();
                System.out.println("Error al agregar texto al fichero");
            }
        }
        System.out.println("Dades enregistrades");
    }
}
