package cat.itb.joseZaquinaula7e4.dam.m03.uf3.exercices;

import java.util.InputMismatchException;
import java.util.Locale;
import java.util.Scanner;

/**
 * Fes un programa que llegeixi l'entrada de l'usuari. Si l'usuari no introdueixi un enter printa No és un enter.
 * Si l'usuari introdueixi un enter printa'l amb el missatge Número: X
 */
public class WaitForInt {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        try{
            int num = scanner.nextInt();
            System.out.println(num);
        }catch (InputMismatchException inputMismatchException){
            System.out.println("No és un enter");
        }
    }
}
