package cat.itb.joseZaquinaula7e4.dam.m03.uf3.exercices;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Locale;
import java.util.Scanner;

/**
 * Una empresa militar ens contacta per a que fem un programa per introduir el codi secret per l'explosió d'un missil.
 * L'usuari introduirà un text per pantalla i l'haurem de guardar en el fitxer secret.txt de la carpeta actual.
 * Imprimeix missil preparat quan s'hagi escrit el fitxer.
 */
public class MisileSecretCode {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        String secretPhrase = scanner.nextLine();
        try {
            Path secretPath = Files.createFile(Path.of("./secret.txt"));
            Files.writeString( secretPath,secretPhrase);
            System.out.println("missil preparat");
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Archivo no creado D:");
        }
    }
}
