package cat.itb.joseZaquinaula7e4.dam.m03.uf3.exercices;

import java.io.File;
import java.nio.file.Path;
import java.util.Locale;
import java.util.Scanner;

/**
 * Fes un programa que, donada una carpeta d'un projecte java cerqui un fitxer
 * java que contingui el text List i imprimeixi els paths per pantalla
 */
public class FindJavaFile {
    public static void main(String[] args) {
        //ruta de la carpeta java
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        String stringPath = scanner.next();
        Path pathFile = Path.of(stringPath);
        //buscar los archivos que contengan la palabra List
        searchWordinPathRecursive(pathFile);


    }

    private static void searchWordinPathRecursive(Path path) {
        //si no hay archivos retorna
        File javaFile = new File(String.valueOf(path));
        if(javaFile.listFiles() == null)
            return;
        //busca la palabra list
        File[] javaFiles;
        javaFiles = javaFile.listFiles();
        for (File file : javaFiles) {
            if ((file.getName().endsWith("java"))) {
                Scanner scannerFile = new Scanner(file.toString());
                while (scannerFile.hasNext()) { //ERROR¡¡ al ser ficheros .java no lee su contenido
                    if (scannerFile.next().equals("List"))
                        System.out.println(file); //si existe una palabra List en el archivo, imprime su path
                }
            }
            searchWordinPathRecursive(file.toPath());
        }

    }
}
