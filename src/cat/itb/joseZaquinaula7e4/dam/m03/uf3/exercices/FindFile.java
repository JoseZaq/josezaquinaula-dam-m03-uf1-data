package cat.itb.joseZaquinaula7e4.dam.m03.uf3.exercices;

import java.io.File;
import java.nio.file.Path;
import java.util.Locale;
import java.util.Scanner;

/**
 * Fes un programa que donat un path i un nom de fitxer, miri si hi ha algun fitxer a dins
 * el path amb aquest nom (mirar a dins de carpetes també).
 * Per cada ocurrencia, imprimeix el path per pantalla.
 */
public class FindFile {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        String pathStr = scanner.nextLine();
        String fileName = scanner.nextLine();
        //
        File pathFile = new File(pathStr);
        //
        File[] listFile = pathFile.listFiles();
        searchFileByName(listFile,fileName);
    }

    private static void searchFileByName(File[] listFile,String fileName) {
        if(listFile == null){
            return;
        }
        for (File file : listFile) {
            if (file.getName().equals(fileName)) {
                System.out.println(file);
            }
            searchFileByName(file.listFiles(),fileName);
        }
    }
}
