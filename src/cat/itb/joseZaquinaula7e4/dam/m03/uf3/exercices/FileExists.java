package cat.itb.joseZaquinaula7e4.dam.m03.uf3.exercices;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Locale;
import java.util.Scanner;

/**
 * Demana a l'usuari un path. Retorna true si existeix, false sinó.
 * input: /home output: true
 */
public class FileExists {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        String pathStr = scanner.next();
        Path path = Paths.get(pathStr);
        boolean exist = Files.exists(path);
        System.out.println(exist);

    }
}
