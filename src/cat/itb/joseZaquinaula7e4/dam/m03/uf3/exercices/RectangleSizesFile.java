package cat.itb.joseZaquinaula7e4.dam.m03.uf3.exercices;

import cat.itb.joseZaquinaula7e4.dam.m03.uf2.classfun.Rectangle;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Locale;
import java.util.Scanner;

/**
 * Volem fer un programa que ens digui l'area de rectangles enmagatzemats en un fitxer El fitxer contindrà:
 * número de rectangles
 * una línia nova per cada rectangle amb els dos costats Et pots descarregar un exemple de fitxer aquí
 * L'usuari introduirà el path al fitxer amb els rectangles.
 * Imprimeix la superfície i el perímetre dels diferents rectangles.
 *
 * Notes:
 *
 * No pots imprimir els rectangles fins a que s'hagin llegit tots.
 * Mira't l'exercici ja resolt RectangleSize de la UF2. Reutilitza tant codi com puguis.
 * Input
 * ./rectangles.txt
 * Output
 * Un rectangle de 2.0 x 4.0 té 8.0 d'area i 12 de perímetre.
 * Un rectangle de 3.5 x 5.4 té 18.9 d'area i 17.8 de perímetre.
 */
public class RectangleSizesFile {
    public static void main(String[] args) {
        //ingresar path
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        String pathString = scanner.next();
        Path pathFile = Path.of(pathString);
        // leyendo el path
        try {
            Scanner scannerRect = new Scanner(pathFile).useLocale(Locale.US);
            int numRect = scannerRect.nextInt();
            Rectangle[] rectangles = new Rectangle[numRect];
            for (int i = 0; i < rectangles.length; i++) {
                rectangles[i] = Rectangle.makeRectangle(scannerRect);
            }
            for (Rectangle rectangle : rectangles) {
                System.out.println(rectangle);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
