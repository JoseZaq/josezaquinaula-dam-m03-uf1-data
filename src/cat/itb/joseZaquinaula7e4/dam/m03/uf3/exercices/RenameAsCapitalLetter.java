package cat.itb.joseZaquinaula7e4.dam.m03.uf3.exercices;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.Locale;
import java.util.Scanner;

/**
 * Demana a l'usuari un path. Renombra tots els fitxers
 * que hi ha a dins per tal que els noms estiguin amb lletres majuscules.
 */
public class RenameAsCapitalLetter {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        String input = scanner.nextLine();
        Path path = Path.of(input);
        if(Files.isDirectory(path)){
            File mainFile= path.toFile();
            File[] subFiles= mainFile.listFiles();
            for (File subFile : subFiles) {
                File file = new File(subFile.toString());
                File fileUpperCase = new File(String.valueOf(Path.of(path.toString(),subFile.getName().toUpperCase())));
                try {
                    Files.move(file.toPath(),fileUpperCase.toPath(), StandardCopyOption.REPLACE_EXISTING);
                    System.out.println("Renombrado Correcto");
                } catch (IOException e) {
                    e.printStackTrace();
                    System.out.println("error al copiar");
                }

            }
        }
    }
}
