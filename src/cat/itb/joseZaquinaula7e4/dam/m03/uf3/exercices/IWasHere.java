package cat.itb.joseZaquinaula7e4.dam.m03.uf3.exercices;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.time.LocalDateTime;

/**
 * Fes un programa que cada cop que s'executi afegeixi una linia al fitxer ~/i_was_here.txt amb la següent linia:
 * I Was Here: 2021-02-16T09:59:04.343769
 */
public class IWasHere {
    public static void main(String[] args) {
        String stringPath = System.getProperty("user.home");
        Path filePath = Path.of(stringPath);
        filePath  = filePath.resolve("i_was_here.txt");
        try{
            if(!Files.exists(filePath))
                Files.createFile(filePath);
            String currentDate = LocalDateTime.now().toString();
            Files.writeString(filePath,String.format("\nI Was Here: %s",currentDate), StandardOpenOption.APPEND);
        }catch (IOException e){
            System.out.println(e.toString());
        }
    }
}
