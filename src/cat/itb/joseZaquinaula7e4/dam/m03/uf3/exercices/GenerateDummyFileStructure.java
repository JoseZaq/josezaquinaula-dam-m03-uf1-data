package cat.itb.joseZaquinaula7e4.dam.m03.uf3.exercices;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Locale;
import java.util.Scanner;

/**
 * Crea la carpeta ~/dummyfolders que contingui 100 carpetes numerades del 1 al 100.
 *
 * Nota Pots obtenir la carpeta home amb la següent comanda
 *
 * String homePath = System.getProperty("user.home");
 */
public class GenerateDummyFileStructure {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        String homePath = System.getProperty("user.home");
        Path home = Path.of(homePath);
        Path dummyFolders = home.resolve("dummyFolders");
        try {
            Files.createDirectory(dummyFolders);
            System.out.println("Carpeta creada correctamente");
        } catch (FileAlreadyExistsException er){
            System.out.println("Carpeta ya creada ._.");
        } catch (IOException e) {
            System.out.println("Error al intentar la carpeta");
            e.printStackTrace();
        }
        createOneHundredFiles(dummyFolders);
    }

    public static void createOneHundredFiles(Path path) {
        for (int i = 0; i < 100; i++) {
            Path subFolder = path.resolve(Integer.toString(i));
            try {
                Files.createDirectory(subFolder);
                System.out.print("carpeta " +i + " creada");
            }catch (FileAlreadyExistsException er){
                System.out.println("Subcarpeta ya creada :_:");
            }
            catch (IOException e){
                e.printStackTrace();
                System.out.println("Error al crear la subcarpeta");
            }
            System.out.println();
        }
    }

}
