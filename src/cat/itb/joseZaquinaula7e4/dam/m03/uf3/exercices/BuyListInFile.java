package cat.itb.joseZaquinaula7e4.dam.m03.uf3.exercices;

/**
 * Fes un programa que ens ajudi a crear una llista de la compra. Al executar-se l'usuari introdurià les dades d'un producte.
 * Del producte en guardarà el nom, el preu i la quantitat que en vol comprar.
 * Després imprimeix el resum de la compra que ha anat afegint en les diferents execucions del programa (guarda la informació en un fitxer format text).
 * Recorda que vas fer un exercici semblant per la UF2 anomenat BuyList. Pots usar el codi que ja tens.
 */
public class BuyListInFile {

}
