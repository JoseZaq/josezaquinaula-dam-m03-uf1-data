package cat.itb.joseZaquinaula7e4.dam.m03.uf3.exercices;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Locale;
import java.util.Scanner;

/**
 * Volem fer un programa que,
 * donat un fitxer de text ens indiqui quantes línies té i quantes paraules té.
 */
public class EssayAnalyser {
    public static void main(String[] args) {
        //get the path file
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        String pathString = scanner.next();
        Path pathFile = Path.of(pathString);
        // contabilitzador de lineas y paraules
        try {
            Scanner pathScanner = new Scanner(pathFile);
            int lines=0;
            int words = 0;
            while(pathScanner.hasNext()){
                String line = pathScanner.nextLine();
                lines++;
                String[] word = line.split(" ");
                words += word.length;
            }
            System.out.println("Número de línies: " + lines);
            System.out.println("Número de paraules: " + words);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
