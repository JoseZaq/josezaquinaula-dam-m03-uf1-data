package cat.itb.joseZaquinaula7e4.dam.m03.uf3.exercices;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;

/**
 * Un radar de transit va guardant les diferents velocitats que registra en un fitxer. Guarda les velocitats en km/hora,
 *  separades per un espai. Et pots descarregar un exemple de fitxer aquí
 * Fes un programa demani a l'usuai la ruta del fitxer i  que calculi la velocitat màxima, mitjana i mínima detectada.
 *
 * Input
 * /home/sjo/radar.txt
 *
 * Output
 * Velocitat màxima: 100km/h Velocitat mínima: 50km/h Velocitat mitjana: 75km/h
 */
public class RadarFile {
    public static void main(String[] args) {
        //ingresar path
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        String pathString = scanner.next();
        Path pathFile = Path.of(pathString);
        // calcular valores de velocidad
        List<Integer> speedList = new ArrayList<>();
        try {
            Scanner pathScanner = new Scanner(pathFile);
            while(pathScanner.hasNext()){
                String line = pathScanner.nextLine();
                 String[] speedStringList = line.split(" ");
                for (String s : speedStringList) {
                    speedList.add(Integer.valueOf(s));
                }
            }
            int max = maxListValue(speedList);
            int min = minListValue(speedList);
            float avg = avgListValue(speedList);
            System.out.printf("Velocitat màxima: %dkm/h Velocitat mínima: %dkm/h Velocitat mitjana: %.2fkm/h",
                    max,min,avg);
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Error al leer el fichero D:");
        }
    }

    /**
     * calcula el valor maximo de la lista
     * @param list lista de enteros
     * @return valor maximo de la lista de entrada
     */
    private static float avgListValue(List<Integer> list) {
        float sum=0;
        for (Integer integer : list) {
            sum += integer;
        }
        return sum/list.size();
    }

    /**
     * calcula el valor minimo de la lista dada
     * @param list lista de enteros
     * @return valor minimo d la lsita dada
     */
    private static int minListValue(List<Integer> list) {
        int min = list.get(0);
        for (int i = 1; i < list.size(); i++) {
            if( min > list.get(i))
                min = list.get(i);
        }
        return min;
    }

    /**
     * calcula el promedio de los calores de una lista
     * @param list lista de enteros
     * @return valor flotante del promedio de los valores de la lista
     */
    private static int maxListValue(List<Integer> list) {
        int max = list.get(0);
        for (int i = 1; i < list.size(); i++) {
            if( max < list.get(i))
                max = list.get(i);
        }
        return max;
    }
}
