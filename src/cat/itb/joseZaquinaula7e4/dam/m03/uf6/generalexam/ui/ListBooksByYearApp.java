package cat.itb.joseZaquinaula7e4.dam.m03.uf6.generalexam.ui;

import cat.itb.joseZaquinaula7e4.dam.m03.uf6.generalexam.data.BookDAO;
import cat.itb.joseZaquinaula7e4.dam.m03.uf6.generalexam.data.DataBase;
import cat.itb.joseZaquinaula7e4.dam.m03.uf6.generalexam.models.Book;

import java.util.*;

/**
 * L'usuari introduirà un any. Mostra tots els llibres de l'any introduït.
 */
public class ListBooksByYearApp {
    public static void main(String[] args) {
        //
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        //
        DataBase.getInstance().connect();
        int year = scanner.nextInt();
        listBooksByYear( year );
        //
        DataBase.getInstance().close();
    }
    protected static void listBooksByYear(int year) {
        List<Book> book = BookDAO.listByYear( year ); // no me deja hacer si nstatic
        List<Integer> maxCL = new ArrayList<>();
        //
        maxCL.add(book.stream().map(Book::getTitle)
                .max(Comparator.comparingInt(x-> (x == null) ? 0:x.length())).get().length());
        maxCL.add(book.stream().map(Book::getAuthor)
                .max(Comparator.comparingInt(x-> (x == null) ? 0:x.length())).get().length());
        maxCL.add(book.stream().map(Book::getISBN)
                .max(Comparator.comparingInt(x-> (x == null) ? 0:x.length())).get().length());
        maxCL.add( book.stream().map(x->String.valueOf(x.getYear()))
                .max(Comparator.comparingInt(String::length)).get().length());
        maxCL.add( book.stream().map(x->String.valueOf(x.getPages()))
                .max(Comparator.comparingInt(String::length)).get().length());
        //
        book.forEach(x -> Printer.printRow(x.toStringPretty(maxCL)));
    }
}
