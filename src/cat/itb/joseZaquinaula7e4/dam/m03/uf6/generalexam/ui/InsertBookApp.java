package cat.itb.joseZaquinaula7e4.dam.m03.uf6.generalexam.ui;

import cat.itb.joseZaquinaula7e4.dam.m03.uf6.generalexam.data.BookDAO;
import cat.itb.joseZaquinaula7e4.dam.m03.uf6.generalexam.data.DataBase;
import cat.itb.joseZaquinaula7e4.dam.m03.uf6.generalexam.models.Book;

import java.util.Locale;
import java.util.Scanner;

/**
 * Demana a l'usuari el diferents atributs del llibre i inserta'l a la base de dades.
 * Nota: Si intentes introduir un llubreamb un isbn existent et fallarà per PK
 */
public class InsertBookApp {
    public static void main(String[] args) {
        //
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        //
        DataBase.getInstance().connect();
        //
        Book book = Book.readBook(scanner);
        if (BookDAO.insert(book))
            System.out.println("## Book inserted");
        //
        DataBase.getInstance().close();
    }
}
