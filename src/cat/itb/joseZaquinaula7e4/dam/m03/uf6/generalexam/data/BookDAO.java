package cat.itb.joseZaquinaula7e4.dam.m03.uf6.generalexam.data;

import cat.itb.joseZaquinaula7e4.dam.m03.uf6.generalexam.models.Book;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class BookDAO {
    public static boolean insert(Book book){
        try {
            Connection connection = DataBase.getInstance().getConnection();
            String query = "INSERT INTO book VALUES(?,?,?,?,?)";
            PreparedStatement insertStatement = connection.prepareStatement(query);
            insertStatement.setString(1,book.getTitle());
            insertStatement.setString(2,book.getAuthor());
            insertStatement.setString(3,book.getISBN());
            insertStatement.setInt(4,book.getYear());
            insertStatement.setInt(5,book.getPages());
            insertStatement.execute();
            return true;
        } catch (SQLException e){
            System.out.println(e.getMessage());
        }
        return false;
    }
    public static List<Book> listByYear(int year){
        List<Book> list = new ArrayList<>();
        try{
            Connection connection = DataBase.getInstance().getConnection();
            String query = "SELECT * FROM book where year = ? ORDER BY isbn";
            PreparedStatement selectByYearStatement = connection.prepareStatement(query);
            selectByYearStatement.setInt(1, year);
            ResultSet result = selectByYearStatement.executeQuery();
            while(result.next()){
                String title = result.getString("title");
                String author = result.getString("author");
                String isbn = result.getString("isbn");
                int pages = result.getInt("pages");
                list.add(new Book(title, author, isbn, year, pages));
            }
        }catch (SQLException e){
            System.out.println(e.getMessage());
        }
        return  list;
    }
    public static Book largestPages(){
        Book book = null;
        try{
            Connection connection = DataBase.getInstance().getConnection();
            String query = "select * from book where pages = (select max(pages) from book)";
            Statement listStatement = connection.createStatement();
            ResultSet result = listStatement.executeQuery(query);
            while(result.next()){
                String title = result.getString("title");
                String author = result.getString("author");
                String isbn = result.getString("isbn");
                int year = result.getInt("year");
                int pages = result.getInt("pages");
                book = new Book(title, author, isbn, pages, year);
            }
        }catch (SQLException e){
            System.out.println(e.getMessage());
        }
        return  book;
    }
}
