package cat.itb.joseZaquinaula7e4.dam.m03.uf6.generalexam.ui;

import cat.itb.joseZaquinaula7e4.dam.m03.uf6.generalexam.data.BookDAO;
import cat.itb.joseZaquinaula7e4.dam.m03.uf6.generalexam.data.DataBase;
import cat.itb.joseZaquinaula7e4.dam.m03.uf6.generalexam.models.Book;

import java.util.Locale;
import java.util.Scanner;

/**
 * Mostra la informació del llibre amb més pàgines
 */
public class LongestBookApp {
    public static void main(String[] args) {
        //
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        //
        DataBase.getInstance().connect();
        //
        System.out.println("## Longest Book");
        Book book = BookDAO.largestPages();
        System.out.println(book);
        //
        DataBase.getInstance().close();
    }
}
