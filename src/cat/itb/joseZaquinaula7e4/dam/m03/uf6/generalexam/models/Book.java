package cat.itb.joseZaquinaula7e4.dam.m03.uf6.generalexam.models;

import cat.itb.joseZaquinaula7e4.dam.m03.uf6.generalexam.ui.Printer;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Book {
    //vars
    private String title;
    private String author;
    private String ISBN;
    private int year;
    private int pages;
    // contructors

    public Book(String title, String author, String ISBN, int year, int pages) {
        this.title = title;
        this.author = author;
        this.ISBN = ISBN;
        this.year = year;
        this.pages = pages;
    }
    //methods
    public static Book readBook(Scanner scanner){
         String title = scanner.nextLine();
         String author = scanner.nextLine();
         String ISBN = scanner.next();
         int year = scanner.nextInt();
         int pages = scanner.nextInt();
         return new Book(title,author, ISBN, year, pages);
    }
    // getterts and setters

    public int getPages() {
        return pages;
    }

    public String getTitle() {
        return title;
    }

    public String getAuthor() {
        return author;
    }

    public String getISBN() {
        return ISBN;
    }

    public int getYear() {
        return year;
    }

    public String toStringPretty(List<Integer> maxCL) {
        List<String> variables = Arrays.asList(title, author, ISBN, Integer.toString(year), Integer.toString(pages));
        return Printer.printColumn(variables,maxCL);

    }
    @Override
    public String toString() {
        List<String> variables = Arrays.asList(title, author, ISBN, Integer.toString(year), Integer.toString(pages));
        return Printer.printColumn(variables);
    }
}
