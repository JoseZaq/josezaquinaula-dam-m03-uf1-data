package cat.itb.joseZaquinaula7e4.dam.m03.uf1.seleccio.exam;

import java.util.Locale;
import java.util.Scanner;

public class CowType {
    //EXAM
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in).useLocale(Locale.US);
        //Estem fent un programa per uns veterianris per gestionar vaques.
        // Hem de classificar els animals segons si són vedell, vaca, toro o bou.
        //
        //Un animal es considera vedell si és menor de 2 anys, en cas contrari és adult.
        //Dels adults, es considera vaca si és femella i toro o bou si és mascle.
        //La diferència entre toro i bou és que el bou està capat i el toro no.
        //
        //Demana a l'usuari l'edat del animal, el sexe (1 = mascle, 2 = femella)
        // i si està capat (1 = no capat, 2 = capat).
        //Imprimeix per pantalla la classe d'animal
        int edad= scanner.nextInt(),sexo= scanner.nextInt(),capado= scanner.nextInt();
        if (edad<2){
            System.out.println("vedell");
        }else if(sexo==2){
            System.out.println("vaca");
        }else if(capado==2){
            System.out.println("bou");
        }else System.out.println("toro");

    }
}
