package cat.itb.joseZaquinaula7e4.dam.m03.uf1.seleccio;

import java.util.Scanner;

public class AbsoluteNumber {
    public static void main(String[] args) {
        //Printa el valor absolut d'un enter entrat per l'usuari.
        Scanner scanner=new Scanner(System.in);
        int num=scanner.nextInt();
        if(num<=0) System.out.println(num*-1);
        else System.out.println(num);


    }
}
