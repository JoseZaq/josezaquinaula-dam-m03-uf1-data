package cat.itb.joseZaquinaula7e4.dam.m03.uf1.seleccio.exam;

import java.util.Locale;
import java.util.Scanner;

public class LaMasovera {
    // EXAM
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in).useLocale(Locale.US);
        //La Masovera, de la cançó la masovera, s'ha fet gran i necessita un programa informàtic que li digui què
        // ha de comprar segons els dia de la setmana.
        //
        //L'usuari ha d'escriure el dia de la setmana en lletres (dilluns, dimarts, etc)
        //Imprimeix per pantalla què ha de comprar.
        //dilluns: Compra llums
        //dimarts: Compra naps
        //dimecres: Compra nespres
        //dijous: Compra nous
        //divendres: Faves tendres
        //dissabte: Tot s'ho gasta
        //diumenge: Tot s'ho menja
        String dia=scanner.next();
        switch(dia){
            case "dilluns":
                System.out.println("Compra llums");
                break;
            case "dimarts":
                System.out.println("Compra naps");
                break;
            case "dimecres":
                System.out.println("Compra nespres");
                break;
            case "dijous":
                System.out.println("Compra nous");
                break;
            case "divendres":
                System.out.println("Faves tendres");
                break;
            case "dissabte":
                System.out.println("Tot s'ho gasta");
                break;
            case "diumenge":
                System.out.println("Tot s'ho menja");
                break;
            default:
                break;

        }
    }
}
