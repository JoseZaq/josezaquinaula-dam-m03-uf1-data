package cat.itb.joseZaquinaula7e4.dam.m03.uf1.seleccio;

import java.util.Scanner;

public class HowManyDaysInMonth {
    public static void main(String[] args) {
        //Demanar un enter a l'usuari que indica el numero de més
        //Retorna el número de dies del mes.
        Scanner scanner=new Scanner(System.in);
        //System.out.println("      DÍAS DEL MES    \nIngrese el numero del més: ");
        int mes=scanner.nextInt();
        switch (mes){
            case 1:
            case 3:
            case 5:
            case 7:
            case 8:
            case 10:
            case 12:
                System.out.println(31);
                break;
            case 2:
                System.out.println(28);
                break;
            case 4:
            case 6:
            case 9:
            case 11:
                System.out.println(30);
                break;
        }
    }
}
