package cat.itb.joseZaquinaula7e4.dam.m03.uf1.seleccio;

import java.util.Locale;
import java.util.Scanner;

public class WhichTriangle {
    //AVANZADAS
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in).useLocale(Locale.US);
        //Llegeix els valors de la llargada de tres segments i escriu si poden formar o no un triangle.
        //Imprimeix No és un triangle, equilàter (tres costats iguals), isòsceles (dos costats iguals)
        // o escalè (tots els costats diferents) segons els valors introduïts
        int a=scanner.nextInt(),b=scanner.nextInt(),c=scanner.nextInt();

        if (a==b || a==c || b==c){
            if (a==b && a==c) System.out.println(" és un triangle equilàter");
            else System.out.println("és un triangle isòsceles ");
        }else System.out.println("és un triangle escalè");
    }
}
