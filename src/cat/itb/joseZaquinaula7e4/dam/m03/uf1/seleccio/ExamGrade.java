package cat.itb.joseZaquinaula7e4.dam.m03.uf1.seleccio;

import java.util.Locale;
import java.util.Scanner;

public class ExamGrade {
    public static void main(String[] args) {
        //L'usuari escriu un valor que representa una nota
        //Imprimeix "Excelent", "Notable", "Bé", "Suficient", "Suspès", "Nota invàlida" segons el la nota numèrica introduïda

        Scanner scanner=new Scanner(System.in).useLocale(Locale.US);
        //System.out.print("Ingresi una nota: ");
        int nota= scanner.nextInt();
        if(!(nota<0 || nota>10)) {
            if (nota >= 9) System.out.print("Excelent");
            else if (nota >= 7) System.out.println("Notable");
            else if (nota >= 6) System.out.println("Bé");
            else if (nota >= 5) System.out.println("Suficient");
            else System.out.println("Suspès");
        }
        else System.out.println("Nota Invàlida");


    }
}
