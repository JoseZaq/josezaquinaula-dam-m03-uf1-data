package cat.itb.joseZaquinaula7e4.dam.m03.uf1.seleccio;

import java.util.Locale;
import java.util.Scanner;

public class InsideCircle {
    //AVANZADOS
    public static void main(String[] args) {
        //Fes un programa que avalui si les coordenades d'un punt (x,y)
            // es troben dins la circumferència descrita pel centre(a,b) i el seu radi r.
        //L'usuari introdueix els 5 valors en l'ordre x, y, a, b, r
        //Imprimeix el punt és dins de la circumferència o el punt és fora
        Scanner scanner=new Scanner(System.in).useLocale(Locale.US);
        //System.out.println("Introdueix els 5 valores en l'ordre x,y,a,b,r");
        int x=scanner.nextInt(),y=scanner.nextInt(),a=scanner.nextInt(),b=scanner.nextInt(),r=scanner.nextInt();
        //a-x es la distancia desde el centro del circulo al punto x
        //b-y es la distancia desde el centro del circulo al punto y
        // si la resta es negativa, es porque y o x es as grande que a y b con respecto al centro
         //y el resultado sigue siendo la distancia desde el punto del centro pero hacia el lado contrario
        int distX= Math.abs(a-x);
        int distY= Math.abs(b-y);
        if (distX>r || distY>r){
            System.out.println("el punt és fora de la circumferència");
        }else{
            System.out.println("el punt és dins de la circumferència");
        }
    }
}
