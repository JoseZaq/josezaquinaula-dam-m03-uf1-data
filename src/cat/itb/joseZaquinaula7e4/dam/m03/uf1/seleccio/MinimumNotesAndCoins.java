package cat.itb.joseZaquinaula7e4.dam.m03.uf1.seleccio;

import java.util.Locale;
import java.util.Scanner;

public class MinimumNotesAndCoins {
    //GENERALIDADES
    public static double CantidadMonedas(double euros,int moneda) {
        String prefijo="cèntims";
        int numMonedas = (int) (euros*100) /moneda;
        if (numMonedas != 0) {
                if (numMonedas == 1) System.out.printf("%d moneda de %d%s\n", numMonedas, moneda,prefijo);
                else System.out.printf("%d monedes de %d%s\n", numMonedas, moneda,prefijo);
        }
        return euros-(double)(numMonedas*moneda)/100;
    }
    public static double CantidadBillete(double euros,int bitllet){
            int numbilletes=(int)euros/bitllet;
            if(bitllet<=2){
                if(numbilletes!=0) {
                    if (numbilletes==1) System.out.printf("%d moneda de %d€\n",numbilletes,bitllet);
                    else System.out.printf("%d monedes de %d€\n",numbilletes,bitllet);
                }
            }else {
                if (numbilletes != 0) {
                    if (numbilletes == 1) System.out.printf("%d bitllet de %d€\n", numbilletes, bitllet);
                    else System.out.printf("%d bitllets de %d€\n", numbilletes, bitllet);
                }
            }
            return euros-numbilletes*bitllet;
    }
    public static void main(String[] args) {
        //L'usuari introdueix una quantitat d'euros.
        //Printa per pantalla el número mínim de cada tipus de bitllets i monedes per tenir aquesta quantitat
        //Exemple si l'usuari entra 603.25
        Scanner scanner=new Scanner(System.in).useLocale(Locale.US);
        double euros=scanner.nextDouble();
        euros=CantidadBillete(euros,500);
        euros=CantidadBillete(euros,200);
        euros=CantidadBillete(euros,100);
        euros=CantidadBillete(euros,50);
        euros=CantidadBillete(euros,20);
        euros=CantidadBillete(euros,10);
        euros=CantidadBillete(euros,5);
        euros=CantidadBillete(euros,2);
        euros=CantidadBillete(euros,1);
        euros=CantidadMonedas(euros,50);
        euros=CantidadMonedas(euros,20);
        euros=CantidadMonedas(euros,10);
        euros=CantidadMonedas(euros,5);
        euros=CantidadMonedas(euros,2);
        CantidadMonedas(euros,1);





    }

}
