package cat.itb.joseZaquinaula7e4.dam.m03.uf1.seleccio;

import java.util.Locale;
import java.util.Scanner;

public class WillWeFightForThecookies {
    public static void main(String[] args) {
        //Introdueix el número de persones i el número de galetes.
        //Si a tothom li toquen el mateix número de galetes imprimeix "Let's Eat!", sinó imprimeix "Let's Fight"
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        //System.out.print("Introdueix el numero de persones: ");
        int per=scanner.nextInt();
        //System.out.print("Introdueix el numero de galetes: ");
        int gall=scanner.nextInt();
        if (per%gall==0) System.out.print("Let's Eat!");
        else System.out.print("Let's Fight");

    }
}
