package cat.itb.joseZaquinaula7e4.dam.m03.uf1.seleccio.exam;

import java.util.Locale;
import java.util.Scanner;

public class RadarDetector {
    //EXAM
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in).useLocale(Locale.US);
        //Farem un programa per un radar de transit que assigna multes segons la velocitat
        // dels cotxes.
        //Si el radar detecta un cotxe a més de 120km/h el cotxe serà multat.
        // Si la velocitat supera la màxima en més de 20 km/h serà una multa greu.
        // Sinó serà una multa lleu.
        //
        //Demana una velocitat a l'usuari.
        //Imprimeix per pantalla el tipus de multa (Correcte, Multa lleu o Multa greu).

        int velocidad=scanner.nextInt();
        if (velocidad>120){
            if (velocidad>140) System.out.println("Multa greu");
            else System.out.println("Multa lleu");
        }else System.out.println("Correcte");
    }
}
