package cat.itb.joseZaquinaula7e4.dam.m03.uf1.seleccio;

import java.util.Locale;
import java.util.Scanner;

public class NiceIsLegalAge {
    public static void main(String[] args) {
        //demani l'edat de l'usuari
        //printa ets major d'edat si és major d'edat.
        Scanner scanner=new Scanner(System.in).useLocale(Locale.US);
        //System.out.printf("Ingresi la seva edat:");
        int edat=scanner.nextInt();
        if (edat >= 18) System.out.println("ets major d'edat");

    }
}
