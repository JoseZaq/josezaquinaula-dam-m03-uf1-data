package cat.itb.joseZaquinaula7e4.dam.m03.uf1.seleccio;

import java.util.Scanner;

public class IdentikitGenerator {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        //Un grup d'investigadors ens ha demanat un programa per generar retrats robots.
        //L'usuari ha d'introduïr el tipus de cabells, ulls, nas i boca i s'imprimeix per pantalla un dibuix de com és el sospitós.
        //Els cabells poden ser arrissats @@@@@, llisos VVVVV o pentinats XXXXX.
        //Els ulls poden ser aclucats .-.-., rodons .o-o. o estrellats _.*-_.".
        //El nas pot ser aixafat ..0.., arromangat ..C.. o aguilenc ..V...
        //La boca pot ser normal .===., bigoti .∼∼∼. o dents-sortides .www..
        //Exemple
        String cabellsDraw,ullsDraw,nasDraw,bocaDraw="";
        //System.out.println("        Retrats robots\nTipo de Cabello (arrisats,llisos o pentinats): ");
        String cabells=scanner.next();
        switch (cabells){
            case "arrisats":
                cabellsDraw="@@@@";
                break;
            case "llisos":
                cabellsDraw="VVVV";
                break;
            case "pentinans":
                cabellsDraw="XXXXX";
                break;
            default:
                cabellsDraw="@@@@";
                break;
        }
        //System.out.println("Tipu de ulls(aclucats,rodons,estrellats): ");
        String ulls=scanner.next();
        switch (ulls){
            case "aclucats":
                ullsDraw=".-.-.";
                break;
            case "rodons":
                ullsDraw=".o-o.";
                break;
            case "estrellats":
                ullsDraw="_.*-_.";
                break;
            default:
                ullsDraw=".-.-.";
                break;
        }
        //System.out.println("Tipu de nas(aixafat,arromangat,aguilenc)");
        String nas=scanner.next();
        //El nas pot ser aixafat ..0.., arromangat ..C.. o aguilenc ..V...
        switch(nas) {
            case "aixafat":
                nasDraw= "..0..";
                break;
            case "arromangat":
                nasDraw = "..C..";
                break;
            case "aguilenc":
                nasDraw= "..V..";
                break;
            default:
                nasDraw= "..0..";
            break;
        }
        //System.out.println("Tipu de boca(normal,bigoti,dents-sortides): ");
        String boca=scanner.next();
        //La boca pot ser normal .===., bigoti .∼∼∼. o dents-sortides .www..
        switch (boca){
            case "normal":
                bocaDraw=".===.";
                break;
            case "bigoti":
                bocaDraw=".~~~.";
                break;
            case "dents-sortides":
                bocaDraw=".www.";
                break;
            default:
                bocaDraw=".===.";
                break;
        }
        System.out.printf("%s\n%s\n%s\n%s",cabellsDraw,ullsDraw,nasDraw,bocaDraw);
    }
}
