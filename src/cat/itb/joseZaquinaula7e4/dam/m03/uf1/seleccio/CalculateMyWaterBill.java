package cat.itb.joseZaquinaula7e4.dam.m03.uf1.seleccio;

import java.util.Locale;
import java.util.Scanner;

public class CalculateMyWaterBill {
    //GENERAL
    public static void main(String[] args) {
        //L'usuari introdueix la lletra del tipus d'habitatge i número de m^3 d'aigua gastats.
        //Printa per pantalla el preu total
        //Pots veure com es calcules les tarífes
        Scanner scanner=new Scanner(System.in).useLocale(Locale.US);
        String casa=scanner.next();
        double aigua= scanner.nextDouble();
        double total=0;
        switch(casa){
            case "A":
                total=(aigua/0.25)*2.46;
                break;
            case "B":
                total=(aigua/0.33)*6.40;
                break;
            case "C":
                total=(aigua/0.40)*7.25;
                break;
            case "D":
                total=(aigua/0.50)*11.21;
                break;
            case "E":
                total=(aigua/0.63)*12.10;
                break;
            case "F":
                total=(aigua/1)*17.28;
                break;
            case "G":
                total=(aigua/1.60)*28.01;
                break;
            case "H":
                total=(aigua/2.5)*40.50;
                break;
            case "I":
                total=(aigua/4.00)*61.31;
                break;
            default:
                break;

        }
        System.out.printf("%.4f€\n",total);
    }
}
