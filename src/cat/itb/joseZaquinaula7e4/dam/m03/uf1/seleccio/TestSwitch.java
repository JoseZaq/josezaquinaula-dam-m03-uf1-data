package cat.itb.joseZaquinaula7e4.dam.m03.uf1.seleccio;

import java.util.Locale;
import java.util.Scanner;

public class TestSwitch {
    public static void main(String[] args) {
        Scanner scanner =new Scanner(System.in).useLocale(Locale.US);
        String diaSemana=scanner.next();

        switch (diaSemana){
            case "Dilluns":
            case "Dimart":
                System.out.println("Entre Semana");
                break;
            default:
                System.out.println("Fin de semana");
                break;

        }

    }
}
