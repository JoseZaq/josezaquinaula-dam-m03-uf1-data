package cat.itb.joseZaquinaula7e4.dam.m03.uf1.seleccio;

import java.util.Locale;
import java.util.Scanner;

public class SecondsDaysTime {
    //EXTRAS
    public static void main(String[] args) {
        //se introduce los segundos que han pasado desde las 00:00
        //se lo muestra en dias:horas:minutos:segundos
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        int days,hours,minuts,leftover_seconds;

        System.out.println("Ingrese el numero de segundos que han pasado desde las 00:00: ");
        int seconds=scanner.nextInt();
        //dias:24horas, horas:60minutos, minutos:60segundos
        //dia:216000sec ,horas:3600s , minutos:60 segundos
        days=seconds/216000;
        leftover_seconds=seconds-(days*216000);
        hours=leftover_seconds/3600;
        leftover_seconds=leftover_seconds-(hours*3600);
        minuts=leftover_seconds/60;
        seconds=leftover_seconds-(minuts*60);
        System.out.printf("Han pasado:\n %ddías %02d:%02d:%02d \n",days,hours,minuts,seconds);

    }
}
