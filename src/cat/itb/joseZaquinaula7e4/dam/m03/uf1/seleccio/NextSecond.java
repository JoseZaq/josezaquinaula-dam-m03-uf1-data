package cat.itb.joseZaquinaula7e4.dam.m03.uf1.seleccio;

import java.util.Locale;
import java.util.Scanner;

public class NextSecond {
    public static void main(String[] args) {
        //'usuari introdueix una hora amb tres enters (hores, minuts i segons).
        //Imprimeix l'hora que serà al cap d'un segon:
        //Entrada
        //12
        //50
        //59
        //Sortida
        //12:51:00
        Scanner scanner=new Scanner(System.in).useLocale(Locale.US);
        int hour= scanner.nextInt();
        int min= scanner.nextInt();
        int sec= scanner.nextInt();

        System.out.printf("Son las %02d:%02d:%02d\n",hour,min,sec);
        sec++;
        if (sec==60){
            min++;
            sec=0;
            if(min==60){
                hour++;
                min=0;
            }
            if(hour==24){
                hour=0;
            }
        }
        System.out.printf("Son las %02d:%02d:%02d\n",hour,min,sec); //%02--> 0: un numero que se quede ahi 2: numero de digitos maximo de la variable(osea hasta 99)
    }
}
