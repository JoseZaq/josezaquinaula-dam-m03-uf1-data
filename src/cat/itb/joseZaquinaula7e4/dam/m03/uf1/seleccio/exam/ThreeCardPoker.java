package cat.itb.joseZaquinaula7e4.dam.m03.uf1.seleccio.exam;

import java.util.Locale;
import java.util.Scanner;

public class ThreeCardPoker {
    //EXAM
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in).useLocale(Locale.US);
        //El Three Card Pocker és un joc de cartes on es poden fer combinacions de 3 cartes.
        //Hi ha les tres combinacions següents:
        //
        //Escala de color: 3 cartes amb números consecutius del mateix pal
        //Trio: 3 cartes del mateix número
        //Escala: 3 cartes amb números consecutius
        //Color: 3 cartes del mateix pal
        //Parella: 2 cartes del mateix número
        //Número alt: cap de les anteriors
        //En la nostra versió del joc, jugarem només amb els números, no amb els pals. Així, les figures possibles seran:
        //
        //Trio
        //Escala
        //Parella
        //Número alt
        //L'usuari introduïrà els números de les 3 cartes (ordenades de menor a major) i li hem de dir quina combinació té.
        int c1= scanner.nextInt(),c2= scanner.nextInt(),c3= scanner.nextInt();
        if (c1==c2 || c1==c3 || c2==c3){
            if(c1==c3 && c1==c2) System.out.println("Trio");
            else System.out.println("Parella");
        }else if (c2==c1+1 && c3==c2+1){
            System.out.println("Escala");
        }else System.out.println("Número alt");
    }
}
