package cat.itb.joseZaquinaula7e4.dam.m03.uf1.seleccio;

import java.util.Locale;
import java.util.Scanner;

public class RockPaperScissors {
    //IF ELSE
    //Volem fer el joc de pedra paper tisora.
    //L'usuari introdueix dos enters (1)pedra, (2) paper, (3) tisora.
    //Imprimeix per pantall Guanya el primer, Guanya el segon, Empat segons els enters introduits.
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in).useLocale(Locale.US);
        int num1= scanner.nextInt(),num2= scanner.nextInt();
        if (num1==num2+1 || (num1==1 && num2==3)){
            System.out.println("Guanya el primer");
        }else System.out.println("Guanya el segon");
        if (num1==num2) System.out.println("Empat");
    }
}
