package cat.itb.joseZaquinaula7e4.dam.m03.uf1.seleccio;

import java.util.Locale;
import java.util.Scanner;

public class WhichBigger {
    //IF ELSE
    //demana dos enters a l'usuari i imprimeix el valor més gran
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        //System.out.print("Ingrese dos numeros:");
        int num = scanner.nextInt();
        //System.out.print("Ingrese otro numero: ");
        int num2 = scanner.nextInt();
        System.out.println(Math.max(num, num2));
    }
}