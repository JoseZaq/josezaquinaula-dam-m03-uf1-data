package cat.itb.joseZaquinaula7e4.dam.m03.uf1.seleccio;

import java.util.Locale;
import java.util.Scanner;

public class NiceIsValidNote {
    //IF ELSE
    //L'usuari escriu un enter
    //imprimeix bitllet vàlid si existeix un bitllet d'euros amb la quantitat entrada
    // , bitllet invàlid en qualsevol altre cas.
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in).useLocale(Locale.US);
        int num= scanner.nextInt();
        if (num==1 || num==2 || num==5 || num==10 || num==20 || num==50 || num==100 || num==200 || num==500){
            System.out.println("bitllet vàlid");
        }else{
            System.out.println("bitllet invàlid");
        }
    }
}
