package cat.itb.joseZaquinaula7e4.dam.m03.uf1.seleccio;

import java.util.Locale;
import java.util.Scanner;

public class WhichPizzaShouldIBuy {
    public static void main(String[] args) {
        //Volem comparar quina pizza és més gran, entre una rectangular i una rodona
        //L'usuai entra el diametre d'una pizza rodona
        //L'usuari entra els dos costats de la pizza rectangular
        //Imprimeix "Compra la rodona" si la pizza rodona és més gran, o "Compta la rectangular" en qualsevol altre cas.
        Scanner scanner=new Scanner(System.in).useLocale(Locale.US);
        //System.out.print("Ingresi el diametre de la pizza rodona: ");
        double redonda= scanner.nextByte();
        redonda= Math.PI*Math.pow((redonda/2),2);
        //System.out.printf("Area de la pizza rodonda: %.2f\n",redonda);
        //System.out.print("Ingresi el costat 1 de la pizza rectangular: ");
        double rec1= scanner.nextByte();
        //System.out.print("Ingresi el costat 2 de la pizza rectangular: ");
        double rec2= scanner.nextByte();
        double rec=rec1*rec2;
        //System.out.printf("El area de la pizza rectangular: %.2f\n",rec);
        if (redonda>rec) System.out.println("\nCompra la rodona");
        else System.out.println("\nCompta la rectangular");

    }
}
