package cat.itb.joseZaquinaula7e4.dam.m03.uf1.seleccio;

import java.util.Locale;
import java.util.Scanner;

public class IsLeapYear {
    public static void main(String[] args) {
        //L'usuari introdueix un any. Indica si és de traspàs printant "2020 és any de traspàs" o "2021 no és any de traspàs".
        Scanner scanner=new Scanner(System.in).useLocale(Locale.US);
        int anyo=scanner.nextInt();
        if (anyo%4==0 && (anyo%100!=0 || anyo%400==0)){
            System.out.printf("%d és any de traspàs\n",anyo);
        }else System.out.printf("%d no és any de traspàs\n",anyo);

    }
}

