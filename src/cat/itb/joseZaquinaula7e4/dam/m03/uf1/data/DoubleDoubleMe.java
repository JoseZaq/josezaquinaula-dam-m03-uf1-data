package cat.itb.joseZaquinaula7e4.dam.m03.uf1.data;

import java.util.Locale;
import java.util.Scanner;

public class DoubleDoubleMe {
    public static void main(String[] args) {
        //L'usuari escriu un valor amb decimals i s'imprimeix el doble.
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        System.out.println("Ingrese un numero con decimales:");
        double input = scanner.nextDouble();
        System.out.println(input * 2);

    }
}
