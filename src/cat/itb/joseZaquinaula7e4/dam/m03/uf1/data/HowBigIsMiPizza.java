package cat.itb.joseZaquinaula7e4.dam.m03.uf1.data;

import java.util.Locale;
import java.util.Scanner;

class HowBigIsMiPizza {
    public static double AreaRodona(double pizza){
        return Math.PI * Math.pow((pizza / 2),2);
    }
    public static void main(String[] args) {
        //L'usuari entra el diametre d'una pizza rodona i s'imprimeix la seva superfície.
        Scanner scanner=new Scanner(System.in);
        System.out.println("Ingrese diametro de la pizza: ");
        double pizza = scanner.nextDouble();
        System.out.printf("Superficie es igual a: %.2f",AreaRodona(pizza));

    }
}
