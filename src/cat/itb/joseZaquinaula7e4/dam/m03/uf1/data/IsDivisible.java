package cat.itb.joseZaquinaula7e4.dam.m03.uf1.data;

import java.util.Scanner;

public class IsDivisible {
    public static void main(String[] args) {
        Scanner scanner= new Scanner(System.in);
        //L'usuari escriu dos valors enters.
        //Imprimeix si el primer és divisible pel segon (si el mòdul de dividir el primer amb el segon és 0).
        System.out.println("Ingrese #1: ");
        int num1= scanner.nextInt();
        System.out.println("Ingrese #2: ");
        int num2= scanner.nextInt();
        System.out.println(num1%num2==0);
    }
}
