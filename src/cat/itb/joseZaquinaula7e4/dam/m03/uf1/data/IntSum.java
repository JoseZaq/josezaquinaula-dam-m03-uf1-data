package cat.itb.joseZaquinaula7e4.dam.m03.uf1.data;

import java.util.Scanner;

public class IntSum {
    public static void main(String[] args) {
        //L'usuari escriu dos enters i s'imprimeix la suma dels dos.
        Scanner scanner=new Scanner(System.in);
        System.out.println("Ingrese un Numero: ");
        int input1=scanner.nextInt();
        System.out.println("Ingrese otro Numero: ");
        int input2=scanner.nextInt();
        System.out.println(input1+input2);
    }
}
