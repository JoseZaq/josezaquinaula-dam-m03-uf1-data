package cat.itb.joseZaquinaula7e4.dam.m03.uf1.data;

import java.util.Scanner;

public class IsGreater {
    public static void main(String[] args) {
        //L'usuari escriu dos valors i s'imprimeix true si el primer és més gran que el segon i false en qualsevol altre cas.
        Scanner scanner=new Scanner(System.in);
        System.out.println("Ingrese #1: ");
        int num1=scanner.nextInt();
        System.out.println("Ingrese #2: ");
        int num2=scanner.nextInt();
        System.out.println(num1>=num2);
    }
}
