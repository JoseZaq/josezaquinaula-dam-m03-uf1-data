package cat.itb.joseZaquinaula7e4.dam.m03.uf1.data;

import java.util.Scanner;

public class IntRectangleArea {
    public static void main(String[] args) {
        //Calcula l'area d'un rectangle (l'usuari introdueix la llargada dels dos costats en valor enter).
        Scanner scanner= new Scanner(System.in);
        System.out.println("Ingrese lado 1: ");
        int input1=scanner.nextInt();
        System.out.println("Ingrese lado 2: ");
        int input2=scanner.nextInt();
        System.out.println(input1*input2);
    }
}
