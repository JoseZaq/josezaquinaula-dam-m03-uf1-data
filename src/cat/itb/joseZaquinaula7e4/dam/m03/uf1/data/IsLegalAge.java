package cat.itb.joseZaquinaula7e4.dam.m03.uf1.data;

import java.util.Scanner;

public class IsLegalAge {
    public static void main(String[] args) {
        //L'usuari escriu un enter amb la seva edat i s'imprimeix true si és major d'edat, i false en qualsevol altre cas.
        Scanner scanner=new Scanner(System.in);
        System.out.println("Ingrese su edad: ");
        int edad=scanner.nextInt();
        System.out.println(edad >= 18);

    }


}
