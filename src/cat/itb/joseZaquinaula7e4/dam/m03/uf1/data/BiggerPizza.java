package cat.itb.joseZaquinaula7e4.dam.m03.uf1.data;

import java.util.Scanner;

public class BiggerPizza {
    public static void main(String[] args) {
        Scanner scanner= new Scanner(System.in);
        //Volem comparar quina pizza és més gran, entre una rectangular i una rodona
        //L'usuai entra el diametre d'una pizza rodona
        //L'usuari entra els dos costats de la pizza rectangular
        //Imprimeix true si la pizza rodona és més gran, o false en qualsevol altre cas.
        System.out.println("Ingrese diametro de la pizza redonda: ");
        double diam=scanner.nextDouble();
        System.out.println("Ingrese lado 1 de la pizza rectangular:");
        double lado1= scanner.nextDouble();
        System.out.println("Ingrese lado 2 de la pizza redonda: ");
        double lado2= scanner.nextDouble();
        System.out.println("Es \"true\" si la pizza redonda es mas grande, sino es \"falso\" ");
        System.out.println((diam/2*3.1416*diam/2)>=(lado1*lado2));

    }
}
