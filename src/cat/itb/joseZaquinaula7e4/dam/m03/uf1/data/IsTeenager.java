package cat.itb.joseZaquinaula7e4.dam.m03.uf1.data;

import java.util.Scanner;

public class IsTeenager {
    public static void main(String[] args) {
        //L'usuari escriu un enter amb la seva edat i s'imprimeix true si l'edat està entre 10 i 20.
        Scanner scanner=new Scanner(System.in);
        System.out.print("Ingrese Nombre:\n");
        String nom= scanner.next();
        System.out.print("Ingrese Edad:\n");
        int edad= scanner.nextInt();

        boolean escaladeedad = edad >= 10 && edad <= 20;
        System.out.printf("\"true\" si se encuentra entre 10 y 20 años %s:\n %s",nom,escaladeedad);
    }
}
