package cat.itb.joseZaquinaula7e4.dam.m03.uf1.data;

import java.util.Scanner;

public class HelloToMe {
    public static void main(String[] args) {
        //'usuari escriu el seu nom i printa per "Bon dia nom introduït"
        Scanner scanner = new Scanner(System.in);
        System.out.print("Ingrese su nombre: ");
        String nom= scanner.next();
        System.out.printf("Bon dia %s",nom);
    }
}
