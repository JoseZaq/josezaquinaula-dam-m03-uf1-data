package cat.itb.joseZaquinaula7e4.dam.m03.uf1.data;

import java.util.Scanner;

public class CrazyOperation {
    public static void main(String[] args) {
        //L'usuari escriu 4 enters i s'imprimeix el valor de suma el primer amb el segon, mutiplicat per la resta del tercer amb quart.
        Scanner scanner=new Scanner(System.in);
        System.out.println("Ingrese #1: ");
        int input1=scanner.nextInt();
        System.out.println("Ingrese #2: ");
        int input2=scanner.nextInt();
        System.out.println("Ingrese #3: ");
        int input3=scanner.nextInt();
        System.out.println("Ingrese #4: ");
        int input4=scanner.nextInt();
        System.out.println((input1+input2)*(input3-input4));
    }
}
