package cat.itb.joseZaquinaula7e4.dam.m03.uf1.data;

import java.util.Locale;
import java.util.Scanner;

public class NiceDivide {
    public static void main(String[] args) {
        //L'usuari introdueix dos enters
        //Printa la divisió (entera) i el mòdul dels dos enters en el següent format
             //11 dividit entre 2 és 5 mòdul 1
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        System.out.print("Ingresi dos enters: ");
        int num1= scanner.nextInt();
        int num2= scanner.nextInt();
        System.out.printf("%d dividit entre %d es %d mòdul %d",num1,num2,num1/num2,num1 % num2);
    }
}
