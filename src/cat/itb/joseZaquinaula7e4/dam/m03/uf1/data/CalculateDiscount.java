package cat.itb.joseZaquinaula7e4.dam.m03.uf1.data;

import java.util.Scanner;

public class CalculateDiscount {
    public static void main(String[] args) {
        //L'usuari entra un preu original i el preu actual i s'imprimeix el descompte (en %).
        Scanner scanner= new Scanner(System.in);
        System.out.println("Ingrese precio original:");
        double input1 = scanner.nextDouble();
        System.out.println("Ingrese precio actual: ");
        double input2 = scanner.nextDouble();
        System.out.println(100-((input2 / input1) * 100));
    }
}
