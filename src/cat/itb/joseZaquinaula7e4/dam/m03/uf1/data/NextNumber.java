package cat.itb.joseZaquinaula7e4.dam.m03.uf1.data;

import java.util.Scanner;

public class NextNumber {
    public static void main(String[] args) {
        //L'usuari escriu un numero enter. Printa per pantalla el número següent amb el format: "Després ve el 5".
        Scanner scanner=new Scanner(System.in);
        System.out.print("Ingrese un numero entero: ");
        int num= scanner.nextInt();
        System.out.printf("Despres ve el %s",num+1);
    }
}
