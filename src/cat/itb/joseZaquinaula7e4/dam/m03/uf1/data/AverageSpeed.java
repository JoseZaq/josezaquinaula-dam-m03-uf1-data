package cat.itb.joseZaquinaula7e4.dam.m03.uf1.data;

import java.util.Locale;
import java.util.Scanner;

public class AverageSpeed {
    public static void main(String[] args) {
        //L'usuari introdueix els quilometres recorreguts i el temps minuts que ha tardat en fer el recorregut
        //Printa per pantalla la velocitat mitjana en km/hora
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        System.out.print("Ingresi els quilometres recorreguts: ");
        double km= scanner.nextInt();
        System.out.print("Ingresi el temps minuts que ha tardat en fer el recorregut: ");
        double t= scanner.nextInt();
        double resp= km/(t/60);
        System.out.printf("La seu velocitat en km/hora es: %.2f",resp);
    }
}
