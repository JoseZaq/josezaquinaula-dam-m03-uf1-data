package cat.itb.joseZaquinaula7e4.dam.m03.uf1.data;

import java.util.Scanner;

public class IsValidNote {
    public static void main(String[] args) {
        //L'usuari escriu un enter i s'imprimeix true si existeix un bitllet d'euros amb la quantitat entrada, false en qualsevol altre cas.
        Scanner scanner=new Scanner(System.in);
        System.out.println("Ingrese un numero: ");
        int num=scanner.nextInt();
        System.out.println(num==1 || num==2 || num==5 || num==10 || num==20 || num==50 || num==100 || num==200 || num==500);

    }
}
