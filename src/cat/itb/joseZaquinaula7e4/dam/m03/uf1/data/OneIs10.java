package cat.itb.joseZaquinaula7e4.dam.m03.uf1.data;

import java.util.Locale;
import java.util.Scanner;

public class OneIs10 {
    public static void main(String[] args) {
        //'usuari introdueix 4 enters
        //Printa true si algun és 10 o false en qualsevol altre cas.
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        System.out.print("Ingresi 4 enters: \n");
        int num1 = scanner.nextInt();
        int num2 = scanner.nextInt();
        int num3 = scanner.nextInt();
        int num4 = scanner.nextInt();
        boolean resp = num1 == 10 || num2== 10 || num3==10 || num4 == 10;
        System.out.printf("hi ha un 10?: %s",resp);
    }
}
