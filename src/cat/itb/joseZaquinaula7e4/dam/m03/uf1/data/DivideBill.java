package cat.itb.joseZaquinaula7e4.dam.m03.uf1.data;

import java.util.Scanner;

public class DivideBill {
    public static void main(String[] args) {
        Scanner scanner= new Scanner(System.in);
        //L'usuari introdueix el número de començals i el preu d'un sopar en cèntims d'euro.
        //Imprimeix quan haurà de pagar cada començal.
        System.out.println("Ingrese el numero de comencales: ");
        double usuarios=scanner.nextDouble();
        System.out.println("Ingrese precio de la cena en centimos de euro: ");
        double precio= scanner.nextDouble();
        System.out.println("El costo individual de la cena es de:");
        System.out.println(precio/usuarios);
    }
}
