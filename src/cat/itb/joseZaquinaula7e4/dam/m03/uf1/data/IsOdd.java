package cat.itb.joseZaquinaula7e4.dam.m03.uf1.data;

import java.util.Locale;
import java.util.Scanner;

public class IsOdd {
    public static void main(String[] args) {
        //L'usuari introdueix un enters
        //Printa la true si és senar, false en qualsevol altre cas.
        Scanner scanner=new Scanner(System.in).useLocale(Locale.US);
        System.out.print("Ingresi un nombre: ");
        int num= scanner.nextInt();
        boolean resp = num % 2 != 0;
        System.out.printf("es impar?: %s",resp);
    }
}
