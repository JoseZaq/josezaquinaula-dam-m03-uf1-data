package cat.itb.joseZaquinaula7e4.dam.m03.uf1.arrays;

import java.util.Arrays;
import java.util.Locale;
import java.util.Scanner;

public class MatrixReader {
    //matrices
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in).useLocale(Locale.US);
        int[][] matrix=ArrayReader.scannerReadIntMatrix(scanner);
        System.out.println(Arrays.deepToString(matrix));
    }
}
