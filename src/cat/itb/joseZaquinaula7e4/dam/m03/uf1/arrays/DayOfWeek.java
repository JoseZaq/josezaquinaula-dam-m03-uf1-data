package cat.itb.joseZaquinaula7e4.dam.m03.uf1.arrays;

import java.util.Locale;
import java.util.Scanner;

public class DayOfWeek {
    //arrays
    public static void main(String[] args) {
        //Donat un enter,
        // printa el dia de la setmana amb text (dilluns, dimarts, dimecres), tenint en compte que dilluns és el 0.
        Scanner scanner=new Scanner(System.in).useLocale(Locale.US);
        String[] diasSemana={"dilluns","dimarts","dimecres","dijous","divendres","dissabte","diumenge"};
        int num=scanner.nextInt();
        System.out.println(diasSemana[num]);

    }
}
