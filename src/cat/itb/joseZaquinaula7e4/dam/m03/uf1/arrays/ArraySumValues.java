package cat.itb.joseZaquinaula7e4.dam.m03.uf1.arrays;

import java.util.Locale;
import java.util.Scanner;

public class ArraySumValues {
    //arrays
    public static void main(String[] args) {
        //L'usuari introduix una llista de valors tal i com s'indica al mètode ArrayReader.
        //Imprimeix per pantalla la suma d'aquests valors.
        Scanner scanner=new Scanner(System.in).useLocale(Locale.US);
        int[] Llist=ArrayReader.scannerReadIntArray(scanner);
        int suma=0;
        for (int j : Llist) {
            suma += j;
        }
        System.out.println(suma);
    }
}
