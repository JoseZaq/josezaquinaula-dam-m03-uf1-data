package cat.itb.joseZaquinaula7e4.dam.m03.uf1.arrays;

import java.util.Locale;
import java.util.Scanner;

public class SimpleBattleshipResult {
    //matrius
    public static void main(String[] args) {
        //Donada la següent configuració del joc Enfonsar la flota,
        // indica si a la posició x, y hi ha aigua o un vaixell (tocat)
        Scanner scanner=new Scanner(System.in).useLocale(Locale.US);
        int[][] campo= {{1, 1, 0, 0, 0, 0, 1},
                {0, 0, 1, 0, 0, 0, 1},
                {0, 0, 0, 0, 0, 0, 1},
                {0, 1, 1, 1, 0, 0, 1},
                {0, 0, 0, 0, 1, 0, 0},
                {0, 0, 0, 0, 1, 0, 0},
                {1, 0, 0, 0, 0, 0, 0}};
        int p1=scanner.nextByte();
        int p2=scanner.nextByte();
        if(campo[p1][p2]==1) System.out.println("tocat");
        else System.out.println("aigua");
    }
}
