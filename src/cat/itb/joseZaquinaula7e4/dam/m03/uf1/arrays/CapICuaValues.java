package cat.itb.joseZaquinaula7e4.dam.m03.uf1.arrays;

import java.util.Arrays;
import java.util.Locale;
import java.util.Scanner;

public class CapICuaValues {
    //arrays
    public static void main(String[] args) {
        //Printa per pantalla cap i cua si la llista de N valors introduits
        // per l'usuari són cap i cua (llegits en ordre invers és la mateixa llista).
        Scanner scanner=new Scanner(System.in).useLocale(Locale.US);
        int[] nums=ArrayReader.scannerReadIntArray(scanner);
        int[] numsInverso=InverseOrder.InvertArrayOrder(scanner,nums);
        if(Arrays.toString(nums).equals(Arrays.toString(numsInverso))){
            System.out.println("cap i cua");
        }


    }
}
