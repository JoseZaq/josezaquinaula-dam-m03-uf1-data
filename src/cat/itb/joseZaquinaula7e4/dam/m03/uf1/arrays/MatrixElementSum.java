package cat.itb.joseZaquinaula7e4.dam.m03.uf1.arrays;

import java.util.Locale;
import java.util.Scanner;

public class MatrixElementSum {
    //matriz
    public static void main(String[] args) {
        //Donada la següent matriu
        //Imrimeix la suma de tots els seus valors.
        //Scanner scanner=new Scanner(System.in).useLocale(Locale.US);
        int[][] matrix = {{2,5,1,6},{23,52,14,36},{23,75,81,64}};
        int suma=0;
        for(int i=0;i<matrix.length;i++){
            for(int j=0;j<matrix[i].length;j++){
                suma+=matrix[i][j];
            }
        }
        System.out.println(suma);

    }
}
