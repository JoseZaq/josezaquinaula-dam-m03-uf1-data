package cat.itb.joseZaquinaula7e4.dam.m03.uf1.arrays.exam;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;

public class PairsAtTheEnd {
    //exam
    public static void main(String[] args) {
        //L'usuari introduirà enters. Afegeix els enters al final de la llista si són parells,
        // i al principi si són senars. Quan entri el -1 s'acaba el programa i s'imprimeix la llista.
        Scanner scanner=new Scanner(System.in).useLocale(Locale.US);
        List<Integer> lista=new ArrayList<Integer>();
        int num=scanner.nextInt();
        while(num!=-1){
            if(num %2==0){
                lista.add(num);
            }else{
                lista.add(0,num);
            }
            num=scanner.nextInt();
        }
        System.out.println(lista);
    }
}
