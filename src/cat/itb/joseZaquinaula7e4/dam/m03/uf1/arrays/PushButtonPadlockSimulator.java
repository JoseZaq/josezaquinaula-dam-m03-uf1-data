package cat.itb.joseZaquinaula7e4.dam.m03.uf1.arrays;

import java.util.Arrays;
import java.util.Locale;
import java.util.Scanner;

public class PushButtonPadlockSimulator {
    //arrays
    public static void main(String[] args) {
        //La nostra versió, taḿbé tindrà 8 botons, però el primer serà el 0. Al inici tots els botons estaran sense apretar.
        //
        //L'usuari introduirà enters indicant quin botó ha d'apretar (o desapretar)
        //Quan introdueixi el -1, és que ja ha acabat i hem d'imprimir l'estat del candau
        Scanner scanner=new Scanner(System.in).useLocale(Locale.US);
        boolean[] botones= new boolean[8];
        int i;
        while(true){
            i=scanner.nextInt();
            if(i==-1) break;
            botones[i]= !botones[i];
        }
        System.out.println(Arrays.toString(botones));
    }
}
