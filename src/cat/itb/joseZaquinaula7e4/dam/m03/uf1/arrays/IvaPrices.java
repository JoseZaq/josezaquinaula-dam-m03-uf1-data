package cat.itb.joseZaquinaula7e4.dam.m03.uf1.arrays;

import java.util.Locale;
import java.util.Scanner;

public class IvaPrices {
    //arrays
    public static void main(String[] args) {
        //En una botiga volem convertir tot de preus sense a IVA al preu amb IVA.
        // Per afegir l'IVA a un preu hem de sumar-hi el 21% del seu valor.
        //L'usuari introduirà el preu de 10 artícles.
        // Imprimeix per pantalla el preu amb l'IVA afegit amb el següent format
        Scanner scanner=new Scanner(System.in).useLocale(Locale.US);
        float[] preus=new float[10];
        for(int i=0;i<preus.length;i++){
            preus[i]=scanner.nextInt();
        }

        for (float v : preus) {
            System.out.printf("%.1f IVA = %.2f\n", v, v + (v * 0.21));
        }
    }
}
