package cat.itb.joseZaquinaula7e4.dam.m03.uf1.arrays;


import java.util.Locale;
import java.util.Scanner;

public class CovidGrowRate {
    //arrays
    public static void main(String[] args) {
        //El departament de salut ens ha demanat que calculem la taxa de infecció que està tenint
        // la Covid en la nostre regió sanitaria. Donat un nombre de casos $casos1$ en una setmana,
        // si la següent tenim un nombre de casos $casos2$, podem calcular la taxa d'infecció amb la formula
        //$$ infecció = \frac{casos2}{casos1} $$
        //L'usuari introduirà un llistat de casos detectats cada setmana (usant el format del ArrayReader).
        // Imprimeix la taxa d'infecció detectada cada setmana.
        Scanner scanner=new Scanner(System.in).useLocale(Locale.US);
        int maxVal=scanner.nextInt();
        double infeccio;
        double[] casosSetmana=new double[maxVal];
        for(int i=0;i<casosSetmana.length;i++){
            casosSetmana[i]=scanner.nextInt();
        }

        for(int i=0;i<casosSetmana.length;i++){
            if(i!=0){
                infeccio=casosSetmana[i]/casosSetmana[i-1];
                System.out.print(infeccio+" ");
            }
        }
        System.out.println();
    }
}
