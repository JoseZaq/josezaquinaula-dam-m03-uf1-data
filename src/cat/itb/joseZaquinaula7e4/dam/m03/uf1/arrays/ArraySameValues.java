package cat.itb.joseZaquinaula7e4.dam.m03.uf1.arrays;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Locale;
import java.util.Scanner;

public class ArraySameValues {
    //arrays
    public static void main(String[] args) {
        //L'usuari introduirà 2 llistes de valors, com s'indica al mètode ArrayReader.
        //Printa per pantalla són iguals si ha introduit la mateixa llista, o no són iguals si són diferents
        Scanner scanner=new Scanner(System.in).useLocale(Locale.US);
        int[] a1=ArrayReader.scannerReadIntArray(scanner);
        int[] a2=ArrayReader.scannerReadIntArray(scanner);
        if(Arrays.toString(a1)==Arrays.toString(a2)){
            System.out.println("són iguals");
        }
    }
}
