package cat.itb.joseZaquinaula7e4.dam.m03.uf1.arrays;

import java.util.Locale;
import java.util.Scanner;

public class ArraySortedValues {
    //arrays
    public static void main(String[] args) {
        //Printa per pantalla ordenats si la llista de N valors introduits per l'usuari estan ordenats.
        //L'usuari primer entrarà el número d'enters a introudir i després els diferents enters.
        Scanner scanner=new Scanner(System.in).useLocale(Locale.US);
        int maxVal=scanner.nextInt();
        int[] nums=new int[maxVal];
        boolean orden=true;
        for(int i=0;i<maxVal;i++){
            nums[i]=scanner.nextInt();
            if(i!=0){
                if(nums[i-1]>nums[i]){
                    orden=false;
                }
            }
        }
        if(orden) System.out.println("ordenats");
        else System.out.println("desordenats");
    }
}
