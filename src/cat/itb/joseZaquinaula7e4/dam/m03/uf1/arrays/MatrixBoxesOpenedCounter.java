package cat.itb.joseZaquinaula7e4.dam.m03.uf1.arrays;

import java.util.Arrays;
import java.util.Locale;
import java.util.Scanner;

public class MatrixBoxesOpenedCounter {
    //matrix
    public static void main(String[] args) {
        //Un banc té tot de caixes de seguretat en una graella, enumerades per
        // fila i columna del 0 al 3.
        //Volem registar quan els usuaris obren una caixa de seguretat,
        // i al final del dia, fer-ne un recompte.
        //
        //L'usuari introduirà parells d'entrers del 0 al 3 quan s'obri la caixa indicada.
        //Quan introduiexi l'enter -1, és que s'ha acabat el dia.
        // Printa per pantalla el nombre de cops
        // que s'ha obert
        Scanner scanner=new Scanner(System.in).useLocale(Locale.US);
        int[][] caixes=new int[4][4];
        int num;
        while(true){
            num=scanner.nextInt();
            if (num==-1) break;
            int fila=num;
            num=scanner.nextInt();
            if (num==-1) break;
            int columna=num;
            caixes[fila][columna]++;
        }
        System.out.println(Arrays.deepToString(caixes));


    }
}
