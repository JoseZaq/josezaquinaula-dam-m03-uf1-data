package cat.itb.joseZaquinaula7e4.dam.m03.uf1.arrays;

import java.util.Arrays;
import java.util.Locale;
import java.util.Scanner;

public class BicicleDistance {
    //arrays
    public static void main(String[] args) {
        //Dada una velocidad de una bicicleta en metros por segundo,
        // indica los metro que habrá recorrido cuando haya pasado 1,2,3,4,5,6,7,8,9 y 10 segundos.
        Scanner scanner=new Scanner(System.in).useLocale(Locale.US);
        float velocitat=scanner.nextFloat();
        float[] metrosRecorridos=new float[10];
        for(int i=0;i<10;i++){
            metrosRecorridos[i]=velocitat*(i+1);
        }
        System.out.println(Arrays.toString(metrosRecorridos));
    }
}
