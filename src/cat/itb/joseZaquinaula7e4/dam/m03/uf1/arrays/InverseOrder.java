package cat.itb.joseZaquinaula7e4.dam.m03.uf1.arrays;

import java.util.Arrays;
import java.util.Locale;
import java.util.Scanner;

public class InverseOrder {
    //arrays
    public static int[] InvertArrayOrder(Scanner scanner,int[] nums){
        int[] numsInverso=new int[nums.length];
        for(int i=0;i<nums.length;i++){
            numsInverso[nums.length-1-i]=nums[i];
        }
        return numsInverso;
    }
    public static void main(String[] args) {
        //L'usuari entra 10 enters. Imprimeix-los en l'odre invers al que els ha entrat.
        Scanner scanner=new Scanner(System.in).useLocale(Locale.US);
        int[] nums=new int[10];
        int[] numsInverso=new int[nums.length];
        for(int i=0;i<nums.length;i++){
            nums[i]=scanner.nextInt();
            numsInverso[nums.length-1-i]=nums[i];
        }
        System.out.println(Arrays.toString(numsInverso));
    }
}
