package cat.itb.joseZaquinaula7e4.dam.m03.uf1.arrays;

public class HighestMountainOnMap {
    //matrix
    public static void main(String[] args) {
        //Usant les imatges d'un satel·lit hem pogut fer raster o (mapa de bits)
        // [https://ca.wikipedia.org/wiki/Mapa_de_bits]
        // que ens indica l'alçada d'un punt concret d'un mapa. Hem obtingut la següent informació
        double[][] map ={{1.5,1.6,1.8,1.7,1.6},{1.5,2.6,2.8,2.7,1.6},
                {1.5,4.6,4.4,4.9,1.6},{2.5,1.6,3.8,7.7,3.6},{1.5,2.6,3.8,2.7,1.6}};
        //Printa per pantalla l'alçada del punt més alt i en quines cordenades es troba
        // usant el següent format:
        //Substituint x i y, per les cordenades i 1.5 metres per l'alçada.

        double[] higher=getHigherPoint(map);
        System.out.printf("%d, %d: %.1f metres",(int)higher[0],(int)higher[1],higher[2]);

    }

    public static double[] getHigherPoint(double[][] map) {
        double[] higher={0,0,map[0][0]};
        for (int i = 1; i < map.length; i++) {
            for (int j = 1; j < map.length; j++) {
                if(map[i][j]>map[i-1][j-1]){
                    higher[0]=i;
                    higher[1]=j;
                    higher[2]=map[i][j];
                }
            }
        }
        return higher;
    }
}
