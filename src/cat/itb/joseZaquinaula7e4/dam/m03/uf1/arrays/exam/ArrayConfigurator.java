package cat.itb.joseZaquinaula7e4.dam.m03.uf1.arrays.exam;

import java.util.Arrays;
import java.util.Locale;
import java.util.Scanner;

public class ArrayConfigurator {
    //exam
    public static void main(String[] args) {
        //Crea un array de 10 enters inicialitzats amb el valor 0.
        //L'usuari introduirà parelles de valors. La primera indicarà la posició de l'array
        // a modificar i la segona el valor a posar-hi.
        //Quan introdueixi la posició -1 és que s'ha d'acabar el programa i printar l'array en l'estat que ha quedat.
        Scanner scanner=new Scanner(System.in).useLocale(Locale.US);
        int[] array=new int[10];
        int num=0,index=0;
        while(num!=-1){
            array[index]=num;
            num=scanner.nextInt();
            if(num!=-1){
                index=num;
                num=scanner.nextInt();
            }
        }
        System.out.println(Arrays.toString(array));
    }
}
