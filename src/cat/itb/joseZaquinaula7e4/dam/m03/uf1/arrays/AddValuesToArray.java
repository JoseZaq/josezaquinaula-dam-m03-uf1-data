package cat.itb.joseZaquinaula7e4.dam.m03.uf1.arrays;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Locale;
import java.util.Scanner;

public class AddValuesToArray {
    //arrays
    public static void main(String[] args) {
        //Inicialitza un array de floats de tamany 50, amb el valor 0.0f a tots els elements.
        //Després asigna els els valors següents a les posicions indicades:
        //
        //primera: 31.0f
        //segona: 56.0f
        //vintena: 12.0f
        //última: 79.0f
        Scanner scanner=new Scanner(System.in).useLocale(Locale.US);
        float[] flotante=new float[50];
        flotante[0]=31.0f;
        flotante[1]=56.0f;
        flotante[19]=12.0f;
        flotante[flotante.length-1]=79.0f;
        System.out.println(Arrays.toString(flotante));


    }
}
