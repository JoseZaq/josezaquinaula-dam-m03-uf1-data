package cat.itb.joseZaquinaula7e4.dam.m03.uf1.arrays;

public class IsThereAMultipleOf7 {
    //arrays
    public static void main(String[] args) {
        //Donat el següent vector, imprimeix true si algún dels números és divisible entre 7 o false sinó.
        int[] values = {4,8,9,40,54,84,40,6,84,1,1,68,84,68,4,840,684,25,40,98,
                54,687,31,4894,468,46,84687,894,40,846,1681,618,161,846,84687,6,848};
        boolean resultado=false;
        int i=0;
        while (i<values.length) {
            if (values[i] % 7 == 0) {
                resultado = true;
                break;
            }
            i++;
        }
        System.out.println(resultado);
    }
}
