package cat.itb.joseZaquinaula7e4.dam.m03.uf1.arrays;

import java.util.Locale;
import java.util.Scanner;

import static cat.itb.joseZaquinaula7e4.dam.m03.uf1.arrays.HighestMountainOnMap.getHigherPoint;

public class HighestMountainScaleChange {
    //matriz
    public static void main(String[] args) {
        //El govern britànic ens ha demanat que també vol accedir a les dades de
        // l'exercici anterior i que les necessitaria en peus i no metres.
        //Per convertir un metre a peus pots has de multiplicar els metres per 3.2808.
        //Fes la conversió i imprimeix la matriu per pantalla.
        Scanner scanner=new Scanner(System.in).useLocale(Locale.US);
        double[][] map ={{1.5,1.6,1.8,1.7,1.6},{1.5,2.6,2.8,2.7,1.6},
                {1.5,4.6,4.4,4.9,1.6},{2.5,1.6,3.8,7.7,3.6},{1.5,2.6,3.8,2.7,1.6}};
        double[] higher=getHigherPoint(map);
        System.out.printf("%d, %d: %.2f",(int)higher[0],(int)higher[1],higher[2]*3.2808);


    }
}
