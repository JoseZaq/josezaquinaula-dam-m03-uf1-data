package cat.itb.joseZaquinaula7e4.dam.m03.uf1.arrays;

import java.util.Locale;
import java.util.Scanner;

public class ArrayMaxValue {
    //arrays
    public static void main(String[] args) {
        //L'usuari introduirà 1 array d'enters, com s'indica al mètode ArrayReader.
        //Un cop llegits tots, printa per pantalla el valor més gran.
        Scanner scanner=new Scanner(System.in).useLocale(Locale.US);
        int[] num=ArrayReader.scannerReadIntArray(scanner);
        int gran=0;
        for(int i=0;i< num.length;i++){
            if(i!=0){
            if(num[i-1]<num[i]){
                gran=num[i];
            }
            }else gran=num[i];
        }
        System.out.println(gran);
    }
}
