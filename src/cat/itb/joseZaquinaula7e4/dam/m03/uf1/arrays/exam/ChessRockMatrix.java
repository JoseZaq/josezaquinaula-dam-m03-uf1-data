package cat.itb.joseZaquinaula7e4.dam.m03.uf1.arrays.exam;
import java.util.Arrays;
import java.util.Locale;
import java.util.Scanner;

public class ChessRockMatrix {
    //exam
    public static void main(String[] args) {
        //Un tauler d'escacs és una graella de 8x8 caselles.
        // Una torre es pot moure en línia recta de forma vertical o horitzontal.
        //Donada una posició d'una torre en un tauler d'escacs,
        // fes una matriu de booleans indicant a quins punts es pot moure la torre en el següent torn.
        Scanner scanner=new Scanner(System.in).useLocale(Locale.US);
        boolean[][] tauler=new boolean[8][8];
        int x=scanner.nextInt();
        int y=scanner.nextInt();
        for (int i = 0; i < tauler.length; i++) {
            for (int j = 0; j < tauler.length; j++){
                if(j==y || i==x){
                    if(!(j==y && i==x)) tauler[i][j]=true;
                }
            }
        }
        System.out.println(Arrays.deepToString(tauler));
    }
}
