package cat.itb.joseZaquinaula7e4.dam.m03.uf1.arrays.exam;

public class HowManyMountains {
    //exam
    public static void main(String[] args) {
        //Usant les imatges d'un satel·lit hem pogut fer raster
        // o (mapa de bits)
        // [https://ca.wikipedia.org/wiki/Mapa_de_bits]
        // que ens indica l'alçada d'un punt concret d'un mapa.
        //Imprimeix per pantalla el numero posicions cims que hi ha.
        // Entenrem que un cim ho és si l'alçada de la seva posició és més alta o igual a
        // les posicions col·lidants (no conten les diagonals).
        //No contarem com a cim les posicions que estan als marges del mapa.
        double[][] map ={{1.5,1.6,1.8,1.7,1.6},{1.5,2.6,2.8,2.7,1.6},{1.5,4.6,4.4,4.9,1.6},{2.5,1.6,3.8,7.7,3.6},{1.5,2.6,3.8,2.7,1.6}};
        for (int i = 1; i < map.length-1; i++) {
            for (int j = 1; j < map.length-1; j++) {
                if(map[i][j]>=map[i][j-1] && map[i][j]>=map[i][j+1] &&
                map[i][j]>=map[i-1][j] && map[i][j]>=map[i+1][j]){
                    System.out.printf("%d %d: %.2f\n",i,j,map[i][j]);
                }
            }
        }


    }
}
