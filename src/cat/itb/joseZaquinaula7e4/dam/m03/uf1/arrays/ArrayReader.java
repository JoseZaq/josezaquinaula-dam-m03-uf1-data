package cat.itb.joseZaquinaula7e4.dam.m03.uf1.arrays;

import java.util.Scanner;

public class ArrayReader {
    //arrays
    /**
     * Reads an int of arrays from user input.
     * The user first introduces the number (N).
     * Later it introduces the integers one by one.
     * @return int array of values introduced of size N
     */
    public static int[] scannerReadIntArray(Scanner scanner){
        int maxVal=scanner.nextInt();
        int[] nums=new int[maxVal];
        for(int i=0;i<nums.length;i++){
            nums[i]=scanner.nextInt();
        }
        return nums;
    }

        //Crea una classe ArrayReader i defineix la funció següent,
        // que llegeix un array d'enter de l'usuari.
        // L'usuari primer entrarà el número d'enters a introudir i després els diferents enters.
        //Nota: a l'exercici ArraySortedValues ja has fet aquesta funció.


    /**
     * Reads an int matrix from user input.
     * The user first introduces the size of the matrix(NxM).
     * Then, introduces the integers one by one.
     * @return int[][] matrix of values introduced of size NxM
     */
    public static int[][] scannerReadIntMatrix(Scanner scanner){
        int maxfila=scanner.nextByte(),maxcolumna=scanner.nextByte();
        int[][] matrix=new int[maxfila][maxcolumna];
        for(int i=0;i<maxfila;i++){
            for(int j=0;j<maxcolumna;j++){
                matrix[i][j]=scanner.nextInt();
            }
        }
        return matrix;
    }
}
