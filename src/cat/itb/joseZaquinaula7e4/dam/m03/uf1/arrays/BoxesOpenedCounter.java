package cat.itb.joseZaquinaula7e4.dam.m03.uf1.arrays;

import java.util.Arrays;
import java.util.Locale;
import java.util.Scanner;

public class BoxesOpenedCounter {
    //arrays
    public static void main(String[] args) {
        //Un banc té tot de caixes de seguretat, enumerades del 0 al 10.
        //Volem registar quan els usuaris obren una caixa de seguretat, i al final del dia, fer-ne un recompte.
        //
        //L'usuari introduirà enters del 0 al 10 quan s'obri la caixa indicada.
        //Quan introduiexi l'enter -1, és que s'ha acabat el dia. Printa per pantalla el nombre de cops que s'ha obert.
        Scanner scanner=new Scanner(System.in).useLocale(Locale.US);
        int[] botones= new int[8];
        int i;
        while(true){
            i=scanner.nextInt();
            if(i==-1) break;
            botones[i]++;
        }
        System.out.println(Arrays.toString(botones));
    }
}
