package cat.itb.joseZaquinaula7e4.dam.m03.uf1.arrays;

import java.util.Locale;
import java.util.Scanner;

public class MatrixSimetric {
    //matrices
    public static void main(String[] args) {
        //Donada una matriu quadrada donada per l'usuari, el programa imprimeix
        // true si la matriu és simètrica, false en cas contrari.
        Scanner scanner=new Scanner(System.in).useLocale(Locale.US);
        int[][] matriz=ArrayReader.scannerReadIntMatrix(scanner);
    }
}
