package cat.itb.joseZaquinaula7e4.dam.m03.uf1.arrays;

public class MatrixIsThereADiv13 {
    //arrays
    public static void main(String[] args) {
        //Donada la següent matriu
        int[][] matrix = {{2,5,1,6},{23,52,14,36},{23,75,81,62}};
        //Imprimeix true si algún dels números és divisible entre 13, false altrement.
        boolean resposta=false;
        for (int[] ints : matrix) {
            for (int j = 0; j < matrix.length; j++) {
                if (ints[j] % 13 == 0) {
                    resposta = true;
                    break;
                }
            }
        }
        System.out.println(resposta);
    }
}
