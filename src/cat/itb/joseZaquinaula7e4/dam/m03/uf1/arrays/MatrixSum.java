package cat.itb.joseZaquinaula7e4.dam.m03.uf1.arrays;

import java.util.Arrays;
import java.util.Locale;
import java.util.Scanner;

public class MatrixSum {
    //matrices
    public static void main(String[] args) {
        //Implementa un programa que demani dos matrius a l'usuari i imprimeixi la suma de les dues.
        Scanner scanner=new Scanner(System.in).useLocale(Locale.US);
        int[][] matriz1=ArrayReader.scannerReadIntMatrix(scanner);
        int[][] matriz2=ArrayReader.scannerReadIntMatrix(scanner);
        int[][] suma=new int[matriz1.length][matriz1.length];
        System.out.println(matriz1.length);
        for(int i=0;i<matriz1.length;i++){
            for(int j=0;j<matriz1.length;j++){
                suma[i][j]=matriz1[i][j]+matriz2[i][j];
            }
        }
        System.out.println(Arrays.deepToString(suma));
    }
}
