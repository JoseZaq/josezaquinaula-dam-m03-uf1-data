package cat.itb.joseZaquinaula7e4.dam.m03.uf1.arrays;

import java.util.Locale;
import java.util.Scanner;

public class MinOf10Values {
    //arrays
    public static void main(String[] args) {
        //L'usuari entra 10 enters. Crea un array amb aquest valors.
        //Imprimeix per pantalla el valor més petit introduit.
        Scanner scanner=new Scanner(System.in).useLocale(Locale.US);
        int[] num=new int[10];
        int petit=0;
        for(int i=0;i<=9;i++){
            num[i]=scanner.nextInt();
            if(i>0){
            if(num[i-1]<num[i]){
                petit=num[i];
            }
            }else petit=num[i];
        }
        System.out.println(petit);
    }
}
