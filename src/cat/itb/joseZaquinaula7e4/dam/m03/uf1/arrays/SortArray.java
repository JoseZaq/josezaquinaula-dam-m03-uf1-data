package cat.itb.joseZaquinaula7e4.dam.m03.uf1.arrays;

import java.util.Arrays;
import java.util.Locale;
import java.util.Scanner;

public class SortArray {
    //arrays
    public static void main(String[] args) {
        //Implementa un programa que demani l'array a l'usuari i ordene els nombres de menor a major.
        Scanner scanner=new Scanner(System.in).useLocale(Locale.US);
        int[] nums=ArrayReader.scannerReadIntArray(scanner);
        int desordenado=3;
        while(desordenado!=1){
            desordenado=0;
            for (int i = 1; i < nums.length; i++) {
                if (nums[i - 1] > nums[i]) {
                    desordenado=1;
                    int temporal = nums[i];
                    nums[i] = nums[i - 1];
                    nums[i - 1] = temporal;
                }
            }
        }
        System.out.println(Arrays.toString(nums));
    }
}
