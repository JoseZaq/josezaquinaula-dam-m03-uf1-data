package cat.itb.joseZaquinaula7e4.dam.m03.uf1.arrays.exam;

import cat.itb.joseZaquinaula7e4.dam.m03.uf1.arrays.ArrayReader;

import java.util.Locale;
import java.util.Scanner;

public class SumPositiveValues {
    //exam
    public static void main(String[] args) {
        //L'usuari introduirà 1 array d'enters, com s'indica al mètode ArrayReader.
        //Un cop llegits tots, printa per pantalla la suma de tots els valors positius.
        Scanner scanner=new Scanner(System.in).useLocale(Locale.US);
        int[] array= ArrayReader.scannerReadIntArray(scanner);
        int suma=0;
        for (int j : array) {
            if (j > 0) {
                suma += j;
            }
        }
        System.out.println(suma);
    }
}
