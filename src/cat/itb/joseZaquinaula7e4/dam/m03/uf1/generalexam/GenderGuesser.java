package cat.itb.joseZaquinaula7e4.dam.m03.uf1.generalexam;

import java.util.Locale;
import java.util.Scanner;

public class GenderGuesser {
    //generalExam
    public static void main(String[] args) {
        //Treballem en una empresa que és dedica al software d'analisis de text. Ens han encomanat la tasca de fer un petit programa que donada una paraula ens indiqui si és masculina, femenina o plural. L'algoritme és complicat però el simplificarem de la següent manera.
        // Tota paraula que acabi en a serà consideradad feminina,
        // mentre que si acaba en s serà plural. La resta masculines.
        //L'usuari introduirà paraules per la consola
        //Imprimeix masculí, femení, plural segons la seva classificació
        //Quan l'usuari introdueixi la paraula END acaba el programa
        Scanner scanner=new Scanner(System.in).useLocale(Locale.US);
        String palabra=scanner.next();
        while(!palabra.equals("END")){
            if(palabra.endsWith("a")){
                System.out.println("femení");
            }else if(palabra.endsWith("s")){
                System.out.println("plural");
            }else System.out.println("masculí");
            palabra=scanner.next();
        }
    }
}
