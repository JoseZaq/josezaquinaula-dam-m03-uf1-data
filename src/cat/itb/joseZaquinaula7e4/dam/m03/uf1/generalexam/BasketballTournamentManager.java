package cat.itb.joseZaquinaula7e4.dam.m03.uf1.generalexam;

import java.util.Locale;
import java.util.Scanner;

public class BasketballTournamentManager {
    //generalExam
    public static void main(String[] args) {
        //Una associació que organitza tornejos de basquet ens ha trucat per a que li montem un petit programa per a un campionat. El campionat consta de 8 equips que van fent partits entre ells. Volem que l'usuari pugui introduir l'equip que ha guanyat un partit i el programa ens avisi de quantes vitories porta, i al final, de qui és el guanyador.
        //
        //L'usuari introduirà un enter entre 1 i 8 que identifica l'equip que ha guanyat un partit.
        //Cada cop que l'usuari introdueixi una victòria informarem del total de victories de l'equip
        //El programa s'acaba quan l'usuari introdueixi un -1
        //El programa informarà de l'equip guanyador
        Scanner scanner=new Scanner(System.in).useLocale(Locale.US);
        int ganador=1;
        int[] equipo=new int[8];
        int equipoG= scanner.nextInt();
        while(equipoG!=-1){
            equipo[equipoG-1]++;
            System.out.printf("L'equip %d té %d victòries\n",equipoG,equipo[equipoG-1]);
            equipoG=scanner.nextInt();
        }
        for (int i=1;i < equipo.length;i++) {
            if(equipo[i] > equipo[i-1]) ganador=i+1;
        }
        System.out.printf("L'equip guanyador és el %d\n",ganador);
    }
}
