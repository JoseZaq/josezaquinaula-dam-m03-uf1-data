package cat.itb.joseZaquinaula7e4.dam.m03.uf1.generalexam;

import java.util.Locale;
import java.util.Scanner;

public class HeartRateWarning {
    //generalExam
    public static void main(String[] args) {
        //L'empresa de rellotges esportius RunWithMe ens ha demanat que fem un petit mòdul per els seus rellotges intel·ligents per el control del rítme cardíac. Volem tenir controlat quan un esportista té el ritme cardíac massa alt o massa baix durant un període de temps elevat de forma consecutiva.
        //L'usuari introduirà el ritme cardíac mínim i màxim desitjat.
        //Introudirà els diferents ritmes cardíacs observats, finalitzats per un -1.
        //Quan es llegeixin més de tres vegades seguides ritmes superiors al màxim introduït imprimeix: "MASSA ALT"
        //Quan es llegeixin més de tres vegades seguides ritmes inferiors al mínim introduït imprimeix: "MASSA BAIX"
        Scanner scanner=new Scanner(System.in).useLocale(Locale.US);
        int min=scanner.nextInt();
        int max=scanner.nextInt();
        int[] contador={0,0};
        int cardio=scanner.nextInt();
        while(cardio!=-1){
            if(cardio < min ){
                contador[0]++;
                contador[1]=0;
            }
            else if(cardio > max) {
                contador[1]++;
                contador[0]=0;
            }
            else{
                contador[0]=0;
                contador[1]=0;
            }
            if(contador[0] >= 3){
                System.out.println("MASSA BAIX");
            }
            if(contador[1] >= 3) {
                System.out.println("MASSA ALT");
            }
            cardio=scanner.nextInt();
        }
    }
}

