package cat.itb.joseZaquinaula7e4.dam.m03.uf1.iterative;

import java.util.Locale;
import java.util.Scanner;

public class DotLine {
    //FOR
    public static void main(String[] args) {
        //Demana un enter a l'usuari.
        //Imprimeix per pantalla tants punts com l'usuari hagi indicat
        Scanner scanner=new Scanner(System.in).useLocale(Locale.US);
        int num=scanner.nextInt();
        for (int i=1;i<=num;i++){
            System.out.print(".");
        }
        System.out.println();
    }
}
