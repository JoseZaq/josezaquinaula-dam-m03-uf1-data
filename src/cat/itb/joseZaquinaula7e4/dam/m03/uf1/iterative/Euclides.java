package cat.itb.joseZaquinaula7e4.dam.m03.uf1.iterative;

import java.util.Locale;
import java.util.Scanner;

public class Euclides {
    //avanzadas
    public static void main(String[] args) {
        //Implementa el algoritmo de Euclides , que calcula le máximo común divisor de dos números positivos.
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        int a=scanner.nextInt(),b=scanner.nextInt(),i=1,res=0;
        if(a<b){
            i=b;b=a;a=i;
        }
        while(i!=0) {
            res =i;
            i=a%b;
            a=b;
            b=i;
        }
        System.out.println(res);
    }
}
