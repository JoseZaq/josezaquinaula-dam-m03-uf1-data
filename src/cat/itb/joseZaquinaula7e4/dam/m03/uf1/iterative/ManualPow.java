package cat.itb.joseZaquinaula7e4.dam.m03.uf1.iterative;

import java.util.Locale;
import java.util.Scanner;

public class ManualPow {
    //general
    public static void main(String[] args) {
        //El usuario introduce dos enteros, la base y el exponente.
        // Imprimir el resultado de la potencia (sin usar la librería math).
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        int base=scanner.nextInt(),exponente=scanner.nextInt();
        int resultado=base;
        for(int i=0;i<exponente;i++){
            resultado*=base;
        }
        System.out.println(resultado);
    }
}
