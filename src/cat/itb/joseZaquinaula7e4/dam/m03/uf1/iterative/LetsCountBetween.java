package cat.itb.joseZaquinaula7e4.dam.m03.uf1.iterative;

import java.util.Locale;
import java.util.Scanner;

public class LetsCountBetween {
    //FOR
    public static void main(String[] args) {
        //L'usuari introdueix dos valors enters.
        //Printa per pantalla tots els valors que hi ha entre els dos valors
        // introduits ordenats de menor a major
        //input: 5 10
        //output: 6789
        Scanner scanner=new Scanner(System.in).useLocale(Locale.US);
        int num1=scanner.nextInt(),num2=scanner.nextInt();
        for(int i=num1+1;i<num2;i++){
            System.out.print(i);
        }
        System.out.println();
    }
}
