package cat.itb.joseZaquinaula7e4.dam.m03.uf1.iterative.practice;

import java.util.Locale;
import java.util.Scanner;

public class MonthName {
    public static void main(String[] args) {
        //El usuario introduce un entero correspondiente a un mes del año 1 <= N <= 12.
        //Si el usuario no introduce un número válido, vuelve a pedir el número al usuario.
        //
        //El programa muestra el nombre del más introducido.
        Scanner scanner=new Scanner(System.in).useLocale(Locale.US);
        int mes=0,indicadorError=0;
        do{
            if(indicadorError>0){
                System.out.printf("Error. El mes %d no és vàlid.\n",mes);
            }
            indicadorError=1;
            mes=scanner.nextInt();
        }while(!(mes>=1 && mes<=12));
        switch (mes){
            case 1:
                System.out.println("Gener");
                break;
            case 2:
                System.out.println("Febrer");
                break;
            case 3:
                System.out.println("Març");
                break;
            case 4:
                System.out.println("Abril");
                break;
            case 5:
                System.out.println("Maig");
                break;
            case 6:
                System.out.println("Juny");
                break;
            case 7:
                System.out.println("Juliol");
                break;
            case 8:
                System.out.println("Agost");
                break;
            case 9:
                System.out.println("Setembre");
                break;
            case 10:
                System.out.println("Octubre");
                break;
            case 11:
                System.out.println("Novembre");
                break;
            case 12:
                System.out.println("Desembre");
                break;
        }


    }
}
