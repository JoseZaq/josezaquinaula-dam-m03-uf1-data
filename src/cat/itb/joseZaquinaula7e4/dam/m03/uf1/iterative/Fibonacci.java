package cat.itb.joseZaquinaula7e4.dam.m03.uf1.iterative;

import java.util.Locale;
import java.util.Scanner;

public class Fibonacci {
    //avanzados
    public static void main(String[] args) {
        //Dado un número entero positivo, escribe los números de Fibonacci (Leonardo de Pisa)
        // inferiores o iguales a él.
        //Los números de Fibonacci se definen de la siguiente manera: El primero es 0,
        // el segundo es 1, el siguiente es la suma de los dos anteriores y así sucesivamente.
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        int a=0;
        int b=1;
        int num=scanner.nextInt();
        System.out.print(a+" "+b+" ");
        for(int i=2;i<=num;i=a+b){
            a=b;
            b=i;
            System.out.print(i+" ");
        }
        System.out.println();
    }
}
