package cat.itb.joseZaquinaula7e4.dam.m03.uf1.iterative;

import java.util.Locale;
import java.util.Scanner;

public class WaitFor5 {
    //WHILE
    public static void main(String[] args) {
        //Farem un programa que preguna a l'usuari un enter fins a que entri el numero 5.
        //
        //L'usuari introdueix enters.
        //Quan introdueixi el 5 imprimeix 5 trobat!.
        Scanner scanner=new Scanner(System.in).useLocale(Locale.US);
        int num=0;
        while(num!=5){
            num=scanner.nextInt();
        }
        System.out.println("5 trobat!");
    }
}
