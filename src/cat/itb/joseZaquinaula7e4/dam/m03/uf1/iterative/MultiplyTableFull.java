package cat.itb.joseZaquinaula7e4.dam.m03.uf1.iterative;

public class MultiplyTableFull {
    //GENERAL
    public static void main(String[] args) {
        //Imprimeix les taules de multiplicar en forma de taula
        for(int i=1;i<=9;i++){
            for(int j=1;j<=9;j++){
                System.out.printf("%2d ",i*j);
            }
            System.out.println();
        }
    }
}
