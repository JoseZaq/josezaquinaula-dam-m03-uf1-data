package cat.itb.joseZaquinaula7e4.dam.m03.uf1.iterative;

import java.util.Locale;
import java.util.Scanner;

public class IsPrime {
    //avanzados
    public static void main(String[] args) {
        //Implementa un programa que muestre true si el número introducido es primero,
        // false en otro caso.
        //Clarificación: Los números 0 y 1 no son primeros.
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        int num=scanner.nextInt();
        boolean resp=true;
            for(int i=2;i<=3;i++){
                if(num%i==0 || num==1){
                    resp=false;
                }
                if(num==i){
                    resp=true;
                }
            }
        System.out.println(resp);
    }
}
