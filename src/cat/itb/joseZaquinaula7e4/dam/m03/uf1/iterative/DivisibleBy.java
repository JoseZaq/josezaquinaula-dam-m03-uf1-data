package cat.itb.joseZaquinaula7e4.dam.m03.uf1.iterative;

import java.util.Locale;
import java.util.Scanner;

public class DivisibleBy {
    //avanzados
    public static void main(String[] args) {
        //Imprimir todos los números enteros divisibles por 3 que hay entre A y B (inclusivo).
        //El usuario introduce dos números A y B
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        int a=scanner.nextInt(),b=scanner.nextInt();
        for(int i=a;i<b;i++){
            if(i%3==0) System.out.println(i);
        }
    }
}
