package cat.itb.joseZaquinaula7e4.dam.m03.uf1.iterative;

import java.util.Locale;
import java.util.Scanner;

public class NumberBetweenOneAndFive {
    //DoWhile
    public static void main(String[] args) {
        //Demanar a l'usuari un enter entre 1 i 5.
        //Si introdueix un número més gran o més petit, torna-li a demanar l'enter.
        //Imprimeix per pantalla: El número introduït: 3, substituint el 3 pel número.
        Scanner scanner=new Scanner(System.in).useLocale(Locale.US);
        int num;
        do {
            num=scanner.nextInt();
        }while(!(num>=1 && num<=5));
        System.out.println("El número introduït: "+num);
    }
}
