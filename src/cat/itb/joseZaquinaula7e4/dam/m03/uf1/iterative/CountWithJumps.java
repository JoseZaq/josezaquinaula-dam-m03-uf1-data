package cat.itb.joseZaquinaula7e4.dam.m03.uf1.iterative;

import java.util.Locale;
import java.util.Scanner;

public class CountWithJumps {
    //generals
    public static void main(String[] args) {
        //El usuario introduce dos valores enteros, el final y el salto
        //Escribe todos los números desde el 1 hasta el final, con una distancia de salto
        Scanner scanner=new Scanner(System.in).useLocale(Locale.US);
        int elfinal=scanner.nextInt(),elsalto=scanner.nextInt();
        for(int i=1;i<=elfinal;i+=elsalto){
            if(i>1) System.out.print(" ");
            System.out.print(i);
        }
        System.out.println();
    }
}
