package cat.itb.joseZaquinaula7e4.dam.m03.uf1.iterative;

import java.util.Locale;
import java.util.Scanner;

public class OnlyVowels {
    //general
    public static void main(String[] args) {
        //Dada una lista de letras, imprime únicamente las vocales que haya.
        //La entrada consta de dos partes:
        //Primero se indica la cantidad de letras
        //A continuación vienen las letras separadas por espacios en blanco
        //Imprimir todas las que son vocales.
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        int cantidadLetras=scanner.nextInt();
        String letra="",vocales="";
        for(int i=0;i<=cantidadLetras;i++){
            letra=scanner.next();
            if(letra.equals("a")||letra.equals("e")||letra.equals("i")||letra.equals("o")||letra.equals("u")){
                vocales=vocales+"\n"+letra;
            }
        }
        System.out.println(vocales);
    }
}
