package cat.itb.joseZaquinaula7e4.dam.m03.uf1.iterative;

import java.util.Locale;
import java.util.Scanner;

public class factorial {
    //avanzados
    public static void main(String[] args) {
        //Calcula el factorial de un valor
        Scanner scanner=new Scanner(System.in).useLocale(Locale.US);
        int factorial_resultado=1;
        int num=scanner.nextInt();
        for(int i=1;i<=num;i++){
            factorial_resultado*=i;
        }
        System.out.println(factorial_resultado);
    }
}
