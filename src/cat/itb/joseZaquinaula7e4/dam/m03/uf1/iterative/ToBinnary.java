package cat.itb.joseZaquinaula7e4.dam.m03.uf1.iterative;

import java.util.Locale;
import java.util.Scanner;

public class ToBinnary {
    //avanzados
    public static void main(String[] args) {
        //Escribe un programa que lea un número natural
        // más pequeño que 256 y escriba su representación en binario.
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        int num=scanner.nextInt();
        int dividido;
        String binarioString="";
        for(int i=num;i>=1;i=dividido){
            dividido=num/2;
            binarioString+=Integer.toString(num%2);
            num=dividido;
            System.out.println(dividido);

        }
        String binarioStringInvertido="";
        for(int i=binarioString.length()-1;i>=0;i--){
            binarioStringInvertido+=" "+binarioString.charAt(i);
        }
        System.out.println(binarioStringInvertido);
    }
}
