package cat.itb.joseZaquinaula7e4.dam.m03.uf1.iterative.practice;

import java.util.Locale;
import java.util.Scanner;

public class IsPowerOf2 {

    public static int potencia(int base,int potencia){
        int resultado=1;
        for(int i=1;i<=potencia;i++){
            resultado*=base;
        }
        return resultado;
    }
    public static void main(String[] args) {
        //El usuario introduce un entero. El programa muestra true sí
        // es una potencia de 2, false en otro caso.
        Scanner scanner=new Scanner(System.in).useLocale(Locale.US);
        String numString=scanner.next();
        int maxNum=potencia(10,numString.length());
        int num=Integer.parseInt(numString);
        int resultadoPotencia=1;
        boolean numPotencia2=false;
            while(resultadoPotencia<maxNum){
                if (num == resultadoPotencia) {
                    numPotencia2 = true;
                    break;
                }
                resultadoPotencia*=2;

            }

        System.out.println(numPotencia2);


    }

}
