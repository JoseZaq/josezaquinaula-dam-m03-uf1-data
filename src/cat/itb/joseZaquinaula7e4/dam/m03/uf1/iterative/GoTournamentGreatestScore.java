package cat.itb.joseZaquinaula7e4.dam.m03.uf1.iterative;

import java.util.Locale;
import java.util.Scanner;

public class GoTournamentGreatestScore {
    //GENERAL
    public static void main(String[] args) {
        //Nos han pedido que hagamos un programa para un torneo de Go .
        // Necesitamos hacer un programa que dada una lista de puntuaciones,
        // nos diga quién es el ganador del torneo.
        //El usuario introducirá el nombre del participante y la puntuación, cada uno en una línea.
        //Cuando ya no haya más participantes entrara la palabra clave END .
        //Imprimir por pantalla el ganador del concurso y los puntos obtenidos.
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        String cadena="",nom="",ganador="";
        int puntaje=0;
        int i=0;
        while (!cadena.equals("END")) {
            i++;
            cadena = scanner.nextLine();
            if (i==2){
                if(Integer.parseInt(cadena)>puntaje){
                    ganador=nom+": "+String.valueOf(cadena);
                }
                puntaje=Integer.parseInt(cadena);
                i=0;
            }else{
                nom=cadena;
            }
        }
        System.out.println(ganador);
    }
}
