package cat.itb.joseZaquinaula7e4.dam.m03.uf1.iterative;

import java.util.Locale;
import java.util.Scanner;

public class PiramidCenter {
    //avanzados
    public static void main(String[] args) {
        //S'imprimeix una piràmide d'altura N de # centrada
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        int num=scanner.nextInt();
        for(int i=1;i<=num;i++){
            for(int p=num;p>i;p--){
                System.out.print(" ");
            }
            for(int j=1;j<=i;j++) {
                System.out.print(" " + "#");
            }
            System.out.println();
        }
    }
}
