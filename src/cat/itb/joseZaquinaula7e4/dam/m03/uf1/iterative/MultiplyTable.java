package cat.itb.joseZaquinaula7e4.dam.m03.uf1.iterative;

import java.util.Locale;
import java.util.Scanner;

public class MultiplyTable {
    //WHILE
    public static void main(String[] args) {
        //Volem printar les taules de múltiplicar.
        //
        //L'usuari introdueix un enter
        //Printa per pantalla la taula de multiplicar del número introduit:
        Scanner scanner=new Scanner(System.in).useLocale(Locale.US);
        int i=1;
        int num=scanner.nextInt();
        while(i<=9){
            System.out.printf("%d * %d = %d\n",i,num,i*num);
            i++;
        }
    }
}
