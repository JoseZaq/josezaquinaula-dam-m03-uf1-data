package cat.itb.joseZaquinaula7e4.dam.m03.uf1.iterative;

import java.util.Locale;
import java.util.Scanner;

public class LetsCount {
    //
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in).useLocale(Locale.US);
        int n=scanner.nextInt();
        int i=1;
        while(i<=n){
            System.out.printf("%d ",i);
            i++;
        }
        //ssalto de linea
        System.out.println();
    }
}
