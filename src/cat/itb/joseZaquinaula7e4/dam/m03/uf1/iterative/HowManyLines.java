package cat.itb.joseZaquinaula7e4.dam.m03.uf1.iterative;

import java.util.Locale;
import java.util.Scanner;

public class HowManyLines {
    //WHILE
    public static void main(String[] args) {
        //Volem contar quantes línies té un text introduït per l'usuari.
        //
        //L'usuari introduiex un text per pantalla que pot tenir salts de línia.
        //Per indicar que ha acabat d'introduïr el text escriurà una línia amb la paraula clau END
        //Imprimeix el nombre de línies que ha introduït l'usuari (sense contar la END)
        //Recorda que per comparar dos Strings no pots usar == i has d'usar line.equals("END");
        Scanner scanner=new Scanner(System.in).useLocale(Locale.US);
        int contador=0;
        String line="";
        while(!line.equals("END")){
            line=scanner.nextLine();
            contador++;
        }
        System.out.println(contador-1);

    }
}
