package cat.itb.joseZaquinaula7e4.dam.m03.uf1.iterative;

import java.util.Locale;
import java.util.Scanner;

public class SumMyNumbers {
    //generals
    public static void main(String[] args) {
        //Hacer un programa que lea un número entero positivo n y escriba
        // la suma de sus cifras. Por ejemplo las cifras del número 456 suman (4 + 5 + 6) = 15
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        String n=scanner.nextLine();
        int num=Integer.parseInt(n);
        int resultado=0;
        int cantidad=n.length();
        if((cantidad%3)!=0){
            int ceros=3-cantidad%3;
            for(int i=1;i<=ceros;i++){
                n+='0';
                cantidad+=1;
            }

        }
        for(int i=0;i<cantidad/3;i++) {
            String numString="";
            for(int p =3*i; p<3+i*3; p++){
                 numString+=n.charAt(p);
            }
            num=Integer.parseInt(numString);

            for (int j = 100; j > 10; j /= 10) {
                int centena = num / 100;
                int decena = (num / 10) % 10;
                int unidad = num % 10;
                resultado += decena + centena + unidad;
            }
        }
        System.out.println(resultado);
    }
}
//INCOMPLETO!!!!