package cat.itb.joseZaquinaula7e4.dam.m03.uf1.iterative;

import java.util.Locale;
import java.util.Scanner;

public class CountDown {
    //FOR
    public static void main(String[] args) {
        //L'usuari introdueix un enter (N) i es mostra per pantalla un compte enrere de N fins a 1.
        //
        //input: 5
        //output: 54321
        Scanner scanner=new Scanner(System.in).useLocale(Locale.US);
        int N=scanner.nextInt();
        for(int i=N;i>=1;i--){
            System.out.print(i);
        }
        System.out.println();
    }
}
