package cat.itb.joseZaquinaula7e4.dam.m03.uf1.iterative.practice;

import java.util.Locale;
import java.util.Scanner;

public class PowersOf2 {
    public static void main(String[] args) {
        //El usuario introduce un entero. El programa mostrará todas las potencias
        // de 2 hasta este exponente.
        Scanner scanner=new Scanner(System.in).useLocale(Locale.US);
        int num=scanner.nextInt();
        int PotResultado=0;
        for(int i=0;i<=num;i++){
            PotResultado*=2;
            if(i==0) PotResultado++;
            System.out.printf("2^%d = %d\n",i,PotResultado);
        }


    }
}
