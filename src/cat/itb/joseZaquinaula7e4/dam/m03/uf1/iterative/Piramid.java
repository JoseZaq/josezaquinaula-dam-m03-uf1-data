package cat.itb.joseZaquinaula7e4.dam.m03.uf1.iterative;

import java.util.Locale;
import java.util.Scanner;

public class Piramid {
    //general
    public static void main(String[] args) {
        //S'imprimeix una piràmide d'altura N de #
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        int n=scanner.nextInt();
        for(int i=1;i<n+1;i++){
            for(int j=1;j<=i;j++){
            System.out.print("#");
            }
            System.out.println();
        }
    }
}
