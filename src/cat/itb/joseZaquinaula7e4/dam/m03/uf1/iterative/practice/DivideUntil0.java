package cat.itb.joseZaquinaula7e4.dam.m03.uf1.iterative.practice;

import java.util.Locale;
import java.util.Scanner;

public class DivideUntil0 {
    public static void main(String[] args) {
        //El usuario introduce un entero. El programa realizará las siguientes
        // operaciones hasta que el número sea 0:
        //
        //Si es par: Divide entre 2.
        //Si es impar: Resto 1.
        //Imprime el número de divisiones y restas
        Scanner scanner=new Scanner(System.in).useLocale(Locale.US);
        int num=scanner.nextInt();
        int divisiones=0,restas=0;
        while (num>0){
            if(num%2==0){
                divisiones++;
                num=num/2;
            }else{
                restas++;
                num-=1;
            }
        }
        System.out.printf("Divisions: %d\nRestes: %d\n",divisiones,restas);
    }
}
