package cat.itb.joseZaquinaula7e4.dam.m03.uf1.staticfunctions;

import java.util.Locale;
import java.util.Scanner;

public class IsTeenagerFunc {
    //funcions
    public static boolean youngVerification(int edad){
        return edad >= 10 && edad <= 20;
    }
    public static void main(String[] args) {
        //L'usuari escriu un enter amb la seva edat i s'imprimeix true si l'edat està entre 10 i 20.
        Scanner scanner=new Scanner(System.in).useLocale(Locale.US);
        //System.out.print("Ingrese Nombre:\n");
        String nom= scanner.next();
        //System.out.print("Ingrese Edad:\n");
        int edad= scanner.nextInt();
        System.out.printf("%s",youngVerification(edad));
    }
}
