package cat.itb.joseZaquinaula7e4.dam.m03.uf1.staticfunctions;

import java.util.Locale;
import java.util.Scanner;

public class PowerOf {
    //MATH
    public static void main(String[] args) {
        //demana dos enters (a i b) a l'usuari i imprimeix el valor de $a^b$
        Scanner scanner=new Scanner(System.in).useLocale(Locale.US);
        int a=scanner.nextInt(),b=scanner.nextInt();
        System.out.println(Math.pow(a,b));
    }
}
