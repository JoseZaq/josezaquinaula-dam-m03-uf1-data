package cat.itb.joseZaquinaula7e4.dam.m03.uf1.staticfunctions;

import java.util.Scanner;

public class IntRectangleAreaFunc {
    //Funcions Modifica els exercicis fets per tal que usin funcions per calcular les dades.
    //Pots reutilitzar una funció feta en un exercici per cridar-la desde un altre.

    public static double rectangleArea(double lenght,double width){
        return lenght*width;
    }

    public static void main(String[] args) {
        //Calcula l'area d'un rectangle (l'usuari introdueix la llargada dels dos costats en valor enter).
        Scanner scanner= new Scanner(System.in);
        //System.out.println("Ingrese lado 1: ");
        int input1=scanner.nextInt();
        //System.out.println("Ingrese lado 2: ");
        int input2=scanner.nextInt();
        System.out.printf("%.2f",rectangleArea(input1,input2));
    }

}
