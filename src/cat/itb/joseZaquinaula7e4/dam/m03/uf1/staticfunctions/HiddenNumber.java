package cat.itb.joseZaquinaula7e4.dam.m03.uf1.staticfunctions;

import java.util.Locale;
import java.util.Scanner;

public class HiddenNumber {
    //MATH
    public static void main(String[] args) {
        //L'oridador pensa un número del 1 al 3.
        //L'usuari introdueix un enter.
        //Imprimeix L'has encertat, o No l'has encertat segons el resultat.
        Scanner scanner=new Scanner(System.in).useLocale(Locale.US);
        int rand=(int)(1+3*Math.random());
        int num = scanner.nextInt();
        System.out.println(rand);
        if (num==rand) System.out.println("L'has encertat");
        else  System.out.println("No l'has encertat");


    }
}
