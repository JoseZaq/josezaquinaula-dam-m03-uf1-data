package cat.itb.joseZaquinaula7e4.dam.m03.uf1.staticfunctions;

import java.util.Locale;
import java.util.Scanner;

public class FirstVarLetter {
    //Char
    public static void main(String[] args) {
        //L'usuari escriu la primer caràcter de l'identificador d'una variable.
        //Indica si és un caràcter vàlid, segons les convensions de Java.
        // Imprimeix una opció correcte si pots.
        //Per llegir un char usa
        Scanner scanner=new Scanner(System.in).useLocale(Locale.US);
        char caracter=scanner.next().charAt(0);
        if(caracter<=90 && caracter>=65){
            char charCorrecta= (char) (caracter+32);
            System.out.println("Caràcter incorrecte.\n"+charCorrecta);
        }

    }
}
