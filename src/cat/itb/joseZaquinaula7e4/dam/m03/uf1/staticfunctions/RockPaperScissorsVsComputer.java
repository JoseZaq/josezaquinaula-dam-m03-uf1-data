package cat.itb.joseZaquinaula7e4.dam.m03.uf1.staticfunctions;

import java.util.Scanner;

public class RockPaperScissorsVsComputer {
    //Volem fer el joc de pedra paper tisora per jugar contra l'ordinador.
    //L'usuari introdueix un enter (1)pedra, (2) paper, (3) tisora.
    //L'ordinador decideix aleatoriament la seva tirada.
    //Imprimeix per pantalla que tira l'orinador (L'ordinador a tirat pedra).
    //Imprimeix per pantalla Has guanyat, Guanya l'ordinador, O empat segons el resultat.
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        int desicion=scanner.nextInt();
        int desicionComputadora= (int) (1+(3*Math.random()));
        switch (desicionComputadora){
            case 1:
                System.out.println("L'ordinador a tirat pedra");
                break;
            case 2:
                System.out.println("L'ordinador a tirat paper");
                break;
            case 3:
                System.out.println("L'ordinador a tirat tisora");
                break;
        }
        if (desicion==desicionComputadora+1 || (desicion==1 && desicionComputadora==3)){
            System.out.println("Has guanyat");
        }else System.out.println("Guanya l'ordinador");
        if (desicion==desicionComputadora) System.out.println("empat");

    }


}
