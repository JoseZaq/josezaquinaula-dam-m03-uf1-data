package cat.itb.joseZaquinaula7e4.dam.m03.uf1;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class UsefulMetodes {
    //string methodes
    public static String justLowercasesLetters(String string) {
        int a=0;
        while(a!=2) {
            a = 0;
            int capitalX = capitalletterDetector(string);
            int lowerX = pointDetector(string);
            if (capitalX != -1) {
                string = string.replace(string.charAt(capitalX), (char) (string.charAt(capitalX)+ 32));
            } else a++;
            if (lowerX != -1) {
                string=string.substring(0,lowerX)+string.substring(lowerX+1);
            } else a++;
        }
        return string;
    }

    public static int pointDetector(String string) {
        //retorna la posicion del punto
        //ascii: punto=46
        for(int i=0;i<string.length();i++){
            if(string.charAt(i)==46 || string.charAt(i) == 44 || string.charAt(i) == 63 || string.charAt(i) == 33 || string.charAt(i) == 39
                    || string.charAt(i) == 45 || string.charAt(i)== 32){
                return i;
            }
        }
        return -1;
    }

    public static int capitalletterDetector(String string) {
        //retorna la posicion de la mayuscula
        //Ascii: 65 a 90 mayusculas
        for(int i=0;i<string.length();i++){
            if(string.charAt(i)>=65 && string.charAt(i)<=90){
                return i;
            }
        }
        return -1;
    }
    //array list
    public static void PrintArrayList(List<Integer> lista) {
        for(Integer listas:lista){
            System.out.print(listas+" ");
        }
        System.out.println();
    }

    public static ArrayList<Integer> getList(Scanner scanner) {
        int input=scanner.nextInt();
        ArrayList<Integer> list=new ArrayList<Integer>();
        while(input!=-1){
            list.add(input);
            input=scanner.nextInt();
        }
        return list;
    }
}
