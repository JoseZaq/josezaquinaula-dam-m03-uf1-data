package cat.itb.joseZaquinaula7e4.dam.m03.uf1.strings;

import java.util.Locale;
import java.util.Scanner;

public class DivideBetweenPeople {
    //string
    public static void main(String[] args) {
        //Donat el preu d'un producte, volem coneixer quan val si el dividim entre
        // un número n de persones que va entre el 0 i el 10.
        // L'usuari introduirà un preu. Imprimeix els diferents preus amb dos decimals input:
        Scanner scanner=new Scanner(System.in).useLocale(Locale.US);
        float divisor=scanner.nextInt();
        String division;
        for (int i = 1; i <= 10; i++) {
            division=String.format("Dividit entre %d persones: %.2f€",i,divisor/i);
            System.out.println(division);
        }

    }
}
