package cat.itb.joseZaquinaula7e4.dam.m03.uf1.strings;

import java.util.Locale;
import java.util.Scanner;

public class IdFromEmail {
    //strings
    public static void main(String[] args) {
        //Donat un correu electrònic printa per pantalla l'identificador de l'usuari dins del domini:
        //input: mar.om@example.com
        //output: mar.om
        Scanner scanner=new Scanner(System.in).useLocale(Locale.US);
        String idUsuari="";
        String correo=scanner.nextLine();
        for(int i=0;i<correo.length();i++){
            if(correo.charAt(i) == '@'){
                break;
            }
            idUsuari+=correo.charAt(i);
        }
        System.out.println(idUsuari);
    }
}
