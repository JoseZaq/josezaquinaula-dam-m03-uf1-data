package cat.itb.joseZaquinaula7e4.dam.m03.uf1.strings.practice;

import java.util.Locale;
import java.util.Scanner;

public class CristmasGenerator {
    //strings practice
    public static void main(String[] args) {
        //Som dia 31 de desembre de 2020 i no has enviat cap felicitació de Nadal! Els reis et portaran carbó!
        //
        //Per sort, ets informàtic i tens un pla. Cerques al teu correu i troves una felicitació
        // que et van enviar l'any passat. Estàs salvat de l'infern!
        // Ara només et falta un petit programa que et fagi la feina bruta.
        //
        //Fes un programa que, donada una felicitació en modifiqui el contigut:
        //
        //La data.
        //Nom felicitat.
        //Nom escriptor.
        //L'entrada serà primer el contingut actual i després el desitjat dels següents valors:
        //
        //any
        //felicitador
        //escriptor Finalement s'introduirà el text de la carta, amb una linia END quan s'acabi
        Scanner scanner=new Scanner(System.in).useLocale(Locale.US);
        String[][] wordsToReplace=new String[3][2];
        for (int i = 0; i < wordsToReplace.length; i++) {
            for (int j = 0; j < wordsToReplace[i].length; j++) {
                wordsToReplace[i][j] =scanner.next();
            }
        }
        String carta="";
        String newLine=scanner.nextLine();
        while(!newLine.contains("END")) {
            carta+="\n"+newLine;
            newLine=scanner.nextLine();
        }
        //replace the words to replace
        carta=carta.replace(wordsToReplace[0][0],wordsToReplace[0][1]);
        carta=carta.replace(wordsToReplace[1][0],wordsToReplace[1][1]);
        carta=carta.replace(wordsToReplace[2][0],wordsToReplace[2][1]);
        System.out.println(carta);
    }
}
