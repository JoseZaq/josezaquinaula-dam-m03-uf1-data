package cat.itb.joseZaquinaula7e4.dam.m03.uf1.strings;

import java.util.Locale;
import java.util.Scanner;

public class MoneyDivPrinter {
    //STRING
    public static void main(String[] args) {
        //L'usuari introdueix una quantitat de euros i un numero de persones a repartir. Imprimeix quans euros que li toca a cada u amb el format següent:
        //input: 10 3
        //output: Si tenim 10.00€ i 3 persones, toquen a 3.33# per persona
        Scanner scanner=new Scanner(System.in).useLocale(Locale.US);
        double euros= scanner.nextDouble();
        int persones=scanner.nextInt();

        double eurosPerPersona=euros/persones;
        String sortida = String.format("Si tenim %.2f i %d persones, toquen a %.2f per persona",euros,persones,eurosPerPersona);
        System.out.println(sortida);

    }
}
