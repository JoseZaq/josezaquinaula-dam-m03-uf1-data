package cat.itb.joseZaquinaula7e4.dam.m03.uf1.strings.practice;

import cat.itb.joseZaquinaula7e4.dam.m03.uf1.UsefulMetodes;

import java.util.Locale;
import java.util.Scanner;

public class RepositoryName {
    //practica de strings
    public static void main(String[] args) {
        //Implementar un programa que donat el número del mòdul, el número de la unitat formativa
        // i el correu electrònic de l'ITB d'un alumne, ens calcule quin serà el nom del repositori de Git.
        //
        //El número del mòdul ha de tindre 2 xifres sempre, omplint amb 0's a l'esquerre si escau.
        Scanner scanner=new Scanner(System.in).useLocale(Locale.US);
        int modulo=scanner.nextInt();
        int uf=scanner.nextInt();
        String correo=scanner.next();
        //saca el nom y cognom del correo + el 7e4
        String alumno=UsefulMetodes.justLowercasesLetters(correo);
        int index=0;
        for (int i = 0; i < alumno.length(); i++) {
            if(correo.charAt(i) == '@') {
                index = i - 2;

            }
        }
        alumno=alumno.substring(0,index);
        String out=String.format("%s-dam-m%02d-uf%d",alumno,modulo,uf);
        System.out.println(out);
    }
}
