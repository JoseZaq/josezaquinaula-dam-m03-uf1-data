package cat.itb.joseZaquinaula7e4.dam.m03.uf1.strings;

import java.util.Locale;
import java.util.Scanner;

public class OneYesOneNo {
    //strings
    public static void main(String[] args) {
        //L'usuari introduirà una paraula. Imprimeix només les lletres en posició senar:
        //input: paracaigudista
        //output: prciuit
        Scanner scanner=new Scanner(System.in).useLocale(Locale.US);
        String paraula=scanner.next();
        String novaParaula="";
        for(int i=0;i<paraula.length();i++){
            if(i %2 == 0)
            novaParaula+=paraula.charAt(i);
        }
        System.out.println(novaParaula);
    }
}
