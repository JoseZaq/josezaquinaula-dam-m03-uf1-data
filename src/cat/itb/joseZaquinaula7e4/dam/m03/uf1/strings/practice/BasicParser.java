package cat.itb.joseZaquinaula7e4.dam.m03.uf1.strings.practice;

import java.util.Locale;
import java.util.Scanner;

public class BasicParser {
    //string practice
    public static void main(String[] args) {
        //Implementar un programa que llegeixi línies de l'entrada en format Markdown i les analitze sintàcticament (parser). Aquest parser té els següents formats:
        //
        //** (\u001B[1m): Inici negreta
        //-- (\u001B[3m): Inici cursiva
        //++ (\u001B[0m): Final format (esborra tots els formats aplicats).
        //A més, suporta colors mitjançant el format //color//, on color és el color que vols posar:
        //
        //r (\u001B[31m): Vermell
        //g (\u001B[32m): Verd
        //b (\u001B[34m): Blau
        //s (\u001B[39m): Reset (color per defecte)
        Scanner scanner=new Scanner(System.in).useLocale(Locale.US);
        String frase=scanner.nextLine();
        while(true){
            if(frase.contains("++")) frase=frase.replace("++","\\u001B[0m");
            else if(frase.contains("--")) frase=frase.replace("--","\\u001B[3m");
            else if(frase.contains("**")) frase=frase.replace("**","\\u001B[1m");
            else if(frase.contains("//r//")) frase=frase.replace("//r//","\\u001B[31m");
            else if(frase.contains("//g//")) frase=frase.replace("//g//","\\u001B[32m");
            else if(frase.contains("//b//")) frase=frase.replace("//b//","\\u001B[34m");
            else if(frase.contains("//s//")) frase=frase.replace("//s//","\\u001B[39m");
            else break;
        }
        System.out.println(frase);
    }
}
