package cat.itb.joseZaquinaula7e4.dam.m03.uf1.strings;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Locale;
import java.util.Scanner;

public class UnaMoscaGenerator {
    //strings
    public static void main(String[] args) {
        //La canço tradicional de la mosca és la següent:
        //
        //una mosca volava per la llum.
        //i la llum es va apagar.
        //i la pobra mosca
        //es va quedar a les fosques
        //i la pobra mosca no va poder volar.
        //
        //Un cop candata, la canço es canta substituint totes les vocals per una de sola (totes les vocals canviades per la A).
        //Volem fer un programa que es digui la lletra donada la vocal amb la que volem substituir.
        //
        //input: i
        //output: ini misci vilivi pir li llim. I li llim is vi ipigir. I li pibri misci is vi qiidir i lis fisqiis i li pibri misci ni vi pidir vilir.
        Scanner scanner=new Scanner(System.in).useLocale(Locale.US);
        String cancion= "una mosca volava per la llum.\n" +
                "i la llum es va apagar.\n" +
                "i la pobra mosca\n" +
                "es va quedar a les fosques\n" +
                "i la pobra mosca no va poder volar.";
        char letra=scanner.next().charAt(0);
        ArrayList<Character> listaVocales=new ArrayList<>(Arrays.asList('a','e','i','o','u'));
        for(char vocal:listaVocales){
            cancion=cancion.replace(vocal,letra);
        }
        System.out.println(cancion);

    }
}
