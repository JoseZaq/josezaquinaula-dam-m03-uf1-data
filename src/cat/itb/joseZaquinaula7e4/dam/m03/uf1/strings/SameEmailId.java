package cat.itb.joseZaquinaula7e4.dam.m03.uf1.strings;

import java.util.Locale;
import java.util.Scanner;

public class SameEmailId {
    //strings
    public static void main(String[] args) {
        //En alguns servidors de correu electrònic (gmail per exemple), l'identificador de l'usuari no té en compte les majuscules ni els punts.
        // Per tant els següents correus són el mateix.
        //mar.om@example.com
        //Mar.Om@example.com
        //marom@example.com
        //m.a.r.o.m@example.com
        //En el nostre sistema de Televisió a la carta NotFlix, hem detectat que molts usuaris usen el mateix correu per poder obtenir un més gratis moltes vegades.
        // Volem fer un sistema que validi quan dos correus són el mateix.
        //
        //L'usuari introduirà dos correus
        //Imprimeix true o false segons si són el mateix correu o no
        Scanner scanner=new Scanner(System.in).useLocale(Locale.US);
        String correo1=scanner.next();
        String correo2=scanner.next();
        boolean fraude=false;
        if(justLowercasesLetters(correo1).equals(justLowercasesLetters(correo2))){
            fraude=true;
        }
        System.out.println(fraude);
    }

    private static String justLowercasesLetters(String string) {
        int a=0;
        while(a!=2) {
            a = 0;
            int capitalX = capitalletterDetector(string);
            int lowerX = pointDetector(string);
            if (capitalX != -1) {
                string = string.replace(string.charAt(capitalX), (char) (string.charAt(capitalX)+ 32));
            } else a++;
            if (lowerX != -1) {
               string=string.substring(0,lowerX)+string.substring(lowerX+1);
            } else a++;
        }
            return string;
        }

    private static int pointDetector(String string) {
        //retorna la posicion del punto
        //ascii: punto=46
        for(int i=0;i<string.length();i++){
            if(string.charAt(i)==46){
                return i;
            }
        }
        return -1;
    }

    private static int capitalletterDetector(String string) {
        //retorna la posicion de la mayuscula
        //Ascii: 65 a 90 mayusculas
        for(int i=0;i<string.length();i++){
            if(string.charAt(i)>=65 && string.charAt(i)<=90){
                return i;
            }
        }
        return -1;
    }
}
