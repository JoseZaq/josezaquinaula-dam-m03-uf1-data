package cat.itb.joseZaquinaula7e4.dam.m03.uf1.strings.practice;

import java.util.Locale;
import java.util.Scanner;

public class SuperSecretText {
    //string practice
    public static void main(String[] args) {
        //Volem fer un petit sistema per a encirptar missatges de text. Donat un enter,
        // sumarem l'enter a cada caràcter per xifrar el missatge i el restarem per dexifrar-lo.
        //
        //L'usuari introduirà 1 si vol xifrar, 0 si vol dexifrar
        //L'usuari introduirà l'enter per xifrar
        //L'usuari introduirà una línia de text
        //Imprimeix per pantalla el text resultant.
        Scanner scanner=new Scanner(System.in).useLocale(Locale.US);
        String newfrase="";
        int opcion=scanner.nextInt();
        int cifra=scanner.nextInt();
        String basura=scanner.nextLine();
        String frase=scanner.nextLine();
        switch (opcion){
            case 0:
                for (int i = 0; i < frase.length(); i++) {
                    newfrase+=(char)(frase.charAt(i)-cifra);
                }
                break;
            case 1:
                for (int i = 0; i < frase.length(); i++) {
                    newfrase+=(char)(frase.charAt(i)+cifra);
                }
                break;
        }
        System.out.println(newfrase);
    }
}
