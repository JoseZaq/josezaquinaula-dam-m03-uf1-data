package cat.itb.joseZaquinaula7e4.dam.m03.uf1.mix;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;

public class HomeworkHelper {
    //general
    public static void main(String[] args) {
        //A un alumne li han manat de deures fer un munt de divisions. Quan les ha acabades
        // li demana a son pare si li pot ajudar a corregir-les.
        // El pare, que té molt de treball i no té temps per a corregir divisions,
        // decideix programar una intel·ligència artificial per a que les corregeixi en el seu lloc.
        // Usant una tècnica de Reconeixement Òptic de Caracters,
        // aconsegueix digitalitzar les divisions.
        //Per exemple, de la següent divisió
        Scanner scanner=new Scanner(System.in).useLocale(Locale.US);
        List<Integer> divideList = new ArrayList<Integer>();
        List<String> divisionSolution = new ArrayList<String>();
        int num=scanner.nextInt();
        while(num!=-1){
            divideList.add(num);
            num=scanner.nextInt();
        }
        for (int i = 0; i < divideList.size(); i+=4) {
            boolean correctDiv=(divideList.get(i+2)*divideList.get(i+1)+divideList.get(i+3))== divideList.get(i);
            System.out.println(correctDiv ? "correcte" : "error");
            //if(correctDiv) divisionSolution.add("correcte");
            //else divisionSolution.add("error");
        }
        //System.out.println(divisionSolution);
    }
}
