package cat.itb.joseZaquinaula7e4.dam.m03.uf1.mix;

import cat.itb.joseZaquinaula7e4.dam.m03.uf1.UsefulMetodes;

import java.util.*;

public class BasketballDecriptor {
    //general
    public static void main(String[] args) {
        //Al basquetbol es poden anotar cistelles d'1, 2 i 3 punts.
        //Donada l'evolució de la puntuació d'un equip en un partit, determina el nombre de cistelles anotades d'1, 2 i 3 punts.
        Scanner scanner=new Scanner(System.in).useLocale(Locale.US);
        int[] tipoPuntos = new int[3];
        List<Integer> puntos;
        puntos=UsefulMetodes.getList(scanner);
        puntos.add(0,0);
        for (int i = 1; i < puntos.size(); i++) {
            if(puntos.get(i) - puntos.get(i-1) == 1) tipoPuntos[0]++;
            if(puntos.get(i) - puntos.get(i-1) == 2) tipoPuntos[1]++;
            if(puntos.get(i) - puntos.get(i-1) == 3) tipoPuntos[2]++;
        }
        System.out.printf("cistelles d'un punt: %d\ncistelles de dos punts: %d\ncistelles de tres punts: %d\n",tipoPuntos[0],tipoPuntos[1],tipoPuntos[2]);
    }
}
