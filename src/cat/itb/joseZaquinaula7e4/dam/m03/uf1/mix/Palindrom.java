package cat.itb.joseZaquinaula7e4.dam.m03.uf1.mix;

import cat.itb.joseZaquinaula7e4.dam.m03.uf1.UsefulMetodes;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;

public class Palindrom {
    //general
    public static void main(String[] args) {
        //Donada una frase digues si és palíndroma.
        //No s'han de comptar els espais en blanc, ni els caracters de puntuació:
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        boolean resposta=false;
        String fraseinv="";
        String frase=scanner.nextLine();
        frase= UsefulMetodes.justLowercasesLetters(frase);
        for (int i = frase.length()-1; i >= 0; i--) {
            fraseinv+=frase.charAt(i);
        }
        if(frase.equals(fraseinv)) resposta = true;
        System.out.println(resposta);
    }
}
