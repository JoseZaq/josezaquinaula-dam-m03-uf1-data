package cat.itb.joseZaquinaula7e4.dam.m03.uf1.mix;

import java.util.*;

public class TenguiFalti {
    //general
    public static void main(String[] args) {
        //Dos aficionats a col·lecionar cromos volen fer un programa que els ajudi a fer
        // intercanvis del cromos que un té repetits i l'altre li falten.
        // El primer usuari introduirà els cromos que té repetits, i el segón els que no té.
        // Necessitem coneixer quins són els que té repetits i li falten al segon.

        //forrmat
        //Primer l'usuari introduirà els cromos repetits. Quan estigui escriurà -1.
        //Després l'usuari introduirà els cromos que li falten ordenats de petit a gran. Quan estigui escriurà -1.
        //Imprimeix els cromos a canviar
        Scanner scanner=new Scanner(System.in).useLocale(Locale.US);
        List<Integer> listaCromosRepetidos;
        List<Integer> listaCromosFaltantes;
        List<Integer> listaCromosaCambiar=new ArrayList<Integer>();
        listaCromosRepetidos=getList(scanner);
        listaCromosFaltantes=getList(scanner);
        for (Integer listaCromosRepetido : listaCromosRepetidos) {
            for (Integer listaCromosFaltante : listaCromosFaltantes) {
                //analiza por cada valor de repetidos todas las faltantes
                if (listaCromosRepetido.equals(listaCromosFaltante)) {
                    //si repetido es igual a faltante añadete a cambiar
                    if(listaCromosaCambiar.size()==0){
                        //si no hya valores en cambiar añade y sale del bucle
                        listaCromosaCambiar.add(listaCromosFaltante);
                        continue;
                    }
                    if(listaCromosFaltante>listaCromosaCambiar.get(listaCromosaCambiar.size()-1))
                        //si es mayor el nuevo añade al ultimo sino a la primera posiciom
                        listaCromosaCambiar.add(listaCromosFaltante);
                    else listaCromosaCambiar.add(0,listaCromosFaltante);
                }
            }
        }
        PrintArrayList(listaCromosaCambiar);
    }

    public static void PrintArrayList(List<Integer> lista) {
        for(Integer listas:lista){
            System.out.print(listas+" ");
        }
        System.out.println();
    }

    public static ArrayList<Integer> getList(Scanner scanner) {
        int input=scanner.nextInt();
        ArrayList<Integer> list=new ArrayList<Integer>();
        while(input!=-1){
            list.add(input);
            input=scanner.nextInt();
        }
        return list;
    }
}
