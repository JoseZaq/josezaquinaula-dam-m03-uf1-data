package cat.itb.joseZaquinaula7e4.dam.m03.uf1.mix;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Locale;
import java.util.Scanner;

public class DniLetterCalculator {
    //general
    public static ArrayList<Character> tablaLetraDni = new ArrayList<Character>(Arrays.asList('T','R','W','A','G','M','Y','F','P','D','X','B'
            ,'N','J','Z','S','Q','V','H','L','C','K','E'));

    public static void main(String[] args) {
        //Donat el número d'un dni, es pot calcular la lletra usant la següent formula
        //L'usuari introduirà el número del dni
        //Imprimeix el dni amb lletra inclosa
        //input: 12345678 output: 12345678Z
        Scanner scanner=new Scanner(System.in).useLocale(Locale.US);
        //dividir entre 23 todo el numero y colocar la letra segun el residuo
        int dni=scanner.nextInt();
        float dniResiduo=dni%23;
        System.out.println(dniResiduo);
        String dniCompleto= Integer.toString(dni)+tablaLetraDni.get((int) dniResiduo);
        System.out.println(dniCompleto);
    }
    public static char getDniLetter(String dni){
        float dniResiduo=Integer.parseInt(dni)%23;
        return tablaLetraDni.get((int)dniResiduo);
    }
}
