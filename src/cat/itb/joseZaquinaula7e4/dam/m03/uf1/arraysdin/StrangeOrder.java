package cat.itb.joseZaquinaula7e4.dam.m03.uf1.arraysdin;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;

public class StrangeOrder {
    //arrays dinamics
    public static void main(String[] args) {
        //L'usuari entrarà un conjunt d'enters per pantalla. Quan introdueixi el -1, és que ja ha acabat.
        // Afegeix el primer a l'inici de la llista, el segon al final, el tercer a l'inici, etc.
        //Printa el resultat:
        Scanner scanner=new Scanner(System.in).useLocale(Locale.US);
        List<Integer> llista= new ArrayList<>();
        int num=scanner.nextInt(),flag=0;
        while(num!=-1){
            if(flag % 2 == 0)
                llista.add(0,num);
            else
                llista.add(num);
            num=scanner.nextInt();
            flag++;
        }
        System.out.println(llista);
    }
}
