package cat.itb.joseZaquinaula7e4.dam.m03.uf1.arraysdin;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;

public class PassaLlista {
    //dinamics
    public static void main(String[] args) {
        //Volem fer un programa que ens digui quins alumnes no han vingut a classe.
        //L'usuari introduirà enters ordenats de major a menor, indicant la posició dels alumnes que han vingut a classe.
        //Quan introdueixi un -1 és que ja ha acabat.
        //Tenim la següent llista de alumnes:
        //"Magalí", "Magdalena", "Magí", "Manel", "Manela", "Manuel",
        // "Manuela", "Mar", "Marc", "Margalida", "Marçal", "Marcel", "Maria", "Maricel", "Marina", "Marta", "Martí", "Martina"
        Scanner scanner=new Scanner(System.in).useLocale(Locale.US);
        List<String> lista=new ArrayList<>();
        lista.add("Magalí");
        lista.add("Magadalena");
        lista.add("Magi");
        lista.add("Manel");
        lista.add("Manela");
        lista.add("Manuel");
        lista.add("Manuela");
        lista.add("Mar");
        lista.add("Marc");
        lista.add("Marçal");
        lista.add("Marcel");
        lista.add("Maria");
        lista.add("Maricel");
        lista.add("Marina");
        lista.add("Marina");
        lista.add("Marta");
        lista.add("Marti");
        lista.add("Martina");
        int num=scanner.nextInt();
        while(num!=-1) {
            lista.remove(num);
            num=scanner.nextInt();
        }
        System.out.println(lista);
    }
}
