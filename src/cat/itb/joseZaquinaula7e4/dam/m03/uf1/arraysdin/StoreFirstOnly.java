package cat.itb.joseZaquinaula7e4.dam.m03.uf1.arraysdin;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;

public class StoreFirstOnly {
    //arraysdin
    public static void main(String[] args) {
        //L'usuari introduirà enters. Enmagatzema en una llista la primera vegada que l'usuari entri un valor.
        //Quan introdueixi un -1 és que ja ha acabat.
        Scanner scanner=new Scanner(System.in).useLocale(Locale.US);
        List<Integer> lista = new ArrayList<Integer>();
        int num=scanner.nextInt();
        while (num != -1) {
            boolean equalverification=false;
            if(lista.size()==0) lista.add(num);
                for (int i = lista.size() - 1; i >= 0; i--) {
                    if (num == lista.get(i)){
                        equalverification=true;
                        break;
                    }
                }
            if(!equalverification) lista.add(num);
            num = scanner.nextInt();
        }
        System.out.println(lista);
    }
}
